function SaveModul(){
	var propinsi = $('#propinsi').val();
	var id = $('#id').val();
	var method = $('#method').val();
	
	$.post("./g3n_propinsi.php", 
		{'method': method, 'propinsi':propinsi, 'id':id},
		function(data){ LoadGrid(data); }, 
		"json"
	);
}

function LoadData(){
	$.post("./g3n_propinsi.php", 
			{'method':'load'},
			function(data){ LoadGrid(data); }, 
			"json"
	);
}

function ResetModul(data){
	$('#id').val('');
	$('#propinsi').val('');
}

function LoadGrid(data){
	$('#table-data-body').html('');
	var strbody = '';
	var kel = '';
	var e;
	for ( var i in data ) {
		e = i % 2;
		var no =  parseInt(i) + 1;
		strbody += "<tr id='data_row_"+ data[i].id +"' class='e"+e+"'>";
		strbody += "<td>" + no + "</td>";
		strbody += "<td>" + data[i].propinsi +"</td>";
		strbody += "<td class='ctr'>";
		strbody += "<a href='#' id='edit_"+data[i].id+"' class='btn' onclick='edit_data("+ data[i].id +"); return false;' >edit</a> ";
		strbody += "<a href='#' id='delete_"+ data[i].id +"' class='btn' onclick='delete_data("+ data[i].id +");return false;' >delete</a>";
		strbody += "</td>";
		strbody += "</tr>";
	}
	$('#table-data-body').html(strbody);
}

function getby(value){
	$.post("g3n_propinsi.php", 
		{'id':value, 'method':'getby'}, 
		function(data){
			$('#id').val(data.id); 
			$('#propinsi').val(data.propinsi);
		},
		"json"
	);
}

function edit_data(value){
	$('#method').val('update');
	getby(value);
	$('#InputForm').dialog('open');
	//alert("method: "+$('#method').val()+"\n"+"id: "+$('#id').val()+"\n"+"propinsi: "+$('#propinsi').val());
}

function delete_data(value){
	if (confirm('Hapus Data, Anda Yakin ?')) { 
		$.post("g3n_propinsi.php", {'id' : value, 'method':'delete'},
			  function(data){
				$("#data_row_" + value).fadeOut('slow', function(){$(this).remove()});
			  }, "json");
	}
}
