<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Insert_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
        
        $this->db_sms = $this->load->database('sms', TRUE);
    }
    function pengaduan($data){
        return $this->db->insert('pol_ponline',$data);
    }
    function pengaduan_approve($param, $data){
        if($param=="terima"){
            $this->db->where('id',$data['id']);
            
            $pengaduan=$this->db->get('pol_ponline')->row_array();
            $pengaduan['KdPengaduan']=$this->initlib->kodePengaduan($pengaduan['KdJenjang'], $pengaduan['KdKategori'], $pengaduan['KdKab'],mysqldatetime_to_date($pengaduan['TglKetahui'],'d-m-Y'));
            $this->Update_db->ponline($pengaduan['id'],array('KdPengaduan' => $pengaduan['KdPengaduan']));
            unset($pengaduan['id']);
            unset($pengaduan['nama']);
            unset($pengaduan['email']);
            unset($pengaduan['telp']);
            unset($pengaduan['alamat']);
            unset($pengaduan['tampil_nama']);
            unset($pengaduan['tampil_telp']);
            unset($pengaduan['tampil_alamat']);
            unset($pengaduan['is_delete']);
            unset($pengaduan['reason_delete']);
            
            return $this->db->insert('g3n_pengaduan',$pengaduan);    
        }
    }
    
    function berita($data){
        $this->db->insert('pol_berita',$data);
    }
    function smsgateway($data){
        $this->db->like('transid', $data['transid']);
        $this->db->from('pol_smsgateway');
        if($this->db->count_all_results() == 0)
            return $this->db->insert('pol_smsgateway',$data);
        
    }
    
    function sms_mo($data){
        $this->db_sms->where('sms_id',$data['sms_id']);
        $this->db_sms->where('id_operator',$data['id_operator']);
        $this->db_sms->from('sms_mo');
        if($this->db_sms->count_all_results() == 0){
            $this->db_sms->insert('sms_mo',$data);
            $this->db_sms->select_max('id');
            return $this->db_sms->get('sms_mo')->row()->id;
        }
        
    }
    
    function sms_dr($data){
        return $this->db_sms->insert('sms_dr',$data);        
    }
    function sms_mt($data){
        return $this->db_sms->insert('sms_mt',$data);
    }
    function pengaduan_sms($data){
        return $this->db->insert('pol_psms',$data);
    }
    function pengaduan_approve_sms($id,$pengaduan,$psms){
        echo $id;
        //print_r($psms);
        $this->Update_db->psms($id,$psms);
        //echo $this->db->last_query();
        $this->db->insert('g3n_pengaduan',$pengaduan);        
    }
    function faq_kategori($data){
        $this->db->insert('pol_faq_kategori',$data);
        $this->db->select_max('id');
        return $this->db->get('pol_faq_kategori')->row()->id;
    }
    function faq($data){
        return $this->db->insert('pol_faq',$data);
    }
    
    function send_email($data){
      return $this->db->insert('pol_send_email',$data);
    }
    
    function user_handling($data_user,$role){
        $this->db->trans_start();
        if($this->db->insert('t_user',$data_user)){
            $id=$this->db->insert_id();
            
            foreach($role as $role_id){
                $this->db->insert('pol_role_jenjang', array('user_id' => $id, 'jenjang_id' => $role_id));
            }
        }
        
        $this->db->trans_complete();
    }
}
?>