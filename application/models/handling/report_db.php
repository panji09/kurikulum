<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Report_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    private function filter($filter=array()){
	$where = array();
	
	$where[] = 'b.deleted = 0';
	if($filter){
	    if(isset($filter['tahun']) && $filter['tahun']!='all'){
		$where[] = 'YEAR(b.tanggal) = '.intval($filter['tahun']);
	    }
	    
	    if(isset($filter['triwulan']) && $filter['triwulan']!='all'){
		switch(intval($filter['triwulan'])){
		    case 1 :
			$min=1; $max=3;
		    break;
		    case 2 :
			$min=4; $max=6;
		    break;
		    case 3 :
			$min=7; $max=9;
		    break;
		    case 4 :
			$min=10; $max=12;
		    break;
		}
		
		$where[] = 'MONTH(b.tanggal) >= '.$min;
		$where[] = 'MONTH(b.tanggal) <= '.$max;
	    }
	    
	    
	    if(isset($filter['provinsi']) && $filter['provinsi']!='all'){
		$where[] = 'b.provinsi_id = '.intval($filter['provinsi']);
	    }
	    
	    if(isset($filter['kabkota']) && $filter['kabkota']!='all'){
		$where[] = 'b.kabkota_id = '.intval($filter['kabkota']);
	    }
	    
	    if(isset($filter['kecamatan']) && $filter['kecamatan']!='all'){
		$where[] = 'b.kecamatan_id = '.intval($filter['kecamatan']);
	    }
	}
	
	return $where;
    }
    function kategori($filter=array()){
	$this->db->select('a.id, a.name as kategori, count(b.kategori_id) as total',false);
	$this->db->from('kategori a');
	
	//filter
	$where = $this->filter($filter);
	
	//end filter
	$this->db->join('pengaduan b','a.id = b.kategori_id and b.approved = 1 '.($where ? ' and '.implode(' and ',$where): ''), 'left');
	
	
	$this->db->where('a.deleted',0);
	$this->db->group_by('a.id');
	$this->db->order_by('a.sort','asc');
	return $this->db->get();
    }
    
    function sumber_info($filter = array()){
	$this->db->select('a.id, a.name as sumber, count(b.sumber_id) as total',false);
	$this->db->from('sumber a');
	
	//filter
	$where = $this->filter($filter);
	//end filter
	$this->db->join('pengaduan b','a.id = b.sumber_id and b.approved = 1 '.($where ? ' and '.implode(' and ',$where): ''), 'left');
	
	$this->db->where('a.deleted',0);
	$this->db->group_by('a.id');
	$this->db->order_by('a.sort','asc');
	return $this->db->get();
    }
    
    function status($filter = array()){
	$this->db->select('a.id, a.name as status, count(b.id) as total',false);
	$this->db->from('status a');
	
	//filter
	$where = $this->filter($filter);
	
	//end filter
	$this->db->join('pengaduan b','a.id = b.status_id and b.approved = 1 '.($where ? ' and '.implode(' and ',$where): ''), 'left');
	
	$this->db->where('a.deleted',0);
	$this->db->group_by('a.id');
	$this->db->order_by('a.sort','asc');
	return $this->db->get();
    }
    
    function program($filter = array()){
	$this->db->select('a.id, a.name as program, count(b.id) as total',false);
	$this->db->from('program a');
	
	//filter
	$where = $this->filter($filter);
	
	//end filter
	$this->db->join('pengaduan b','a.id = b.program_id and b.approved = 1 '.($where ? ' and '.implode(' and ',$where): ''), 'left');
	
	$this->db->where('a.deleted',0);
	$this->db->group_by('a.id');
	$this->db->order_by('a.sort','asc');
	return $this->db->get();
    }
    
    function kategori_status($filter = array()){
	$this->db->select('a.id, a.name as kategori, count(b.kategori_id) as total',false);
	$this->db->select('sum(if(b.status_id = 1,1,0)) as pending',false);
	$this->db->select('sum(if(b.status_id = 2,1,0)) as proses',false);
	$this->db->select('sum(if(b.status_id = 3,1,0)) as selesai',false);
	$this->db->from('kategori a');
	
	//filter
	$where = $this->filter($filter);
	
	//end filter
	$this->db->join('pengaduan b','a.id = b.kategori_id and b.approved = 1 '.($where ? ' and '.implode(' and ',$where): ''), 'left');
	
	$this->db->where('a.deleted',0);
	$this->db->group_by('a.id');
	$this->db->order_by('a.sort','asc');
	return $this->db->get();
    }
    
    function media($filter = array()){
	$this->db->select('a.id, a.name as media, count(b.id) as total',false);
	$this->db->from('media a');
	
	//filter
	$where = $this->filter($filter);
	
	//end filter
	$this->db->join('pengaduan b','a.id = b.media_id and b.approved = 1 '.($where ? ' and '.implode(' and ',$where): ''), 'left');
	
	$this->db->where('a.deleted',0);
	$this->db->group_by('a.id');
	$this->db->order_by('a.sort','asc');
	return $this->db->get();
    }
    
    function program_status($filter = array()){
	$this->db->select('a.id, a.name as program, count(b.program_id) as total',false);
	$this->db->select('sum(if(b.status_id = 1,1,0)) as pending',false);
	$this->db->select('sum(if(b.status_id = 2,1,0)) as proses',false);
	$this->db->select('sum(if(b.status_id = 3,1,0)) as selesai',false);
	$this->db->from('program a');
	
	//filter
	$where = $this->filter($filter);
	
	//end filter
	$this->db->join('pengaduan b','a.id = b.program_id and b.approved = 1 '.($where ? ' and '.implode(' and ',$where): ''), 'left');
	
	$this->db->where('a.deleted',0);
	$this->db->group_by('a.id');
	$this->db->order_by('a.sort', 'asc');
	return $this->db->get();
    }
    
    function pelaku_status($filter = array()){
	$this->db->select('a.id, a.name as pelaku, count(c.pelaku_id) as total',false);
	$this->db->select('count(if(b.status_id = 1,c.pelaku_id,null)) as pending', false);
	$this->db->select('count(if(b.status_id = 2,c.pelaku_id,null)) as proses', false);
	$this->db->select('count(if(b.status_id = 3,c.pelaku_id,null)) as selesai', false);
	
	$this->db->from('pelaku a');
	
	//filter
	$where = $this->filter($filter);
	//end filter
	
	$this->db->join('pengaduan b','b.approved = 1 '.($where ? ' and '.implode(' and ',$where): ''), 'left',false);
	$this->db->join('pengaduan_pelaku c', 'a.id = c.pelaku_id and b.id = c.pengaduan_id','left');
	
	$this->db->where('a.deleted',0);
	$this->db->group_by('a.id');
	$this->db->order_by('a.sort','asc');
	return $this->db->get();
    }
    
    function wilayah_status($filter = array(), $level='provinsi'){
	$this->db->select('a.id, a.name as wilayah, '.($level=='nasional' ? 'count(b.provinsi_id) as total' : ($level=='provinsi' ? 'count(b.kabkota_id) as total' : ($level == 'kabkota' ? 'count(b.kecamatan_id) as total' : ''))),false);
	$this->db->select('sum(if(b.status_id = 1,1,0)) as pending',false);
	$this->db->select('sum(if(b.status_id = 2,1,0)) as proses',false);
	$this->db->select('sum(if(b.status_id = 3,1,0)) as selesai',false);
	
	if($level == 'nasional'){
	    $this->db->from('provinsi a');
	    $where_wilayah = 'and a.id = b.provinsi_id';
	}elseif($level == 'provinsi'){
	    $this->db->from('kabkota a');
	    $this->db->where('a.provinsi_id',$filter['provinsi']);
	    $where_wilayah = 'and a.id = b.kabkota_id';
	}elseif($level == 'kabkota'){
	    $this->db->from('kecamatan a');
	    $this->db->where('a.kabkota_id',$filter['kabkota']);
	    $where_wilayah = 'and a.id = b.kecamatan_id';
	}
	//filter
	$where = $this->filter($filter);
	//end filter
	
	$this->db->join('pengaduan b','b.approved = 1 '.$where_wilayah.' '.($where ? ' and '.implode(' and ',$where): ''), 'left',false);
	
	
	$this->db->group_by('a.id');
	$this->db->order_by('a.id');
	return $this->db->get();
    }
    
}
?> 
