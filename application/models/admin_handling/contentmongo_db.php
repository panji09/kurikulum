<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Content_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function exist($id){
	return count($this->get_all(array('id' => $id))) == 1;
    }
    
    function get($id){
	return $this->get_all($filter = array('id' => $id));
    }
    
    function get_berita($limit=null, $offset=null){
	
	return $this->get_all($filter = array('type' => 'berita'), $limit, $offset);
    }
    
    function get_all($filter=array(), $limit=null, $offset=null){
	
	if(isset($filter['id']))
	    $this->mongoci->where('_id', new MongoId($filter['id']));
	
	if(isset($filter['type']))
	    $this->mongoci->where('type', $filter['type']);
	
	if(isset($filter['date_gt']))
	    $this->mongoci->whereGt('date', $filter['date_gt']);
	
	if(isset($filter['date_lt']))
	    $this->mongoci->whereLt('date', $filter['date_lt']);
	    
	if($limit)
	    $this->mongoci->limit($limit);
        if($offset)
	    $this->mongoci->offset($offset);
	
	$this->mongoci->orderBy(array('date' => 'DESC'));
        
        return $this->mongoci->get('content');
        //$this->mongoci->lastQuery();
    }
    
    function save($content_id=null, $data_content=array(), $data_push=array()){
	$result = false;
	
	$last_update = array('time' => time(), 'ip' => $this->input->ip_address());
	
	if($content_id && $this->exist($content_id)){
	    //update
	    $this->mongoci->where('_id', new MongoId($content_id));
	    
	    if($data_content)
		$this->mongoci->set($data_content);
	    
	    $this->mongoci->set('last_update', $last_update);
	    $this->mongoci->push('log_last_update', $last_update);
	    
	    if($data_push)
		$this->mongoci->push($data_push);

	    $result = $this->mongoci->update('content');
	    
	}else{
	    //insert
	    $data_content['last_update'] = $last_update;
	    $data_content['log_last_update'] = array($last_update);
	    $result = $this->mongoci->insert('content', $data_content);
	    
	}
	return $result;
    }
}
?> 
