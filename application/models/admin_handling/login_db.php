<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    function login_admin($data=null){
        $this->db->select('user.id, user.person_id, user.username, user.last_login, user.last_ip, person.nama, person.email, person.handphone');
        $this->db->from('user')
                ->join('person','user.person_id = person.id')
                ->where('user.username',$data['username'])
                ->where('user.password',md5($data['password']))
                ->where('!user.deleted','',false);
        $is_login=$this->db->get();
        if($is_login->num_rows()==1){
	    $result = $is_login->row();
	    
	    $time = time();
	    $ip = $this->input->ip_address();
	    $this->db->where('id',$result->id);
            $this->db->update('user', array('last_login' => $time, 'last_ip' => $ip));
            
            //$result->last_login = $time;
            //$result->last_ip = $ip;
            return $result;
        }
        else
            return false;
    }  
}
?>