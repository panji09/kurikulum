<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Person_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct(); 
    }
    
    function get_info($person_id){
        $query=$this->db->get_where('person',array('id' => $person_id));
        if($query->num_rows() == 1){
            return $query->row();
        }else{
            $person_obj = new stdClass();
            foreach($query->list_fields() as $field){
                $person_obj->$field = '';
            }
            return $person_obj;
        }
    }
    function save(&$person_id=null, &$data_person){
        $sucess=false;
        
        if($person_id){
            //update person
            $this->db->where('id',$person_id);
            if($this->db->update('person',$data_person))
                return $sucess=true;
            else
                return $sucess;
            
        }else{
            //new person
            if($this->db->insert('person',$data_person)){
                $insert_id = $this->db->insert_id();
                $data_person['person_id']=$insert_id;
                
                $person_id = $insert_id;
                
                return $sucess=true;
            }else
                return $sucess; //return false
        }
    }
}
?>