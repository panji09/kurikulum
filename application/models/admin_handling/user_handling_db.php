<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_handling_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
        
    }
    
    function get($id){
	$query = $this->get_all($filter=array('id' => $id));
	//echo $this->db->last_query();
	if($query->num_rows() == 1){
            return $query->row();
        }else{
            $user_handling_obj = new stdClass();
            foreach($query->list_fields() as $field){
                $user_handling_obj->$field = '';
            }
            return $user_handling_obj;
        }
    }
    
    function get_all($filter=array(), $limit=null, $offset=0){
	$this->db->select('a.id, a.username, a.password, a.activated, b.nama, b.nip, b.jabatan_dinas, b.jabatan_kjp_bop, b.jabatan_bos, b.email, b.alamat_kantor, b.telp_kantor, b.fax_kantor, b.alamat_rumah, b.handphone, a.person_id, a.last_login, a.last_logout, c.provinsi_id, c.kabkota_id, c.level_id');
	$this->db->from('user_handling a');
	$this->db->join('person b','a.person_id = b.id', 'left');
	$this->db->join('role_level c','a.id = c.user_id', 'left');
	
	$this->db->where('a.deleted',0);
	
	if($filter){
	    if(isset($filter['id']))
		$this->db->where('a.id', $filter['id']);
	    if(isset($filter['level_id']))
		$this->db->where('c.level_id', $filter['level_id']);
	    if(isset($filter['provinsi_id']))
		$this->db->where('c.provinsi_id', $filter['provinsi_id']);
	    if(isset($filter['kabkota_id']))
		$this->db->where('c.kabkota_id', $filter['kabkota_id']);
	}
	if($limit)
	    $this->db->limit($limit,$offset);
	    
	return $this->db->get();
    }
    function exist($id){
        return $this->db->get_where('user_handling',  array('id' => $id));
    }
    
    function save($id=null, $data_person, $data_user, $data_role_jenjang, $data_role_level, $data_role_program){
        $result=false;
        $CI =& get_instance();
        $CI->load->model('admin_handling/person_db');
        
        $this->db->trans_start();
        
        $exist=$this->exist($id);
        if($exist->num_rows() == 1){
	    //update
	    $user_handling = $exist->row();
	    if($CI->person_db->save($user_handling->person_id,$data_person)){
		//echo $this->db->last_query();
		$this->db->where('id', $id);
		$user_handling_id = $id;
		if($result=$this->db->update('user_handling', $data_user)){
		    if($data_role_jenjang)
			$this->save_role_jenjang($user_handling_id, $data_role_jenjang);
		    //echo $this->db->last_query();
		    if($data_role_level)
			$this->save_role_level($user_handling_id, $data_role_level);
		    //echo $this->db->last_query();
		    if($data_role_program)
			$this->save_role_program($user_handling_id, $data_role_program);
		    //echo $this->db->last_query();
		}
	    }
        }else{
	    //insert
	    if($CI->person_db->save($id=null,$data_person)){
		$data_user['person_id'] = $data_person['person_id'];
		if($result=$this->db->insert('user_handling', $data_user)){
		    $user_handling_id = $this->db->insert_id();
		    //echo $this->db->last_query();
		    if($data_role_jenjang)
			$this->save_role_jenjang($user_handling_id, $data_role_jenjang);
		    //echo $this->db->last_query();
		    if($data_role_level)
			$this->save_role_level($user_handling_id, $data_role_level);
		    //echo $this->db->last_query();
		    if($data_role_program)
			$this->save_role_program($user_handling_id, $data_role_program);
		    //echo $this->db->last_query();
		}
	    }
        }
        $this->db->trans_complete();
        
        //echo $this->db->last_query();
        return $result;
    }
    
    function delete($id){
	$this->db->where('id',$id);
	return $this->db->update('user_handling', array('deleted' => 1));
    }
    
    function save_role_jenjang($user_id, $data_role_jenjang){
	$result = false;
	$this->db->delete('role_jenjang', array('user_id' => $user_id));
	foreach($data_role_jenjang as $jenjang_id){
	    $result=$this->db->insert('role_jenjang', array('user_id' => $user_id, 'jenjang_id' => $jenjang_id));
	}
	
	return $result;
    }
    function get_role_jenjang($user_id){
	return $this->db->get_where('role_jenjang', array('user_id' => $user_id));
    }
    function save_role_level($user_id, $data_role_level){
	$this->db->delete('role_level', array('user_id' => $user_id));
	$data_role_level['user_id'] = $user_id;
	return $this->db->insert('role_level', $data_role_level);
	
    }
    function get_role_level($user_id){
	return $this->db->get_where('role_level', array('user_id' => $user_id));
    }
    function save_role_program($user_id, $data_role_program){
	$result = false;
	$this->db->delete('role_program', array('user_id' => $user_id));
	foreach($data_role_program as $program_id){
	    $result=$this->db->insert('role_program', array('user_id' => $user_id, 'program_id' => $program_id));
	}
	
	return $result;
    }
    function get_role_program($user_id){
	return $this->db->get_where('role_program', array('user_id' => $user_id));
    }
    
    function save_user_handling(&$id, $data_user_handling){
	$result=false;
        $this->db->trans_start();
	
	$exist=$this->exist($id);
	if($exist->num_rows() == 1){
	    //update
	    $this->db->where('id', $id);
	    $result = $this->db->update('user_handling', $data_user_handling);
	}else{
	    //insert
	    $result = $this->db->insert('user_handling', $data_user_handling);
	    $id = $this->db->insert_id();
	}
	
	$this->db->trans_complete();
	return $result;
    }
}
?>