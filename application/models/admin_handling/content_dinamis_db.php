<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Content_dinamis_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    function exist($id){
        return $this->db->get_where('content_dinamis',  array('id' => $id));
    }
    function save($id, $data_content, $data_files, $data_content_dinamis){
        $result=false;
        $CI =& get_instance();
	$CI->load->model('admin_handling/content_files_db');
	
        $this->db->trans_start();
        
        $exist=$this->exist($id);
        if($exist->num_rows() == 1){
            //update
            $content_dinamis=$exist->row();
            $this->db->where('id', $content_dinamis->content_id);
            if($this->db->update('content',$data_content)){
                //insert files
                if($data_files)
		    $return = $CI->content_files_db->save($content_id=$content_dinamis->content_id, $data_files);
                
                $this->db->where('id', $id);
                $result=$this->db->update('content_dinamis',$data_content_dinamis);
            }
        }else{
            //insert
            if($this->db->insert('content',$data_content)){
                $content_id = $this->db->insert_id();
                $data_content_dinamis['content_id']= $content_id;
                $result = $this->db->insert('content_dinamis',$data_content_dinamis);
                
                if($data_files)
		    $return = $CI->content_files_db->save($content_id, $data_files);
            }
        }
        $this->db->trans_complete();
        
        return $result;
    }
    
    function get($id){
        /*
        $this->db->select('content_dinamis.id, content.tags, content.category, content.title, content.title, content.content, content.agenda_from, content.agenda_to, content.last_update, person.nama, content_dinamis.published');
        $this->db->from('content_dinamis');
        $this->db->join('content', 'content.id = content_dinamis.content_id','inner');
        $this->db->join('person', 'content.author = person.id','inner');
        
        $this->db->where('content_dinamis.id',$id);
        
        $this->db->where('content_dinamis.deleted', 0);
        */
        $query = $this->get_all(null,null,0,array('id' => $id));
        //echo $this->db->last_query();
        if($query->num_rows() == 1){
            return $query->row();
        }else{
            $content_obj = new stdClass();
            foreach($query->list_fields() as $field){
                $content_obj->$field = '';
            }
            return $content_obj;
        }
    }
    /*
    function get_files($id){
	$this->db->select('content_files.id, content_files.name, content_files.file, content_files.size');
        $this->db->from('content_dinamis');
        $this->db->join('content', 'content.id = content_dinamis.content_id','inner');
        $this->db->join('content_files', 'content_files.content_id = content.id','inner');
        
        $this->db->where('content_dinamis.id',$id);
        
        $this->db->where('content_dinamis.deleted', 0);
        
        return $this->db->get();
    }
    function delete_file($id){
	return $this->db->delete('content_files', array('id' => $id));
    }
    */
    function get_all($content_kategori_id=null, $limit=null, $offset=0, $filter=array()){
        $this->db->select('content_dinamis.id,
        content_dinamis.content_kategori_id, content.tags, content.category, content.title, content.content, content.agenda_from, content.agenda_to, content.last_update, person.nama, content_dinamis.published');
        $this->db->from('content_dinamis');
        $this->db->join('content', 'content.id = content_dinamis.content_id','inner');
        $this->db->join('person', 'content.author = person.id','inner');
        
        if($content_kategori_id)
	    $this->db->where('content_dinamis.content_kategori_id', $content_kategori_id);
        
        if($filter){
	    if(isset($filter['published']))
		$this->db->where('content_dinamis.published', $filter['published']);
	    
	    if(isset($filter['category']))
		$this->db->where('content.category', $filter['category']);
	    
	    if(isset($filter['tag']))
		$this->db->like('content.tags', $filter['tag']);
		
	    if(isset($filter['gt_id']))
		$this->db->where('content_dinamis.id >', $filter['gt_id']);
	    
	    if(isset($filter['lt_id']))
		$this->db->where('content_dinamis.id <', $filter['lt_id']);
		
	    if(isset($filter['id']))
		$this->db->where('content_dinamis.id', $filter['id']);
	    
        }
        
        $this->db->where('content_dinamis.deleted', 0);
        
        if($limit)
	  $this->db->limit($limit, $offset);
	
	$this->db->order_by('content_dinamis.id', 'DESC');
	
	return $this->db->get();
    }
    
    function delete($id){
	$this->db->where('id', $id);
	$this->db->update('content_dinamis', array('deleted' => 1));
    }
}
?>