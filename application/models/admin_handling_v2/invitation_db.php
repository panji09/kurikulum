<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Invitation_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function exist($invitation_id){
	return count($this->get_all(array('invitation_id' => $invitation_id))) == 1;
    }
    
    function get($invitation_id){
	return $this->get_all($filter = array('invitation_id' => $invitation_id));
    }
    
    function get_all($filter=null){
	
	if(isset($filter['invitation_id']))
	    $this->mongoci->where('_id', new MongoId($filter['invitation_id']));
	
	$this->mongoci->where('deleted',0);
        $this->mongoci->orderBy(array('created' => 'DESC'));
        
        return $this->mongoci->get('invitation');
        //$this->mongoci->lastQuery();
    }
    
    function save($invitation_id=null, $data_page=array(), $data_push=array()){
	$result = false;
	
	$last_update = array('time' => time(), 'ip' => $this->input->ip_address());
	
	if($invitation_id && $this->exist($invitation_id)){
	    //update
	    $this->mongoci->where('_id', new MongoId($invitation_id));
	    
	    if($data_page)
		$this->mongoci->set($data_page);
	    
	    $this->mongoci->set('last_update', $last_update);
	    $this->mongoci->push('log_last_update', $last_update);
	    
	    if($data_push)
		$this->mongoci->push($data_push);

	    $result = $this->mongoci->update('invitation');
	    
	}else{
	    //insert
	    $data_page['last_update'] = $last_update;
	    $data_page['log_last_update'] = array($last_update);
	    $data_page['created'] = time();
	    $data_page['deleted'] = 0;
	    
	    $result = $this->mongoci->insert('invitation', $data_page);
	    
	}
	
	return $result;
    }
    
    function delete($invitation_id=null){
	return $this->save($invitation_id, array('deleted' => 1));
    }
    
}
?> 
