<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Feedback_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function exist($feedback_id){
	return count($this->get_all(array('feedback_id' => $feedback_id))) == 1;
    }
    
    function get($feedback_id){
	return $this->get_all($filter = array('feedback_id' => $feedback_id));
    }
    
    function get_all($filter=null){
	
	if(isset($filter['feedback_id']))
	    $this->mongoci->where('_id', new MongoId($filter['feedback_id']));
	
	$this->mongoci->where('deleted',0);
        $this->mongoci->orderBy(array('created' => 'DESC'));
        
        return $this->mongoci->get('feedback');
        //$this->mongoci->lastQuery();
    }
    
    function save($feedback_id=null, $data_page=array(), $data_push=array()){
	$result = false;
	
	$last_update = array('time' => time(), 'ip' => $this->input->ip_address());
	
	if($feedback_id && $this->exist($feedback_id)){
	    //update
	    $this->mongoci->where('_id', new MongoId($feedback_id));
	    
	    if($data_page)
		$this->mongoci->set($data_page);
	    
	    $this->mongoci->set('last_update', $last_update);
	    $this->mongoci->push('log_last_update', $last_update);
	    
	    if($data_push)
		$this->mongoci->push($data_push);

	    $result = $this->mongoci->update('feedback');
	    
	}else{
	    //insert
	    $data_page['last_update'] = $last_update;
	    $data_page['log_last_update'] = array($last_update);
	    $data_page['created'] = time();
	    $data_page['deleted'] = 0;
	    
	    $result = $this->mongoci->insert('feedback', $data_page);
	    
	}
	
	return $result;
    }
    
    function delete($user_id=null){
	return $this->save($feedback_id, array('deleted' => 1));
    }
    
}
?> 
