<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emailer extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','date'));
		//$this->load->model(array('admin_handling/region_db', 'admin_handling/content_dinamis_db','admin_handling/content_statis_db', 'admin_handling/content_files_db', 'admin_handling/pengaduan_db', 'select_db', 'admin_handling/respon_db'));
		$this->load->library(array('initlib','session','email'));
		//$this->load->database();
	}
	
	public function index($from=null, $to=null, $cc=null, $bcc=null, $subject=null, $message=null, $reply_to=null){
	    
	    //echo base64_decode($cc);
	    if($from && $to && $subject && $message && php_sapi_name() == 'cli'){
		
		$from = json_decode(base64_decode($from), true);
		$to = json_decode(base64_decode($to), true);
		$cc = json_decode(base64_decode($cc), true);
		$bcc = json_decode(base64_decode($bcc), true);
		$subject = base64_decode($subject);
		$message = base64_decode($message);
		$reply_to = json_decode(base64_decode($reply_to), true);
		
		$this->email->from($from['email'], $from['name']);
		$this->email->to($to); 
		$this->email->cc($cc); 
		$this->email->bcc($bcc); 
		
		if($reply_to)
		    $this->email->reply_to($reply_to['email'], $reply_to['name']);
		
		$this->email->subject($subject);
		$this->email->message($message);	

		$this->email->send();
		
		echo $this->email->print_debugger();
	    }
	}
	
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */