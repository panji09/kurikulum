<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH.'/third_party/simple_html_dom/simple_html_dom.php';
require_once(APPPATH.'third_party/recaptcha/recaptchalib.php');

class Halaman extends CI_Controller {
	function __construct(){
	    parent::__construct();
	    
	    $this->load->database();
	    
	    $this->config->load('recaptcha');
	    
	    session_start();
	    $this->program_id = 4;
	}
	
	function index(){
	    redirect('halaman/berita');
	}
	
	function tag($content_kategori_id=null){
	    $result = array();
	    
	    $query = $this->content_dinamis_db->get_all($content_kategori_id,null,0, array('published' => 1));
	    
	    if($query->num_rows()){
		$tags = $query->result();
		
		foreach($tags as $row){
		    if($row->tags){
			$temp = explode(',', strtolower($row->tags));
		    
			foreach($temp as $row1){
			    $result[$row1] = (isset($result[$row1]) ? $result[$row1] : 0) + 1;
			}
		    }
		}
	    }
	    
	    return $result;
	    
	}
	
	function init_content(){
	    $data['berita']=$this->content_dinamis_db->get_all($content_kategori_id=1, $limit=5, $offset=0, array('published' => 1));
	    $data['agenda']=$this->content_dinamis_db->get_all($content_kategori_id=5, $limit=5, $offset=0, array('published' => 1));
	    
	    return $data;
	}
	
// 	public function berita($param1=null, $param2=null){
// 	    $data['title']= $this->config->item('title').' : Berita';
// 	    
// 	    $data['tag']=$this->tag($content_kategori_id=1);
// 	    $data = array_merge($data, $this->init_content());
// 	    
// 	    if($param1=='tag'){
// 		$data['halaman_main'] = $this->content_dinamis_db->get_all($content_kategori_id=1, $limit=5, $offset=0, array('published' => 1, 'tag' => $param2));
// 		$data['content'] = 'halaman';
// 	    }
// 	    elseif(is_numeric($param1)){
// 		$id = $param1;
// 		$data['halaman_main'] = $this->content_dinamis_db->get($id);
// 		$data['content'] = 'halaman_single';
// 	    }else{
// 		$data['halaman_main'] = $this->content_dinamis_db->get_all($content_kategori_id=1, $limit=5, $offset=0, array('published' => 1));
// 		$data['content'] = 'halaman';
// 	    }
// 	    $this->load->view('home/main',$data);
// 	      
// 	}
	
	public function agenda($param1=null, $param2=null){
	    $data['title']= $this->config->item('title').' : Agenda';
	    
	    $data['tag']=$this->tag($content_kategori_id=5);
	    $data = array_merge($data, $this->init_content());
	    
	    if($param1=='tag'){
		$param2 = urldecode($param2);
		$data['halaman_main'] = $this->content_dinamis_db->get_all($content_kategori_id=5, $limit=5, $offset=0, array('published' => 1, 'tag' => $param2));
		//echo $this->db->last_query();
		$data['content'] = 'halaman';
	    }
	    elseif(is_numeric($param1)){
		$id = $param1;
		$data['halaman_main'] = $this->content_dinamis_db->get($id);
		$data['content'] = 'halaman_single';
	    }else{
		$data['halaman_main'] = $this->content_dinamis_db->get_all($content_kategori_id=5, $limit=5, $offset=0, array('published' => 1));
		$data['content'] = 'halaman';
	    }
	    $this->load->view('home/main',$data);
	      
	}
	
	/*
	<?php
			$list_halaman = array(
			    'berita' => 'Berita',
			    'agenda' => 'Agenda',
			    'usulan_buku' => 'Usulan Buku',
			    'usulan_standar_pendidikan' => 'Usulan Standar Pendidikan',
			    'usulan_kerangka_dasar_dan_struktur' => 'Usulan Kerangka Dasar dan Struktur'
			);
		    ?>
	*/
	
	function usulan($id=null){
	
	    $this->session->set_userdata('previous', $this->uri->uri_string());
	    
	    $data['title']= $this->config->item('title').' : Usulan';
	    
	    $data['content'] = 'usulan';
	    $this->load->view('home/main',$data);
	   
	}
	
	function progres_kurikulum($id=null){
	
	    $this->session->set_userdata('previous', $this->uri->uri_string());
	    
	    $data['title']= $this->config->item('title').' : Tentang Kurikulum';
	    
	    $data['content'] = 'progres_kurikulum';
	    $this->load->view('home/main',$data);
	   
	}
	
	function perkembangan_kurikulum(){
	    $this->session->set_userdata('previous', $this->uri->uri_string());
	    
	    $data['title'] = 'Perkembangan Kurikulum';
	    
	    $data['content'] = 'perkembangan_kurikulum';
	    $this->load->view('home/main',$data);
	}
	
	function berita(){
	    $this->session->set_userdata('previous', $this->uri->uri_string());
	    
	    $data['title'] = 'Berita';
	    
	    $data['content'] = 'berita';
	    $this->load->view('home/main',$data);
	}
	
	function berita_kurikulum(){
	    $this->session->set_userdata('previous', $this->uri->uri_string());
	    
	    $data['title'] = 'Berita Kurikulum';
	    
	    $data['content'] = 'berita_kurikulum';
	    $this->load->view('home/main',$data);
	}
	
	function ajax_info_loadmore($page_id=null, $created=null){
// 	    echo 'uy';
// 	    $filter['created_lt'] = $created;
	    $query = $this->dynamic_pages_db->get_all(array('page_id'=> $page_id, 'published' => "1", 'created_lt' => intval($created)),$limit=5, $offsset=0);
// 	    print_r($query);
	    foreach($query as $row){
// 		print_r($row);
		echo "<div>
			  <a href='".site_url($page_id.'/'.$row['_id']->{'$id'})."' style='color:white'>".date('d-m-Y',$row['created'])."</a> <br>
			  <a href='".site_url($page_id.'/'.$row['_id']->{'$id'})."' style='color:white'>".$row['title']."</a>
		      </div>";
	    }
	}
	
	function usulan_buku($id=null){
	
	    $this->session->set_userdata('previous', $this->uri->uri_string());
	    
	    $data['title']= $this->config->item('title').' : Usulan Buku';
	    
	    $data = array_merge($data, $this->init_content());
	    
	    if($id){
		$data['halaman_main'] = $this->content_dinamis_db->get($id);
		$data['content'] = 'halaman_single';
	    }else{
		$category = $this->session->userdata('usulan_buku_category');
		if($category && $category != array('null','null','null')){
		    $filter['category'] = implode(',',$category);
		    //print_r($data['category']);
		}
		$filter['published'] = 1;
		$data['halaman_main'] = $this->content_dinamis_db->get_all($content_kategori_id=7, $limit=5, $offset=0, $filter);
		//echo $this->db->last_query();
		$data['content'] = 'halaman';
	    }
	    $this->load->view('home/main',$data);
	   
	}
	
	function usulan_standar_pendidikan($id=null){
	    $data['title']= $this->config->item('title').' : Usulan Standar Pendidikan';
	    
	    $data = array_merge($data, $this->init_content());
	    
	    if($id){
		$data['halaman_main'] = $this->content_dinamis_db->get($id);
		$data['content'] = 'halaman_single';
	    }else{
		$category = $this->session->userdata('usulan_standar_pendidikan_category');
		if($category && $category != array('null')){
		    $filter['category'] = implode(',',$category);
		    //print_r($data['category']);
		}
		$filter['published'] = 1;
		$data['halaman_main'] = $this->content_dinamis_db->get_all($content_kategori_id=8, $limit=5, $offset=0, $filter);
		$data['content'] = 'halaman';
	    }
	    $this->load->view('home/main',$data);
	   
	}
	
	function usulan_kerangka_dasar_dan_struktur($id=null){
	    $data['title']= $this->config->item('title').' : Usulan Kerangka Dasar dan Struktur';
	    
	    $data = array_merge($data, $this->init_content());
	    
	    if($id){
		$data['halaman_main'] = $this->content_dinamis_db->get($id);
		$data['content'] = 'halaman_single';
	    }else{
		
		$category = $this->session->userdata('usulan_kerangka_dasar_dan_struktur_category');
		if($category && $category != array('null')){
		    $filter['category'] = implode(',',$category);
		    //print_r($data['category']);
		}
		$filter['published'] = 1;
		$data['halaman_main'] = $this->content_dinamis_db->get_all($content_kategori_id=9, $limit=5, $offset=0, $filter);
		$data['content'] = 'halaman';
	    }
	    $this->load->view('home/main',$data);
	   
	}
	
	function ajax_loadmore($halaman=null, $content_id=null){
	    
	    $list_halaman = array(
		'berita' => 1,
		'agenda' => 5,
		'usulan_buku' => 7,
		'usulan_standar_pendidikan' => 8,
		'usulan_kerangka_dasar_dan_struktur' => 9
	    );
	    
	    $category = $this->session->userdata($halaman.'_category');
	    if(isset($category[0]) && $category[0] != 'null'){
		$filter['category'] = implode(',',$category);
		//print_r($data['category']);
	    }
	    $filter['published'] = 1;
	    $filter['lt_id'] = $content_id;
	    
	    $content_kategori_id = $list_halaman[$halaman];
	    $data['halaman'] = $halaman;
	    $data['halaman_main'] = $this->content_dinamis_db->get_all($content_kategori_id, $limit=5, $offset=0, $filter);
	    $this->load->view('home/inc/content/list_content_halaman', $data);
	}
	
	function post_filter($halaman=null){
	    //print_r($_POST);
	    $data_search = array(
		$halaman.'_category' => $this->input->post('category'),
	    );
	    
	    $this->session->set_userdata($data_search);
	    
	    redirect($halaman);
	}
	
	function clear_filter($halaman=null){
	    $data_search = array(
		'type' => '',
		'cari' => '',
		'tahun' => '',
		'provinsi' => '',
		'kabkota' => '',
		'kategori' => ''
	    );
	    $this->session->unset_userdata($data_search);
	    
	    redirect($halaman);
	}
	
	function post_usulan($halaman=null){
	    //echo 'uy';
	    //print_r($_FILES['file']['name']); exit;
	    $input = $this->input->post('usulan');
	    $input['attachment']='';
	   
	    $recaptcha = $this->input->post('g-recaptcha-response');
	    $response = $this->recaptcha->verifyResponse($recaptcha);

	    if (!(isset($response['success']) and $response['success'] === true)) {
		$this->session->set_flashdata('success', array('status' => false, 'msg' => 'kode verifikasi error'));
	    } else {
		//sukses
		
		unset($input['g-recaptcha-response']);
		
		$input['attachment']='';
		//upload
		if(isset($_FILES['file']) && $_FILES['file']['name']){
		    $config = array();
		    $config['upload_path'] = getcwd().'/media/upload/files/usulan';
		    $config['allowed_types'] = 'doc|docx|xls|xlsx|ppt|pptx|pdf|zip|rar|jpg|png|jpeg';
		    $config['max_size'] = '51200'; //50MB
		    $config['file_name'] = 'usulan_'.time();
		    $config['overwrite'] = true;
		    
		    $this->load->library('upload', $config);
		    if (!$this->upload->do_upload('file')){
			$error = $this->upload->display_errors('','');
			$this->session->set_flashdata('success', array('status' => false, 'msg' => $error));
			
			redirect($this->session->userdata('previous'));
		    }else{
			$data = $this->upload->data();
			$input['attachment'] = $data['file_name'];
		    }
		  
		}
		
		if($this->feedback_db->save(null, $input)){
		    
// 		    print_r($input);
		    $this->session->set_flashdata('success', array('status' => true, 'msg' => 'Terima kasih masukan anda telah kami terima.'));
		}else{
		    //echo $this->db->last_query();
		    $this->session->set_flashdata('success', array('status' => false, 'msg' => 'simpan error'));
		}
	    }
	    redirect($this->session->userdata('previous'));
	}
	
	
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */