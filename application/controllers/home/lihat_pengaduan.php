<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lihat_pengaduan extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','date'));
		$this->load->model(array('admin_handling/region_db', 'admin_handling/content_dinamis_db','admin_handling/content_statis_db', 'admin_handling/content_files_db', 'admin_handling/pengaduan_db', 'select_db', 'admin_handling/respon_db'));
		$this->load->library(array('initlib','session','email'));
		$this->load->database();
		
	}
	
	public function index(){
	    $data['title']= $this->config->item('title').' : Lihat Pengaduan';
	    
	    $data['content'] = 'lihat_pengaduan';
	    $this->load->view('home/main',$data);	
	}
	
	function post_filter(){
	    //print_r($_POST);
	    $data_search = array(
		'type' => $this->input->post('type'),
		'cari' => $this->input->post('cari'),
		'tahun' => $this->input->post('tahun'),
		'provinsi' => $this->input->post('provinsi'),
		'kabkota' => $this->input->post('kabkota'),
		'kategori' => $this->input->post('kategori')
	    );
	    
	    $this->session->set_userdata($data_search);
	    
	    redirect('lihat_pengaduan');
	}
	
	function clear_filter(){
	    $data_search = array(
		'type' => '',
		'cari' => '',
		'tahun' => '',
		'provinsi' => '',
		'kabkota' => '',
		'kategori' => ''
	    );
	    $this->session->unset_userdata($data_search);
	    
	    redirect('lihat_pengaduan');
	}
	
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */