<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH.'/third_party/simple_html_dom/simple_html_dom.php';
require_once(APPPATH.'third_party/recaptcha/recaptchalib.php');

class Home extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->database();
		
		$this->config->load('recaptcha');
		
		session_start();
		
	}
	
	public function index(){
		$data['title']=$this->config->item('title').' : Home';
		
		$data['content'] = 'home';
		$this->load->view('home/main',$data);
		
	}
	
	function halaman($page='home'){
	
	    if($page=='home'){
		redirect('');
	    }
	}
	
// 	function post_usulan_agenda(){
// 	    //print_r($_POST);
// 	    $privatekey = $this->config->item('private_key');
// 	    $resp = recaptcha_check_answer(
// 		$privatekey,
// 		$_SERVER["REMOTE_ADDR"],
// 		$_POST["recaptcha_challenge_field"],
// 		$_POST["recaptcha_response_field"]
// 	    );
// 	    
// 	    
// 	    if(!$resp->is_valid) {
// 		$this->session->set_flashdata('success', array('status' => false, 'msg' => 'kode verifikasi error'));
// 		//echo 'gagal';
// 	    }else{
// 		//print_r($_POST);
// 		$email = $this->input->post('email');
// 		$nama = $this->input->post('nama');
// 		 
// 		$param = array(
// 		    'nama' => $this->input->post('nama'),
// 		    'lembaga' => $this->input->post('lembaga'),
// 		    'alamat' => $this->input->post('alamat'),
// 		    'no_hp' => $this->input->post('no_hp'),
// 		    'email' => $this->input->post('email'),
// 		    'tema_materi' => $this->input->post('tema_materi'),
// 		    'deskripsi_kegiatan' => $this->input->post('deskripsi_kegiatan'),
// 		    'peserta_kegiatan' => $this->input->post('peserta_kegiatan'),
// 		    'usulan_tanggal' => $this->input->post('usulan_tanggal'),
// 		    
// 		);
// 		$pesan = $this->load->view('email/usulan_agenda',$param, true);
// 		$email_to = $this->config->item('email_to');
// 		send_email($from=array('name'=> $this->config->item('title_short'), 'email' => $this->config->item('smtp_user')), $to=array($email_to), $cc=array(), $bcc=array(), $subject= $this->config->item('title_short').' | Usulan Agenda oleh '.$nama.' ('.$email.')', $pesan, $reply_to=array('name'=>$nama, 'email' => $email));
// 		$this->session->set_flashdata('success', array('status' => true, 'msg' => 'pesan terkirim.'));
// 	    
// 		//echo 'sent';
// 	    }
// 	    
// 	    redirect('');
// 	}
// 	
	function post_usulan_agenda($halaman=null){
	    //sukses
	    $input = $this->input->post('agenda');
	    $input['attachment']='';
	   
	    //sukses
	    
	    $input['attachment']='';
	    //upload
	    if(isset($_FILES['file']) && $_FILES['file']['name']){
		$config = array();
		$config['upload_path'] = getcwd().'/media/upload/files/agenda';
		$config['allowed_types'] = 'doc|docx|xls|xlsx|ppt|pptx|pdf|zip|rar|jpg|png|jpeg';
		$config['max_size'] = '51200'; //50MB
		$config['file_name'] = 'usulan_agenda_'.time();
		$config['overwrite'] = true;
		
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('file')){
		    $error = $this->upload->display_errors('','');
		    $this->session->set_flashdata('success', array('status' => false, 'msg' => $error));
		    
		    redirect($this->session->userdata('previous'));
		}else{
		    $data = $this->upload->data();
		    $input['attachment'] = $data['file_name'];
		}
	      
	    }
	    
	    if($this->invitation_db->save(null, $input)){
		
// 		    print_r($input);
		$this->session->set_flashdata('success', array('status' => true, 'msg' => 'sukses'));
	    }else{
		//echo $this->db->last_query();
		$this->session->set_flashdata('success', array('status' => false, 'msg' => 'simpan error'));
	    }
	    redirect($this->session->userdata('previous'));
	}
	
	function post_kehadiran($halaman=null){
	    //sukses
	    $input = $this->input->post('hadir');
	    
	    
	    if($this->participant_db->save(null, $input)){
		
// 		    print_r($input);
		$this->session->set_flashdata('success', array('status' => true, 'msg' => 'sukses'));
	    }else{
		//echo $this->db->last_query();
		$this->session->set_flashdata('success', array('status' => false, 'msg' => 'simpan error'));
	    }
	    redirect($this->session->userdata('previous'));
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */