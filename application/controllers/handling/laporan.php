<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//include(APPPATH.'third_party/html2fpdf/html2fpdf.php');
require_once(APPPATH.'third_party/dompdf/dompdf_config.inc.php');
class Laporan extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper(array('url','date'));
        $this->load->model(array('select_db','admin_handling/region_db','admin_handling/pengaduan_db', 'admin_handling/respon_db','handling/report_db'));
        $this->load->library(array('initlib','session','datatables','email'));
        $this->load->database();
        
	//$this->provinsi=31;
	$this->role_jenjang = $this->session->userdata('role_jenjang');
	$this->role_level = $this->session->userdata('role_level');
	$this->role_program = $this->session->userdata('role_program');
    
	$this->initlib->cek_session_handling();
    }
    function index($kategori=null,$tahun='all', $triwulan='all'){
	//print_r($_SERVER); die();
        
        if(!$kategori)
	    redirect('handling/laporan/index/report_kategori/all/all');
        $user_login=$this->session->userdata('handling');
        $data['title'] = 'Aplikasi '.$this->config->item('title').' : Laporan';
        $data['module']='mod_laporan';
	
	if(!$this->session->userdata('filter_laporan')){
	    if($this->role_level->level_id == 1){
		$filter['provinsi'] = 'all';
		$filter['kabkota'] = 'all';
	    }elseif($this->role_level->level_id == 2){
		$filter['provinsi'] = $this->role_level->provinsi_id;
		$filter['kabkota'] = 'all';
	    }elseif($this->role_level->level_id == 3){
		$filter['provinsi'] = $this->role_level->provinsi_id;
		$filter['kabkota'] = $this->role_level->kabkota_id;
	    }
	    
	    $this->session->set_userdata('filter_laporan', $filter);
	}
        $this->load->view('handling/main_view',$data);
    }
    
    function post_laporan(){
	$kategori = $this->input->post('kategori');
	$tahun = $this->input->post('tahun');
	$triwulan = $this->input->post('triwulan');
	
	$filter = array(
	    'provinsi' => $this->input->post('provinsi'),
	    'kabkota' => $this->input->post('kabkota')
	);
	
	$this->session->set_userdata('filter_laporan', $filter);
	redirect('handling/laporan/index/'.$kategori.'/'.$tahun.'/'.$triwulan);
    }
    
    function rekap($format='pdf'){
	//$this->load->view('handling/rekap_table_view');
	
	
	if(strtolower($format)=='pdf'){
	    $html = $this->load->view('handling/rekap_table_view','',true);
	    $dompdf = new DOMPDF();
	    $dompdf->set_paper("A4");
    
	    // load the html content
	    $dompdf->load_html($html);
	    $dompdf->render();
	    $canvas = $dompdf->get_canvas();
	    $font = Font_Metrics::get_font("helvetica", "bold");
	    $canvas->page_text(16, 800, "Page: {PAGE_NUM} of {PAGE_COUNT}", $font, 8, array(0,0,0));
	    $dompdf->stream("report.pdf",array("Attachment"=>0));
	
	}elseif(strtolower($format)=='excel'){
	    header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
	    header("Content-Disposition: attachment; filename=report.xls");  //File name extension was wrong
	    header("Expires: 0");
	    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	    header("Cache-Control: private",false);
	    $this->load->view('handling/rekap_table_view');
	}
	
    }
}

?>