<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Manual extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper(array('url','date'));
        $this->load->model(array('select_db','admin_handling/region_db','admin_handling/pengaduan_db', 'admin_handling/respon_db'));
        $this->load->library(array('initlib','session','datatables','email'));
        $this->load->database();
        
	$this->provinsi=31;
	$this->role_jenjang = $this->session->userdata('role_jenjang');
	$this->role_level = $this->session->userdata('role_level');
	$this->role_program = $this->session->userdata('role_program');
    
	$this->initlib->cek_session_handling();
    }
    function index(){
	//print_r($_SERVER); die();
        
        $user_login=$this->session->userdata('handling');
        $data['title'] = 'Aplikasi '.$this->config->item('title').' : Manual Aplikasi';
        $data['module']='mod_manual';
        
        $this->load->view('handling/main_view',$data);
    }
    
    
    
}

?>