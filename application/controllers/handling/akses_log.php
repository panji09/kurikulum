<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Akses_log extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper(array('url','date'));
        $this->load->model(array('select_db','admin_handling/region_db','admin_handling/pengaduan_db','admin_handling/user_handling_db', 'admin_handling/respon_db'));
        $this->load->library(array('initlib','session','datatables','email'));
        $this->load->database();
        
	$this->role_jenjang = $this->session->userdata('role_jenjang');
	$this->role_level = $this->session->userdata('role_level');
	$this->role_program = $this->session->userdata('role_program');
    
	$this->initlib->cek_session_handling();
    }
    function index(){
	$user_login=$this->session->userdata('handling');
        $data['title'] = 'Aplikasi '.$this->config->item('title').' : Akses Log';
        $data['module']='mod_akses_log';
        
        $this->load->view('handling/main_view',$data);
    }
    
    function post_akses_log(){
	$filter=array(
            'provinsi' => $this->input->post('provinsi'),
            'kabkota' => $this->input->post('kabkota')
        );
        
        $this->session->set_flashdata('filter_akses_log', $filter);
        
        redirect('handling/akses_log');
    }
    
    
}

?>