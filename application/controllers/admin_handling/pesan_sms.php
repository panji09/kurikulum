<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pesan_sms extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    
    function __construct(){
        parent::__construct();
        $this->load->helper(array('init', 'url','date'));
        $this->load->library(array('session','initlib','datatables'));
        $this->load->model(array('select_db', 'admin_handling/content_statis_db', 'admin_handling/content_dinamis_db', 'admin_handling/content_files_db', 'admin_handling/pengaduan_db', 'admin_handling/user_admin_db', 'admin_handling/gateway_sms_db'));
        $this->load->database();
        
	$this->initlib->cek_session_admin();
    }
    
    function index(){
	$data['title'] = 'Pesan SMS';
	$data['module'] = 'pesan_sms';
	$data['load_url'] = site_url('admin_handling/pesan_sms/ajax_pesan_sms');
	$this->load->view('admin_handling/main',$data);
	//redirect('admin_handling/pengaduan');
    }
    
    /*
    function ajax_pesan_sms(){
	$this->datatables->select('
	    a.id as id,
	    b.nama,
	    a.msisdn as msisdn,
	    a.pesan as pesan,
	    a.tanggal as tanggal,
	    if(a.published = 1,"ya","tidak") as tampilkan,
	    if(a.approved = 1,"ya","tidak") as verifikasi,
	    a.id as action
	',false);
	
	$this->datatables->from('pesan_sms as a');
	$this->datatables->join('operator as b','a.operator_id = b.id','left');
	
	$this->datatables->where('a.deleted',0);
	
	//$this->datatables->where('a.deleted',0);
	
	$this->datatables->edit_column('pesan','<div class="expander">$1</div>','pesan');
	$this->datatables->edit_column('tanggal','$1','mysqldatetime_to_date(tanggal,"d/m/Y, H:i:s")');
	
	$this->datatables->edit_column(
	    'action',
	    '
	    <div class="btn-group btn-group-sm" style="min-width: 100px">
		<button type="button" class="btn btn-default btn_edit" data-id=$1 data-approved=$4 data-published=$5 data-action="$2/$1" data-toggle="modal" data-target="#modal_approved">Terima</button>
		<button type="button" class="btn btn-default btn_delete" data-action="$3/$1" data-toggle="modal" data-target="#modal_delete_reason">Tolak</button>
	    </div>',
	    'id, site_url("admin_handling/pesan_sms/approved"), site_url("admin_handling/pesan_sms/delete"), verifikasi, tampilkan'
	);
	//echo $this->db->last_query();
	echo $this->datatables->generate();
    }
    */
    function ajax_pesan_sms(){
	$this->datatables->select('
	    a.id as id,
	    a.kode_pengaduan as kode_pengaduan,
	    a.tanggal as tanggal,
	    i.name as ditujukan,
	    k.name as operator,
	    b.handphone as msisdn,
	    h.name as kategori,
	    g.name as peran,
	    group_concat(distinct m.name separator ",") as isu,
	    a.deskripsi as deskripsi,
	    if(a.published = 1,"ya","tidak") as tampilkan,
	    a.approved as approved,
	    a.id as action
	',false);
	
	$this->datatables->from('pengaduan as a');
	$this->datatables->join('person as b','a.person_id = b.id','left');
	$this->datatables->join('operator as k','b.operator_id = k.id','left');
	$this->datatables->join('kecamatan as c','a.kecamatan_id = c.id','left');
	$this->datatables->join('kabkota as d','a.kabkota_id = d.id','left');
	$this->datatables->join('provinsi as e','a.provinsi_id = e.id','left');
	$this->datatables->join('jenjang as f','a.jenjang_id = f.id','left');
	$this->datatables->join('sumber as g','a.sumber_id = g.id','left');
	$this->datatables->join('kategori as h','a.kategori_id = h.id','left');
	$this->datatables->join('level as i','a.level_id = i.id','left');
	$this->datatables->join('media as j','a.media_id = j.id','left');
	$this->datatables->join('pengaduan_isu as l', 'l.pengaduan_id = a.id', 'left');
	$this->datatables->join('isu as m', 'm.id = l.isu_id', 'left');
	
	$this->datatables->where('a.deleted',0);
	$this->datatables->where('a.media_id',2); //pengaduan sms
	
	$this->datatables->group_by('a.id','DESC');
	
	$this->datatables->edit_column('deskripsi','<div class="expander">$1</div>','deskripsi');
	$this->datatables->edit_column('tanggal','$1','mysqldatetime_to_date(tanggal,"d/m/Y, H:i:s")');
	$this->datatables->edit_column('approved','$1','check(approved)');
	
	$this->datatables->edit_column(
	    'action',
	    '
	    <div class="btn-group btn-group-sm" style="min-width: 100px">
		<button type="button" class="btn btn-default btn_edit" data-id=$1 data-approved=$4 data-published=$5 data-action="$2/$1" data-toggle="modal" data-target="#modal_approved">Terima</button>
		<button type="button" class="btn btn-default btn_delete" data-action="$3/$1" data-toggle="modal" data-target="#modal_delete_reason">Tolak</button>
	    </div>',
	    'id, site_url("admin_handling/pesan_sms/approved"), site_url("admin_handling/pesan_sms/delete"), verifikasi, tampilkan'
	);
	echo $this->datatables->generate();
    }
    
    function load_pengaduan($id){
	$param['id'] = $id;
	$this->load->view('admin_handling/inc/content/pesan_sms_show.php',$param);
    }
    
    function approved($pengaduan_id){
	$data_pengaduan = array(
	    'level_id' => 1, //pusat
	    'sumber_id' => $this->input->post('sumber_info'),
	    'sumber_lain' => ($this->input->post('sumber_info')==9999 ? htmlentities($this->input->post('sumber_lain')) : null),
	    'kategori_id' => $this->input->post('kategori'),
	    'approved' => 1,
	    'published' => ($this->input->post('published') ? 1 : 0)
	);
	
	
	$data_isu = $this->input->post('isu');
	
	if($this->pengaduan_db->approved($pengaduan_id, $data_pengaduan, $data_isu)){
	    $query = $this->pengaduan_db->get($pengaduan_id);
	    
            $this->session->set_flashdata('success', array('status' => true, 'msg' => 'sukses'));
        }else{
            $this->session->set_flashdata('success', array('status' => false, 'msg' => 'gagal'));
        }
	redirect('admin_handling/pesan_sms');
    }
    
    function delete($pengaduan_id){
	$data_pengaduan = array(
	    'deleted' => 1,
	    'reason_deleted' => $this->input->post('reason_deleted')
	);
	
	if($this->pengaduan_db->save_pengaduan($pengaduan_id, $data_pengaduan)){
            $this->session->set_flashdata('success', array('status' => true, 'msg' => 'sukses'));
        }else{
            $this->session->set_flashdata('success', array('status' => false, 'msg' => 'gagal'));
        }
	redirect('admin_handling/pesan_sms');
    }
    
    function fix_table(){
	$sms = $this->gateway_sms_db->get_all();
	//echo $this->db->last_query();
	foreach($sms->result() as $row){
	    $url = 'http://pengaduan.kemdikbud.go.id/admin_handling/gateway_sms/post_pengaduan';
	    //$url = site_url('admin_handling/gateway_sms/post_pengaduan');
	    $input_data = array(
		'operator_id' => $row->operator_id, 
		'msisdn' => $row->msisdn,
		'pesan' => $row->pesan,
		'tanggal' => $row->tanggal
	    );
	    
	    file_get_contents($url.'/'.base64_encode(json_encode($input_data)));
	    /*
	    $input['param'] = json_encode($input_data);
	    
	    // use key 'http' even if you send the request to https://...
	    
	    $options = array(
		'http' => array(
		    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
		    'method'  => 'POST',
		    'content' => http_build_query($input),
		),
	    );
	    $context  = stream_context_create($options);
	    $result = file_get_contents($url, false, $context);
	    */
	    echo "OK\n";
	    //sleep(1);
	}
    }
}
 ?>
