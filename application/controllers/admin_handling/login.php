<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    
    function __construct(){
        parent::__construct();
        //$this->load->model(array('_TEMPLATE_db'));
	
    }
    
    function index(){
	$this->load->view('admin_handling/login');
    }
    
    function post_login(){
	$data['username']=$this->input->post('username');
	$data['password']=$this->input->post('password');
	
	if($user_login=$this->login_db->login_admin($data)){
	    $user_login['user_id'] = $user_login['_id']->{'$id'};
	    unset($user_login['_id']);
	    unset($user_login['log_last_login']);
	    unset($user_login['log_last_update']);
	    $this->session->set_userdata('admin',$user_login);
	    $this->session->set_userdata('admin_logged_in',true);    
	    redirect('admin_handling/admin');
	}else{
	    redirect('admin_handling/login');
	}
    }
    
    function logout(){
        $this->session->unset_userdata('admin');
	$this->session->unset_userdata('admin_logged_in');
	unset($_SESSION['KCFINDER']);
	redirect('');
    }
}
 ?>
