<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Dashboard</title>

    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-TileColor" content="#5bc0de">
    <meta name="msapplication-TileImage" content="<?=$this->config->item('home_img')?>/logo_kurikulum_2013.png">

    <!--SCRIPT HEADER-->
    <?=$this->load->view('admin_handling/inc/script_header')?>
    <!--./SCRIPT HEADER-->
  </head>
  <body>
    <div id="wrap">
      <div id="top">

        <!--TOP BAR-->
	<?=$this->load->view('admin_handling/inc/top_bar')?>
	<!--./TOP BAR-->

        <!--HEADER TITLE-->
	<?=$this->load->view('admin_handling/inc/header_title')?>
	<!--./HEADER TITLE-->
      </div><!-- /#top -->
      <!--LEFT MENU-->
      <?=$this->load->view('admin_handling/inc/left_menu')?>
      <!--./LEFT MENU-->
      <div id="content">
        <div class="outer">
          <div class="inner">
            <!--CONTENT-->
	    <?=$this->load->view('admin_handling/inc/content/'.$module)?>
	    <!--./CONTENT-->

          <!-- end .inner -->
        </div>

        <!-- end .outer -->
      </div>

      <!-- end #content -->
    </div><!-- /#wrap -->
    <!--FOOTER-->
    <?=$this->load->view('admin_handling/inc/footer')?>
    <!--./FOOTER-->

    <!--MODAL-->
    <?=$this->load->view('admin_handling/inc/modal_main')?>
    <?=$this->load->view('admin_handling/inc/modal')?>
    <!--./MODAL-->
    
    <!--SCRIPT FOOTER-->
    <?=$this->load->view('admin_handling/inc/script_footer')?>
    <!--./SCRIPT FOOTER-->
  </body>
</html>