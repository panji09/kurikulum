<!-- #helpModal -->
    <div id="helpModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Modal title</h4>
          </div>
          <div class="modal-body">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
              in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal --><!-- /#helpModal -->
    
    <!-- delete -->
    <div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
	  </div>
	  <div class="modal-body">
	    Apakah ingin menghapus data ini?
	  </div>
	  <div class="modal-footer">
	    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
	    <a href="" id='link_delete' class="btn btn-primary">Hapus</a>
	  </div>
	</div>
      </div>
    </div>
    <!-- ./delete -->
    
    <!-- delete reason -->
    <div class="modal fade" id="modal_delete_reason" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
	  </div>
	  <form method='post' action='' role="form">
	  <div class="modal-body">
	      <div class="form-group">
		  <label for="">Alasan?</label>
		  <textarea class="form-control" name='reason_deleted'></textarea>
	      </div>
	  </div>
	  <div class="modal-footer">
	    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
	    <input type="submit" class="btn btn-primary" value='Hapus'>
	  </div>
	  </form>
	</div>
      </div>
    </div>
    <!-- ./delete reason -->
    
    <!-- verifikasi pengaduan -->
    <div class="modal fade" id="modal_approved" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    <h4 class="modal-title" id="myModalLabel">Terima</h4>
	  </div>
	  <form method='post' action='' role="form" class="form-horizontal">
	  <div class="modal-body">
	      
	      
	  </div>
	  <div class="modal-footer">
	    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
	    <input type="submit" class="btn btn-primary" value='Terima'>
	  </div>
	  </form>
	</div>
      </div>
    </div>
    <!-- ./delete reason -->