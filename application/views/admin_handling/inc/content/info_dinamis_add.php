<?php
$uri3 = $this->uri->segment(3);
$this->session->set_userdata('redirect','admin_handling/halaman/'.$this->uri->segment(3));

if(isset($id)){
    $query = $this->dynamic_pages_db->get($id);
    
    if($query)
	$content = $query[0];
}else{
    $id = '';
}

$page_id = $uri3;
?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="box">
                        <header class="dark">
                            <div class="icons">
                                <i class="fa fa-ok"></i>
                            </div>
                            <h5><?=$title?></h5>
                            <div class="toolbar">
                              <ul class="nav">
                                <li>
                                  <div class="btn-group">
                                    <a class="accordion-toggle btn btn-xs minimize-box" data-toggle="collapse" href="#collapse2">
                                      <i class="fa fa-chevron-up"></i>
                                    </a>
                                  </div>
                                </li>
                              </ul>
                            </div>
                        </header>
                        <div id="collapse2" class="body collapse in">
                            <form id='form_content' role="form" action='<?=site_url('admin_handling/halaman/post_info_dinamis/'.$page_id.'/'.$id)?>' method='post'>
                                <div class="form-group">
                                  <label for="">Judul</label>
                                  <input type="text" name='content[title]' class="form-control" id="" placeholder="Judul" value='<?=(isset($content['title']) ? $content['title'] : '')?>'>
                                </div>
                                <div class="form-group">
                                  <label for="">Isi</label>
                                  <textarea class='form-control ckeditor' name='content[content]' placeholder='Isi'><?=(isset($content['content']) ? $content['content'] : '')?></textarea>
                                </div>
                                
                                
                                <div class="checkbox">
                                  <label>
                                    <input type="checkbox" name='content[published]' value='1' <?=(isset($content['published']) && $content['published'] ? 'checked' : '')?>> Terbitkan
                                  </label>
                                </div>
                                <div id='hide_input' style='display:none'>
                                </div>
                                <button type="submit" id='submit' class="btn btn-default">Submit</button>
                            </form>
                            <div>&nbsp</div>
                            
                            
                        </div>
                    </div>
                </div>
            </div>