            <div class="row">
                <div class="col-lg-12">
                    <div class="box">
                        <header class="dark">
                            <div class="icons">
                                <i class="fa fa-ok"></i>
                            </div>
                            <h5><?=$title?></h5>
                            <div class="toolbar">
                              <ul class="nav">
                                <li>
                                  <div class="btn-group">
                                    <a class="accordion-toggle btn btn-xs minimize-box" data-toggle="collapse" href="#collapse2">
                                      <i class="fa fa-chevron-up"></i>
                                    </a>
                                  </div>
                                </li>
                              </ul>
                            </div>
                        </header>
                        <div id="collapse2" class="body collapse in">
                            <?php
                                $notif=$this->session->flashdata('success');
                                if($notif):
                            ?>
                            <div class="alert <?=($notif['status'] ? 'alert-success' : 'alert-danger')?>"><?=$notif['msg']?></div>
                            <?php endif;?>
                            <div class="form-group">
                                <button class="btn btn-primary" onclick="location.href='<?=site_url('admin_handling/halaman/'.$this->uri->segment(3).'/add')?>'">Tambah</button>
                                
                            </div>
                            
                            <table id="info_dinamis" class="table responsive table-bordered table-condensed table-hover table-striped">
                                <thead>
                                    <tr>
                                        
                                        <th>Judul</th>
                                        <th>Tanggal Buat</th>
                                        <th>Tanggal Update</th>
                                        <th>Author</th>
                                        <th>Terbitkan</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
				    foreach($this->dynamic_pages_db->get_all(array('page_id' =>  $this->uri->segment(3))) as $row):
                                ?>
				    <tr>
                                        
                                        <td><?=$row['title']?></td>
                                        <td><time class="" datetime="<?=date('c',$row['created'])?>"><?=time_ago($row['created'])?></time></td>
                                        <td><time class="" datetime="<?=date('c',$row['last_update']['time'])?>"><?=time_ago($row['last_update']['time'])?></time></td>
                                        
                                        <td><?=$row['author']['fullname']?></td>
                                        <td><?=($row['published'] ? 'Ya' : 'Tidak')?></td>
                                        <td>
					    <div class="btn-group btn-group-sm" style="min-width: 100px">
					      <a href="<?=site_url('admin_handling/halaman/'.$this->uri->segment(3).'/edit/'.$row['_id']->{'$id'})?>" class="btn btn-default" >Edit</a>
					      <button type="button" class="btn btn-default btn_delete" data-href="<?=site_url('admin_handling/halaman/'.$this->uri->segment(3).'/delete/'.$row['_id']->{'$id'})?>" data-toggle="modal" data-target="#modal_delete">Hapus</button>
					    </div>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>    
                        </div>
                    </div>
                </div>
            </div>