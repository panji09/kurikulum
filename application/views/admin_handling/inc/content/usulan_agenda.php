            <div class="row">
                <div class="col-lg-12">
                    <div class="box">
                        <header class="dark">
                            <div class="icons">
                                <i class="fa fa-ok"></i>
                            </div>
                            <h5><?=$title?></h5>
                            <div class="toolbar">
                              <ul class="nav">
                                <li>
                                  <div class="btn-group">
                                    <a class="accordion-toggle btn btn-xs minimize-box" data-toggle="collapse" href="#collapse2">
                                      <i class="fa fa-chevron-up"></i>
                                    </a>
                                  </div>
                                </li>
                              </ul>
                            </div>
                        </header>
                        <div id="collapse2" class="body collapse in">
                            <?php
                                $notif=$this->session->flashdata('success');
                                if($notif):
                            ?>
                            <div class="alert <?=($notif['status'] ? 'alert-success' : 'alert-danger')?>"><?=$notif['msg']?></div>
                            <?php endif;?>
                            
                            <table id="info_dinamis" class="table responsive table-bordered table-condensed table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>Tanggal</th>
                                        <th>Nama</th>
                                        <th>Lembaga</th>
                                        <th>Tema</th>
                                        <th>File</th>
                                        <th>Deskripsi</th>
                                        <th>Lainnya</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
				    if($invitation):
				    foreach($invitation as $row):
                                ?>
				    <tr>
                                        <td><time class="" datetime="<?=date('c',$row['created'])?>"><?=time_ago($row['created'])?></time></td>
                                        <td><?=(isset($row['nama']) ? $row['nama'] : '')?></td>
                                        <td><?=(isset($row['lembaga']) ? $row['lembaga'] : '')?></td>
                                        <td><?=(isset($row['tema_materi']) ? $row['tema_materi'] : '')?></td>
                                        
                                        <td><?=(isset($row['attachment']) ? '<a target="_blank" href='.$this->config->item('upload').'/files/agenda/'.$row['attachment'].'>'.$row['attachment'].'</a>' : '')?></td>
                                        <td><div class="expander"><?=(isset($row['deskripsi_kegiatan']) ? nl2br($row['deskripsi_kegiatan']) : '')?></div></td>
                                        
                                        <td>
                                        <?php 
					    $detail = $row;
					    unset($detail['_id']);
					    unset($detail['deleted']);
					    unset($detail['last_update']);
					    unset($detail['log_last_update']);
					    
					    unset($detail['created']);
					    unset($detail['nama']);
					    unset($detail['lembaga']);
					    unset($detail['tema_materi']);
					    unset($detail['attachment']);
					    unset($detail['deskripsi']);
					    
					    
// 					    print_r($detail);
					?>
					
					
					<?php foreach($detail as $key=>$val):?>
					    <strong><?=$key?>:</strong> <?=$val?><br>
					    
					<?php endforeach;?>
					
                                        </td>
                                    </tr>
                                <?php 
				    endforeach;
				    endif;
                                
                                ?>
                                </tbody>
                            </table>    
                        </div>
                    </div>
                </div>
            </div>