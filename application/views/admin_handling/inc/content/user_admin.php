            <div class="row">
                <div class="col-lg-12">
                    <div class="box">
                        <header class="dark">
                            <div class="icons">
                                <i class="fa fa-ok"></i>
                            </div>
                            <h5><?=$title?></h5>
                            <div class="toolbar">
                              <ul class="nav">
                                <li>
                                  <div class="btn-group">
                                    <a class="accordion-toggle btn btn-xs minimize-box" data-toggle="collapse" href="#collapse2">
                                      <i class="fa fa-chevron-up"></i>
                                    </a>
                                  </div>
                                </li>
                              </ul>
                            </div>
                        </header>
                        <div id="collapse2" class="body collapse in">
                            <?php
                                $notif=$this->session->flashdata('success');
                                if($notif):
                            ?>
                            <div class="alert <?=($notif['status'] ? 'alert-success' : 'alert-danger')?>"><?=$notif['msg']?></div>
                            <?php endif;?>
                            <div class="form-group">
                                <button class="btn btn-primary" onclick="location.href='<?=site_url('admin_handling/user_admin/add')?>'">Tambah</button>
                                
                            </div>
                            
                            <table id="user_handling" class="table responsive table-bordered table-condensed table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>Nama Lengkap</th>
                                        <th>Username</th>
                                        <th>Aktif</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach($this->user_db->get_all() as $row):?>
				    <tr>
                                        <td><?=$row['fullname']?></td>
                                        <td><?=$row['username']?></td>
                                        <td><?=($row['activated'] ? 'Ya' : 'Tidak')?></td>
                                        <td>
					    <div class="btn-group btn-group-sm" style="min-width: 100px">
					      <a href="<?=site_url('admin_handling/user_admin/edit/'.$row['_id']->{'$id'})?>" class="btn btn-default" >Edit</a>
					      <button type="button" class="btn btn-default btn_delete" data-href="<?=site_url('admin_handling/user_admin/delete/'.$row['_id']->{'$id'})?>" data-toggle="modal" data-target="#modal_delete">Hapus</button>
					  </div>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>    
                        </div>
                    </div>
                </div>
            </div>