<?php
$this->session->set_userdata('redirect','admin_handling/'.$this->uri->segment(2));

if(isset($user_id)){
    $query = $this->user_db->get($user_id);
    
    if($query)
	$user = $query[0];
}else{
    $user_id = '';
}
?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="box">
                        <header class="dark">
                            <div class="icons">
                                <i class="fa fa-ok"></i>
                            </div>
                            <h5><?=$title?></h5>
                            <div class="toolbar">
                              <ul class="nav">
                                <li>
                                  <div class="btn-group">
                                    <a class="accordion-toggle btn btn-xs minimize-box" data-toggle="collapse" href="#collapse2">
                                      <i class="fa fa-chevron-up"></i>
                                    </a>
                                  </div>
                                </li>
                              </ul>
                            </div>
                        </header>
                        <div id="collapse2" class="body collapse in">
			    <form id="form" method="post" action="<?=site_url('admin_handling/user_admin/post_add/'.$user_id)?>" role='form' class="form">
				<div class="form-group">
				  <label>Nama Lengkap</label>
				  <input type="text" class="form-control" id="fullname" name='user[fullname]' value='<?=(isset($user['fullname']) ? $user['fullname'] : '')?>' placeholder="nama Lengkap" required>
				</div>
				<div class="form-group">
				  <label>Username</label>
				  <input type="text" class="form-control" id="username" name='user[username]' placeholder="Username" value='<?=(isset($user['username']) ? $user['username'] : '')?>' required>
				</div>
				<div class="form-group">
				  <label>Password</label>
				  <input type="password" class="form-control" name='user[password]' placeholder="Password"  <?=($this->uri->segment(3) == 'add' ? 'required' : '')?>>
				</div>
				<div class="form-group">
				  <label>Email</label>
				  <input type="email" class="form-control" id="email" name='user[email]' value='<?=(isset($user['email']) ? $user['email'] : '')?>' placeholder="Email" required>
				</div>
				<div class="checkbox">
				  <label>
				    <input type="checkbox" value='1' name='user[activated]' <?=(isset($user['activated']) && $user['activated'] ? 'checked' : '')?>> aktif
				  </label>
				</div>
				<button type="submit" class="btn btn-default">Proses</button>
			    </form>
                        </div>
                    </div>
                </div>
            </div>