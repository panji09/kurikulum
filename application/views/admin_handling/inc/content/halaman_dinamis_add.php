<?php $this->session->set_userdata('redirect','admin_handling/halaman/'.$this->uri->segment(3));?>
<?php $category = (isset($dinamis->category) && $dinamis->category ? explode(',',$dinamis->category) : array()); //print_r($category)?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="box">
                        <header class="dark">
                            <div class="icons">
                                <i class="fa fa-ok"></i>
                            </div>
                            <h5><?=$title?></h5>
                            <div class="toolbar">
                              <ul class="nav">
                                <li>
                                  <div class="btn-group">
                                    <a class="accordion-toggle btn btn-xs minimize-box" data-toggle="collapse" href="#collapse2">
                                      <i class="fa fa-chevron-up"></i>
                                    </a>
                                  </div>
                                </li>
                              </ul>
                            </div>
                        </header>
                        <div id="collapse2" class="body collapse in">
                            <form id='form_content' role="form" action='<?=site_url('admin_handling/halaman/post_content_dinamis/'.$content_kategori_id.'/'.$id)?>' method='post'>
                                <div class="form-group">
                                  <label for="">Judul</label>
                                  <input type="text" name='title' class="form-control" id="" placeholder="Judul" value='<?=$dinamis->title?>'>
                                </div>
                                
                                <?php if($content_kategori_id == 5): // if agenda 5?>
                                <div class="form-group">
				    <div class="row">
					<div class="col-lg-6">
					    <div class="form-group">
						<label for="">Dari Tanggal</label>
						<div class="input-group date">
						    <input type="text" class="form-control" placeholder='Dari Tanggal' name='agenda_from' value='<?=(isset($dinamis->agenda_from) ? mysqldatetime_to_date($dinamis->agenda_from,"d/m/Y H:i") : date("d/m/Y H:i",time()));?>'> <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
						</div>
					    </div>
					</div>
					<div class="col-lg-6">
					    <div class="form-group">
						<label for="">Sampai Tanggal</label>
						<div class="input-group date">
						    <input type="text" class="form-control" placeholder='Sampai Tanggal' name='agenda_to' value='<?=(isset($dinamis->agenda_to) ? mysqldatetime_to_date($dinamis->agenda_to,"d/m/Y H:i") : date("d/m/Y H:i",time()));?>'> <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
						</div>
					    </div>
					</div>
				    </div>
				</div>
                                <?php endif; // end if kategori agenda 5?>
                                
                                <?php if($content_kategori_id == 7): // if referensi buku 6?>
                                <div class="form-group">
				    <div class="row">
					<div class="col-lg-4">
					    <div class="form-group">
						<label for="">Buku</label>
						<select class="form-control" name='category[]'>
						    <option value="null">-pilih-</option>
						    <?php
							$kategori_referensi_buku = $this->select_db->kategori_referensi_buku();
							foreach($kategori_referensi_buku->result() as $row):
						    ?>
						    <option value='<?=$row->id?>' <?=(isset($category[0]) && $category[0] == $row->id ? 'selected' : '')?>><?=$row->name?></option>
						    
						    <?php endforeach;?>
						</select>
					    </div>
					</div>
					<div class="col-lg-4">
					    <div class="form-group">
						<label for="">Jenjang Pendidikan</label>
						<select class="form-control" id="jenjang" name='category[]'>
						    <option value="null">-pilih-</option>
						<?php
						    $jenjang = $this->select_db->jenjang(array('in_id'=> array(1,2,3)));
						    foreach($jenjang->result() as $row):
						?>
						    <option value="<?=$row->id?>" <?=(isset($category[1]) && $category[1] == $row->id ? 'selected' : '')?>><?=$row->desc_name?></option>
						    
						<?php endforeach;?>
						</select>
					    </div>
					</div>
					<div class="col-lg-4">
					    <div class="form-group">
						<label for="">Mata Pelajaran</label>
						<select class="form-control" id='mata_pelajaran' name='category[]'>
						    <option value="null">-pilih-</option>
						    <?php if(isset($category[1]) && $category[1]!='null'):?>
							<?php foreach($this->select_db->kategori_mata_pelajaran(array('jenjang_id' => $category[1]))->result() as $row):?>
							    <option value="<?=$row->id?>" <?=(isset($category[2]) && $category[2] == $row->id ? 'selected' : '')?>><?=$row->name?></option>
							<?php endforeach;?>
						    <?php endif;?>
						</select>
					    </div>
					</div>
				    </div>
				</div>
                                <?php endif; // end if kategori referensi buku 6?>
                                
                                <?php if($content_kategori_id == 8): // if referensi standar pendidikan 7?>
                                <div class="form-group">
				    <div class="row">
					<div class="col-lg-12">
					    <div class="form-group">
						<label for="">Standar Pendidikan</label>
						<select class="form-control" name='category[]'>
						    <option value="null">-pilih-</option>
						    <?php
							$kategori_standar_pendidikan = $this->select_db->kategori_standar_pendidikan();
							foreach($kategori_standar_pendidikan->result() as $row):
						    ?>
							<option value="<?=$row->id?>" <?=(isset($category[0]) && $category[0] == $row->id ? 'selected' : '')?>><?=$row->name?></option>
							
						    <?php endforeach;?>
						</select>
					    </div>
					</div>
					
				    </div>
				</div>
                                <?php endif; // end if kategori referensi buku 7?>
                                
                                <?php if($content_kategori_id == 9): // if referensi kerangka dasar dan struktur 8?>
                                <div class="form-group">
				    <div class="row">
					<div class="col-lg-6">
					    <div class="form-group">
						<label for="">Jenjang Pendidikan</label>
						<select class="form-control" id="jenjang" name='category[]'>
						    <option value="null">-pilih-</option>
						    <?php
							$jenjang = $this->select_db->jenjang(array('in_id'=> array(1,2,3)));
							foreach($jenjang->result() as $row):
						    ?>
							<option value="<?=$row->id?>" <?=(isset($category[0]) && $category[0] == $row->id ? 'selected' : '')?>><?=$row->desc_name?></option>
							
						    <?php endforeach;?>
						</select>
					    </div>
					</div>
					<div class="col-lg-6">
					    <div class="form-group">
						<label for="">Mata Pelajaran</label>
						<select class="form-control" id='mata_pelajaran' name='category[]'>
						    <option value="null">-pilih-</option>
						    <?php if(isset($category[0]) && $category[0]!='null'):?>
							<?php foreach($this->select_db->kategori_mata_pelajaran(array('jenjang_id' => $category[0]))->result() as $row):?>
							    <option value="<?=$row->id?>" <?=(isset($category[1]) && $category[1] == $row->id ? 'selected' : '')?>><?=$row->name?></option>
							<?php endforeach;?>
						    <?php endif;?>
						</select>
					    </div>
					</div>
				    </div>
				</div>
                                <?php endif; // end if kategori referensi kerangka dasar dan struktur 8?>
                                
                                
                                <div class="form-group">
                                  <label for="">Isi</label>
                                  <textarea class='form-control ckeditor' name='content' placeholder='Isi'><?=$dinamis->content?></textarea>
                                </div>
                                
                                <?php if($content_kategori_id == 5 || $content_kategori_id == 1): // if agenda 5 dan berita 1?>
                                <div class="form-group">
                                  <label for="">Tags</label>
                                  <em>pisahkan dengan koma(,)</em><br>
                                  <input type="text" value="<?=$dinamis->tags?>" class="form-control" id='tags' data-role="tagsinput" placeholder="Tags" name="tags"/>
				  
                                </div>
                                <?php endif; //end if berita dan agenda?>
                                <div class="checkbox">
                                  <label>
                                    <input type="checkbox" name='published' value='1' <?=($dinamis->published ? 'checked' : '')?>> Terbitkan
                                  </label>
                                </div>
                                <div id='hide_input' style='display:none'>
                                </div>
                                <button type="submit" id='submit' class="btn btn-default">Submit</button>
                            </form>
                            <div>&nbsp</div>
                            
                            <?php /*
                            <form role='form' id="fileupload" data-url="<?=site_url('admin_handling/halaman/post_upload')?>" method="POST" enctype="multipart/form-data">
				<div class="form-group">
				  <!-- Redirect browsers with JavaScript disabled to the origin page -->
				  <noscript><input type="hidden" name="redirect" value="http://blueimp.github.io/jQuery-File-Upload/"></noscript>
				  <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
				  <div class="row fileupload-buttonbar">
				      <div class="col-lg-7">
					  <!-- The fileinput-button span is used to style the file input field as button -->
					  <span class="btn btn-success fileinput-button">
					      <i class="glyphicon glyphicon-plus"></i>
					      <span>Add files...</span>
					      <input type="file" name="files[]" multiple>
					  </span>
					  <button type="submit" class="btn btn-primary start">
					      <i class="glyphicon glyphicon-upload"></i>
					      <span>Start upload</span>
					  </button>
					  <button type="reset" class="btn btn-warning cancel">
					      <i class="glyphicon glyphicon-ban-circle"></i>
					      <span>Cancel upload</span>
					  </button>
					  <button type="button" class="btn btn-danger delete">
					      <i class="glyphicon glyphicon-trash"></i>
					      <span>Delete</span>
					  </button>
					  <input type="checkbox" class="toggle">
					  <!-- The global file processing state -->
					  <span class="fileupload-process"></span>
				      </div>
				      <!-- The global progress state -->
				      <div class="col-lg-5 fileupload-progress fade">
					  <!-- The global progress bar -->
					  <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
					      <div class="progress-bar progress-bar-success" style="width:0%;"></div>
					  </div>
					  <!-- The extended global progress state -->
					  <div class="progress-extended">&nbsp;</div>
				      </div>
				  </div>
				  <!-- The table listing the files available for upload/download -->
				  <table role="presentation" class="table table-striped">
				    <tbody class="files">
					<?php if($id):?>
					<?php
					    $files=$this->content_files_db->get($id);
					    
					    if($files->num_rows())
					    foreach($files->result() as $row):
					?>
					
					<tr class="template-download fade in">
					    <td>
						<span class="preview">
						    
						</span>
					    </td>
					    <td>
						<p class="name"> 
							<a download="<?=$row->file?>" title="<?=$row->file?>" href="<?=base_url('media/upload/files/'.$row->file)?>"><?=$row->file?></a>
						</p>
						
						    <p class="name">
						      <input type="text" value="<?=$row->file?>" class="form-control name_link">
						      <input type="hidden" value="<?=$row->name?>" class="form-control name_file">
						      <input type="hidden" value="<?=$row->size?>" class="form-control size_file">
						    </p>
						
					    </td>
					    <td>
						<span class="size"><?=$row->size?></span>
					    </td>
					    <td>
						
						    <button data-url="<?=site_url('/admin_handling/halaman/post_upload?file='.$row->file).'&id='.$row->id?>" data-type="DELETE" class="btn btn-danger delete">
							<i class="glyphicon glyphicon-trash"></i>
							<span>Delete</span>
						    </button>
						    <input type="checkbox" class="toggle" value="1" name="delete">
						
					    </td>
					</tr>
					<?php endforeach;?>
					<?php endif;?>
				    </tbody>
				  </table>
                                </div>
                                
				
			    </form>
			    */
			    ?>
                        </div>
                    </div>
                </div>
            </div>