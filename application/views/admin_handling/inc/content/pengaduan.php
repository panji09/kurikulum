            <div class="row">
                <div class="col-lg-12">
                    <div class="box">
                        <header class="dark">
                            <div class="icons">
                                <i class="fa fa-ok"></i>
                            </div>
                            <h5><?=$title?></h5>
                            <div class="toolbar">
                              <ul class="nav">
                                <li>
                                  <div class="btn-group">
                                    <a class="accordion-toggle btn btn-xs minimize-box" data-toggle="collapse" href="#collapse2">
                                      <i class="fa fa-chevron-up"></i>
                                    </a>
                                  </div>
                                </li>
                              </ul>
                            </div>
                        </header>
                        <div id="collapse2" class="body collapse in">
                            <?php
                                $notif=$this->session->flashdata('success');
                                if($notif):
                            ?>
                            <div class="alert <?=($notif['status'] ? 'alert-success' : 'alert-danger')?>"><?=$notif['msg']?></div>
                            <?php endif;?>
                            <table id="pengaduan" class="table responsive table-bordered table-condensed table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>*</th>
                                        <th>Kode Pengaduan</th>
                                        <th>Tanggal</th>
                                        <th>Ditujukan</th>
                                        <th>Media</th>
                                        <th>Kategori</th>
                                        <th>Substansi</th>
                                        <th>Daerah</th>
                                        <th>Deskripsi</th>
                                        <th>Publikasi</th>
                                        <th>Verifikasi</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>    
                        </div>
                    </div>
                </div>
            </div>