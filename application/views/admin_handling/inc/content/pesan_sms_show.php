<?php
    $query = $this->pengaduan_db->get($id);
    if($query):
	$pesan_sms = $query->row();
    
?>
<table class='table'>
      <tbody>
	  <tr>
	      <td><label>Operator</label></td>
	      <td><?=$pesan_sms->operator?></td>
	  </tr>
	  <tr>
	      <td><label>Telp</label></td>
	      <td><?=$pesan_sms->handphone?></td>
	  </tr>
	  
	  <tr>
	      <td><label>Deskripsi</label></td>
	      <td><?=$pesan_sms->deskripsi?></td>
	  </tr>
	  <tr>
	      <td><label>Peran</label></td>
	      <td>
		  <div class='form-group'>
		      <select name='sumber_info' id='sumber_info' class="form-control" required>
			<option value=''>-- pilih --</option>
			<?php 
			    $sumber = $this->select_db->sumber_info($role='home')->result();
			    foreach($sumber as $row):?>
			    <option value="<?=$row->id?>" <?=($pesan_sms->sumber_id == $row->id ? 'selected' : '')?>><?=$row->name?></option>
			<?php endforeach;?>
		      </select>
		  </div>
		  <div class="form-group" id='sumber_lain_form'>
		      <input name='sumber_lain' id='sumber_lain' type="text" value="<?=($pesan_sms->sumber_id == 9999 ? $pesan_sms->sumber_lain : '')?>" required class="form-control" placeholder="Sumber lain" disabled>
		  </div>
		  <script>
		      $(document).ready(function() {
			function sumber_check(){
			    if(parseInt($("#sumber_info").val()) == 9999){
				$("#sumber_lain_form input").prop('disabled', false);
			    }else{
				$("#sumber_lain_form input").prop('disabled', true);
			    }
			}
			sumber_check();
			$("#sumber_info").change(function(){
			    sumber_check();
			    
			});
		      });
		  </script>
	      </td>
	  </tr>
	  <tr>
	      <td><label>Kategori</label></td>
	      <td>
		  <div class='form-group'>
		      <select name='kategori' id='kategori' class="form-control" required>
			<option value=''>-- pilih --</option>
			<?php 
			    $kategori = $this->select_db->kategori()->result();
			    foreach($kategori as $row):?>
			    <option value="<?=$row->id?>" <?=($pesan_sms->kategori_id == $row->id ? 'selected' : '')?>><?=$row->name?></option>
			<?php endforeach;?>
		      </select>
		  </div>
	      </td>
	  </tr>
	  <tr>
	      <td><label>Substansi</label></td>
	      <td>
		  <div class='form-group'>
		      <?php
			  $isu = $this->select_db->isu()->result();
			  $query = $this->pengaduan_db->get_isu($pesan_sms->id);
			  $pengaduan_isu = array();
			  
			  if($query){
			      foreach($query->result() as $row){
				  $pengaduan_isu[] = $row->isu_id;
			      }
			  }
			  
			  foreach($isu as $row):
			  ?>
			      <div class="checkbox-inline no_indent">
				  <input <?=($row->id == 9999 ? 'id="isu_lain"' : '')?> type="checkbox"  name="isu[]" value="<?=$row->id?>" <?=(in_array($row->id, $pengaduan_isu) ? 'checked' : '')?>> <?=$row->name?>
			      </div>
			      
			      <?php if($row->id == 9999):?>
				  
				  <div class="checkbox-inline no_indent" id='isu_lain_form'>
				      <input name='isu_lain' id='isu_lain_input' type="text" value="" required class="form-control" placeholder="Substansi lain" required disabled>
				  </div>
				  
			      <?php endif;?>
			  <?php endforeach;?>
		  </div>
		  <script>
		      $(document).ready(function() {
			function isu_check(){
			    if($("#isu_lain").is(":checked")){
				$("#isu_lain_input").prop('disabled', false);
			    }else{
				$("#isu_lain_input").prop('disabled', true);
			    }
			}
			isu_check();
			$("#isu_lain").change(function(){
			    isu_check();
			    
			});
		      });
		  </script>
	      </td>
	  </tr>
	  
	  <tr>
	      <td>&nbsp</td>
	      <td><input type='checkbox' name='published' value='1' id='published'> Publikasi</td>
	  </tr>
      </tbody>
  </table>
<?php endif;?>