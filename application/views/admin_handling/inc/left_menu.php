<?php
$uri2=$this->uri->segment(2);
$uri3=$this->uri->segment(3);
$uri4=$this->uri->segment(4);
$admin = $this->session->userdata('admin');
?>
<div id="left">
        <div class="media user-media">
          <a class="user-link" href="">
            <img class="media-object img-thumbnail user-img" alt="User Picture" src="<?=$this->config->item('admin_handling_img')?>/user.png">
            
            
          </a>
          <div class="media-body">
            <h5 class="media-heading"><?=$admin['fullname']?></h5>
            <ul class="list-unstyled user-info">
              
              <li>Terakhir Login :
                <br>
                <small>
		  <?php if(isset($admin['last_login']['time'])):?>
                  <i class="fa fa-calendar"></i>&nbsp;<time class="" datetime="<?=date('c',$admin['last_login']['time'])?>"><?=time_ago($admin['last_login']['time'])?></time>
		  <?php endif;?>
		</small>
              </li>
            </ul>
          </div>
        </div>

        <!-- #menu -->
        <ul id="menu" class="collapse">
          <li class="nav-header">Menu</li>
          <li class="nav-divider"></li>
          <li <?=($uri2=='masukan' ? 'class="active"' : '')?>>
            <a href="<?=site_url('admin_handling/masukan')?>">
              <i class="fa fa-table"></i>&nbsp; Masukan</a>
          </li>
          
          <li <?=($uri2=='usulan_agenda' ? 'class="active"' : '')?>>
            <a href="<?=site_url('admin_handling/usulan_agenda')?>">
              <i class="fa fa-table"></i>&nbsp; Undangan Agenda</a>
          </li>
          
          <li <?=($uri3=='berita_kurikulum' || $uri3=='berita' ? 'class="active"' : '')?>>
            <a href="javascript:;">
              <i class="glyphicon-th-list"></i>
              <span class="link-title">Berita</span>
              <span class="fa arrow"></span>
            </a>
            <ul>
              <li <?=($uri3=='berita' ? 'class="active"' : '')?>>
		<a href="<?=site_url('admin_handling/halaman/berita')?>">
		  <i class="fa fa-table"></i>&nbsp; Berita</a>
	      </li>
              
              <li <?=($uri3=='berita_kurikulum' ? 'class="active"' : '')?>>
		<a href="<?=site_url('admin_handling/halaman/berita_kurikulum')?>">
		  <i class="fa fa-table"></i>&nbsp; Berita Perkembangan Kurikulum</a>
	      </li>
	      
	      
            </ul>
          </li>
          
          <li <?=($uri3=='tentang_kurikulum' || $uri3=='perkembangan_kurikulum' || $uri3=='usulan'  ? 'class="active"' : '')?>>
            <a href="javascript:;">
              <i class="glyphicon-th-list"></i>
              <span class="link-title">Halaman</span>
              <span class="fa arrow"></span>
            </a>
            <ul>
              <li <?=($uri3=='tentang_kurikulum' ? 'class="active"' : '')?>>
		<a href="<?=site_url('admin_handling/halaman/tentang_kurikulum')?>">
		  <i class="fa fa-table"></i>&nbsp; Tentang Kurikulum</a>
	      </li>
              
              <li <?=($uri3=='perkembangan_kurikulum' ? 'class="active"' : '')?>>
		<a href="<?=site_url('admin_handling/halaman/perkembangan_kurikulum')?>">
		  <i class="fa fa-table"></i>&nbsp; Perkembangan Kurikulum</a>
	      </li>
	      
	      <li <?=($uri3=='usulan' ? 'class="active"' : '')?>>
		<a href="<?=site_url('admin_handling/halaman/usulan')?>">
		  <i class="fa fa-table"></i>&nbsp; Masukan</a>
	      </li>
            </ul>
          </li>
          
          
          <li <?=($uri2=='user_admin' ? 'class="active"' : '')?>>
            <a href="<?=site_url('admin_handling/user_admin')?>">
              <i class="fa fa-table"></i>&nbsp; User Admin</a>
          </li>
        </ul><!-- /#menu -->
      </div><!-- /#left -->