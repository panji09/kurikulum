<?php
    $filter_laporan = $this->session->userdata('filter_laporan');
    if($this->role_level->level_id == 2){
	$filter_laporan['provinsi'] = $this->role_level->provinsi_id;
	$filter_laporan['kabkota'] = (isset($filter_laporan['kabkota']) ? $filter_laporan['kabkota'] : 'all');
    }elseif($this->role_level->level_id == 3){
	$filter_laporan['provinsi'] = $this->role_level->provinsi_id;
	$filter_laporan['kabkota'] = $this->role_level->kabkota_id;
    }
    //print_r($filter_laporan);
    function wilayah($wilayah){
	return '
	<tr>
	  <td colspan="11"><b>'.$wilayah.'</b></td>
	</tr>
	';
    }
    
    function separator(){
	return '
	<tr>
	  <td colspan="11"></td>
	</tr>
	';
    }
    
    function content_head($content=array()){
	return '
	<tr>
	  <td></td>
	  <td class="border"><b>Kode Pengaduan</b></td>
	  <td class="border">'.(isset($content['kode_pengaduan']) ? $content['kode_pengaduan'] : null).'</td>
	  <td class="border"></td>
	  <td class="border"><b>Program</b></td>
	  <td class="border">'.(isset($content['program']) ? $content['program'] : null).'</td>
	  <td class="border"></td>
	  <td class="border"><b>Media</b></td>
	  <td class="border">'.(isset($content['media']) ? $content['media'] : null).'</td>
	  <td></td>
	  <td></td>
	  
	</tr>
	<tr>
	  <td></td>
	  <td class="border"><b>Sumber Informasi</b></td>
	  <td class="border">'.(isset($content['sumber']) ? $content['sumber'] : null).'</td>
	  <td class="border"></td>
	  <td class="border"><b>Kategori</b></td>
	  <td class="border">'.(isset($content['kategori']) ? $content['kategori'] : null).'</td>
	  <td class="border"></td>
	  <td class="border"><b>Status</b></td>
	  <td class="border">'.(isset($content['status']) ? $content['status'] : null).'</td>
	  <td></td>
	  <td></td>
	  
	</tr>
	<tr>
	  <td></td>
	  <td class="border"><b>Lokasi</b></td>
	  <td class="border">'.(isset($content['lokasi']) ? $content['lokasi'] : null).'</td>
	  <td class="border"></td>
	  <td class="border"><b>Pelaku</b></td>
	  <td class="border">'.(isset($content['pelaku']) ? $content['pelaku'] : null).'</td>
	  <td class="border-bottom"></td>
	  <td class="border-bottom"></td>
	  <td class="border-bottom-right"></td>
	  <td></td>
	  <td></td>
	  
	</tr>
	';
    }
    
    function content_pengaduan($content=array()){
	return '
	<tr>
	  <td></td>
	  <td class="border">'.(isset($content['no']) ? $content['no'] : null).'</td>
	  <td class="border">'.(isset($content['nama']) ? $content['nama'] : null).'</td>
	  <td class="border">'.(isset($content['alamat']) ? $content['alamat'] : null).'</td>
	  <td class="border">'.(isset($content['email']) ? $content['email'] : null).'</td>
	  <td class="border">'.(isset($content['telp']) ? $content['telp'] : null).'</td>
	  <td class="border">'.(isset($content['tanggal']) ? $content['tanggal'] : null).'</td>
	  <td colspan="2" class="border">'.(isset($content['deskripsi']) ? $content['deskripsi'] : null).'</td>
	  <td></td>
	  <td></td>
	</tr>
	';
    }
    
    function content_penanganan($content=array()){
	return '
	<tr>
	  <td></td>
	  <td class="border">'.(isset($content['no']) ? $content['no'] : null).'</td>
	  <td class="border">'.(isset($content['oleh']) ? $content['oleh'] : null).'</td>
	  <td class="border">'.(isset($content['tanggal']) ? $content['tanggal'] : null).'</td>
	  <td class="border">'.(isset($content['status']) ? $content['status'] : null).'</td>
	  <td colspan="4" class="border">'.(isset($content['deskripsi']) ? $content['deskripsi'] : null).'</td>
	  <td></td>
	  <td></td>
	</tr>
	';
    }
?>

<!DOCTYPE html>
<html>
  <head>
    <title>Laporan - Rekap</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
   <style>
    body {
	color: #333;
	font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
	font-size: 14px;
	line-height: 1.42857;
    }
    table {
	border-collapse: collapse;
	width:100%;
    }

    table, td, th {
	
	
    }
    .border{
	border: 1px solid black;
    }
    .border-bottom{
	border-bottom: 1px solid black;
    }
    
    .border-top{
	border-top: 1px solid black;
    }
    
    .border-left{
	border-left: 1px solid black;
    }
    
    .border-right{
	border-right: 1px solid black;
    }
    
    .border-bottom-right{
	border-bottom: 1px solid black;
	border-right: 1px solid black;
    }
    </style>
    
    <!-- ./Bootstrap -->
  </head>
  <body>
  
  <table class='border'>
  <?php
      /*
      if($this->role_level->level_id==2){
	  //level provinsi
	  $query_wilayah = $this->region_db->kabkota(array('provinsi_id' => $this->role_level->provinsi_id));
      }elseif($this->role_level->level_id==3){
	  //level kabkota
	  $query_wilayah = $this->region_db->kecamatan(array('kabkota_id' => $this->role_level->kabkota_id));
      }*/
      
      if($filter_laporan['provinsi'] == 'all'){
	  $level = 'nasional';
	  $query_wilayah = $this->region_db->provinsi();
      }
      
      if($filter_laporan['provinsi'] != 'all'){
	  $level = 'provinsi';
	  if($this->role_level->level_id == 2)
	      $filter_laporan['provinsi'] = $this->role_level->provinsi_id;
	  
	  $query_wilayah = $this->region_db->kabkota(array('provinsi_id' => $filter_laporan['provinsi']));    
	  
	  //$height = $this->region_db->kabkota(array('provinsi_id' => $filter_laporan['provinsi']))->num_rows() * 60;
	  //echo $height;
      }
      
      if($filter_laporan['kabkota'] != 'all'){
	  $level = 'kabkota';
	  
	  if($this->role_level->level_id == 3)
	      $filter_laporan['kabkota'] = $this->role_level->kabkota_id;
	  
	  $query_wilayah = $this->region_db->kecamatan(array('kabkota_id' => $filter_laporan['kabkota']));
	  //$height = $this->region_db->kecamatan(array('kabkota_id' => $filter_laporan['kabkota']))->num_rows() * 60;
	  
	  //echo $height;
      }
      //echo $this->db->last_query();
      foreach($query_wilayah->result() as $wilayah): //query_wilayah
	$filter = array(
	    'program' => $this->role_program,
	    'jenjang' => $this->role_jenjang,
	    'tahun' => $this->uri->segment(5),
	    'triwulan' => $this->uri->segment(6),
	    'approved' => 1
	);
	//print_r($filter);
	
	
	
	if($filter_laporan['kabkota'] != 'all'){
	    $filter['kecamatan'] = $wilayah->id;
	}
	elseif($filter_laporan['provinsi'] != 'all'){
	    $filter['kabkota'] = $wilayah->id;
	}
	elseif($filter_laporan['provinsi'] == 'all'){
	  $filter['provinsi'] = $wilayah->id;
	}
	
	$query_pengaduan = $this->pengaduan_db->get_all($filter);
	//echo $this->db->last_query();
	
	    
      ?>
  <?php if($query_pengaduan->num_rows()):?>
      <?=wilayah($wilayah->name)?>
      <?=separator()?>
      <?php 
	  $no = 0;
	  foreach($query_pengaduan->result() as $pengaduan): //query_pengaduan?>
      <?php
	  
	  //content head
	  $query_pelaku = $this->pengaduan_db->get_pelaku($pengaduan->id);
	  //echo $this->db->last_query();
	  $pelaku = array();
	  if($query_pelaku->num_rows())
	      foreach($query_pelaku->result() as $row_pelaku){
		  if($row_pelaku->pelaku_id == 7)//lainnya
		      $pelaku[] = $row_pelaku->name.'('.$row_pelaku->pelaku_lain.')';
		  else
		      $pelaku[] = $row_pelaku->name;
	      }
	  $content = array(
	      'kode_pengaduan' => $pengaduan->kode_pengaduan,
	      'program' => $pengaduan->program,
	      'sumber' => $pengaduan->sumber,
	      'kategori' => $pengaduan->kategori,
	      'lokasi' => $pengaduan->provinsi.', '.$pengaduan->kabkota.', '.$pengaduan->kecamatan,
	      'pelaku' => implode(', ', $pelaku),
	      'media' => $pengaduan->media,
	      'status' => $pengaduan->status
	  );
	  echo content_head($content);
      ?>
      <tr>
	<td></td>
	<td colspan="8" class='border'><center><b>Pengaduan</b></center></td>
	<td></td>
	<td></td>
      </tr>
      <tr>
	<td></td>
	<td class='border'><b>#</b></td>
	<td class='border'><b>Nama</b></td>
	<td class='border'><b>Alamat</b></td>
	<td class='border'><b>Email</b></td>
	<td class='border'><b>Telp</b></td>
	<td class='border'><b>Tanggal</b></td>
	<td colspan="2" class='border'><b>Deskripsi</b></td>
	<td></td>
	<td></td>
      </tr>
      <?php
	  
	  $content = array(
	      'no' => ++$no,
	      'nama' => $pengaduan->nama,
	      'alamat' => $pengaduan->alamat_rumah,
	      'email' => $pengaduan->email,
	      'telp' => $pengaduan->handphone,
	      'tanggal' => mysqldatetime_to_date($pengaduan->tanggal,"d/m/Y H:i:s"),
	      'deskripsi' => $pengaduan->deskripsi
	  );
	  echo content_pengaduan($content);
      ?>
      
      <?php
	  $query_respon = $this->respon_db->get_all(array('pengaduan_id' => $pengaduan->id));
	  //echo $this->db->last_query();
	  if($query_respon->num_rows()):
      ?>
      <tr>
	<td></td>
	<td colspan="8" class='border'><center><b>Penanganan</b></center></td>
	<td></td>
	<td></td>
      </tr>
      <tr>
	<td></td>
	<td class='border'><b>#</b></td>
	<td class='border'><b>Oleh</b></td>
	<td class='border'><b>Tanggal</b></td>
	<td class='border'><b>Status</b></td>
	<td colspan="4" class='border'><b>Deskripsi</b></td>
	<td></td>
	<td></td>
      </tr>
      <?php
	  
	      
	      $no_respon=0;
	      foreach($query_respon->result() as $row_respon){
		  $content = array(
		    'no' => ++$no_respon,
		    'oleh' => $row_respon->user.' ('.$row_respon->level.')',
		    'tanggal' => mysqldatetime_to_date($row_respon->tanggal,"d/m/Y H:i:s"),
		    'status' => $row_respon->status,
		    'deskripsi' => $row_respon->deskripsi
		  );
		  echo content_penanganan($content);  
	      }
	      
	  endif;
	  
      ?>
      <?=separator();?>
      <?php endforeach; //end query_pengaduan?>
  <?php endif;//query_pengaduan numrows?>
  
  
  <?php endforeach; //end query wilayah?>
  </table>

  </body>
</html>