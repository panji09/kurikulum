<!DOCTYPE html>
<html>
  <head>
    <title><?=$title?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?=$this->load->view('handling/inc/script_header')?>
  </head>
  <body>
      <?=$this->load->view('handling/inc/modal')?>
      <div id="wrap">
  <!-- Static navbar -->
        <?=$this->load->view('handling/inc/header')?>
        <?=$this->load->view('handling/inc/menu')?>
      <div class="container">
        <?=$this->load->view('handling/modules/'.$module)?>
      </div>
      </div>
        <?=$this->load->view('handling/inc/script_footer')?>
        <?=$this->load->view('handling/inc/footer')?>
  </body>
</html>