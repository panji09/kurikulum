<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="//code.jquery.com/jquery.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="<?=$this->config->item('handling_plugin')?>/bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?=$this->config->item('handling_plugin')?>/moment/moment.js"></script>
  <script src="<?=$this->config->item('handling_plugin')?>/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
  
  
  <!--print-->
  <script src="<?=$this->config->item("plugin")?>/printthis/printThis.js"></script>
  <!--./print-->
  
  <!--highchart-->
  <script src="<?=$this->config->item("plugin")?>/highcharts/js/highcharts.js"></script>
  <script src="<?=$this->config->item("plugin")?>/highcharts/js/modules/exporting.src.js"></script>
  <!--./highchart-->
  
  <!--jquery expander-->
  <script src="<?=$this->config->item('admin_js')?>/jquery.expander.js"></script>
  <!--end jquery expander-->
  
  <!-- datatables-->
  <script type="text/javascript" language="javascript" src="<?=$this->config->item('admin_plugin')?>/datatables/media/js/jquery.dataTables.js"></script>
  <script src="<?=$this->config->item('handling_plugin')?>/datatables-bootstrap/js/datatables.js"></script>
  <!-- end datatables-->
  
  <!--bootstrap validator-->
  <script type="text/javascript" src="<?=$this->config->item('plugin')?>/bootstrap-validator/dist/js/bootstrapValidator.min.js"></script>
  <!--./bootstrap validator-->
  
  <!--page pengaduan-->
  <?php if($this->uri->segment(2)=='pengaduan'):?>
  <script>
	      
	      function run_expander(){
		  $('div.expandable p').expander({
		      slicePoint:       250,  // default is 100
		      expandPrefix:     ' ', // default is '... '
		      expandText:       '[...]', // default is 'read more'
		      collapseTimer:    0, // re-collapses after 5 seconds; default is 0, so no re-collapsing
		      userCollapseText: '[^]'  // default is 'read less'
		  });
	      }
	      function run_respon() {
		  $('.respon-edit').click(function(){
		      $('#collapseTwo').collapse('hide');
		      $('#collapseOne').collapse('show');
		      $('#modal_jawab form').attr('action',$(this).data('href'));
		      
		      $('#collapseOne .panel-body').empty();
		      $.get( $(this).data('load'), function( data ) {
				    $('#collapseOne .panel-body').html(data);
				    script_modal_pengaduan();
				    run_expander();
				    
		      });
		  });
	      }
	      //script modal pengaduan
	      function script_modal_pengaduan(){
		  
		  
		  //alert('yt');
		  
		  $(".kategori_penyelesaian").change(function() {
		      $("#penyelesaian").empty();
		      $("#penyelesaian").append('<option value="">-Pilih-</option>');
		      $.post('<?=site_url('services/get_penyelesaian')?>',
		      { kategori_penyelesaian_id : $(this).val() },
		      function(data) {
			  $.each(data, function(i, item){
			      $("#penyelesaian").append(
				      '<option value="' + item.id + '">'+item.name + '</option>'
			      );
			  })
		      },
		      'json'
		      );
		  });
		  $("#provinsi1").change(function() {
		      $("#kabkota1").empty();
		      $("#kabkota1").append('<option value="all">-Pilih-</option>');
		      $("#kecamatan1").empty();
		      $("#kecamatan1").append('<option value="">-Pilih-</option>');
		      $.post('<?=site_url('services/get_kabkota')?>',
		      { provinsi_id : $("#provinsi1 option:selected").val() },
		      function(data) {
			  $.each(data, function(i, item){
			      $("#kabkota1").append(
				  '<option value="' + item.id + '">'+item.name + '</option>'
			      );
			  })
		      },
		      'json'
		      );
		  });
		  $("#kabkota1").change(function() {
		      $("#kecamatan1").empty();
		      $("#kecamatan1").append('<option value="">-Pilih-</option>');
		      $.post('<?=site_url('services/get_kecamatan')?>',
		      { kabkota_id : $("#kabkota1 option:selected").val() },
		      function(data) {
			  $.each(data, function(i, item){
			      $("#kecamatan1").append(
				  '<option value="' + item.id + '">'+item.name + '</option>'
			      );
			  })
		      },
		      'json'
		      );
		  });
		  
		  $('#sumber_info').change(function(){
		      //alert($('#sumber_info').val());
		      if($('#sumber_info').val() == 9999){
			  $('#sumber_lain_form').show();
			  //$('#sumber_lain_form input').show();
			  
		      }else{
			  $('#sumber_lain_form').hide();
		      }
		  });
		  
		  $('#lokasi').change(function(){
		      if($('#lokasi').val() == 1){
			  $('#jenjang_sekolah_form').show();
			  $('#title_nama_lokasi').html('Nama Madrasah');
			  $('#nama_lokasi').attr('placeholder','Nama Madrasah');
		      }else if($('#lokasi').val() == 9999){
			  $('#jenjang_sekolah_form').hide();
			  $('#title_nama_lokasi').html('Nama Lokasi');
			  $('#nama_lokasi').attr('placeholder','Nama Lokasi');
		      }
		  });
		  
		  if($('#lokasi').val() == 1){
		      $('#jenjang_sekolah_form').show();
		      $('#title_nama_lokasi').html('Nama Madrasah');
		      $('#nama_lokasi').attr('placeholder','Nama Madrasah');
		  }else if($('#lokasi').val() == 9999){
		      $('#jenjang_sekolah_form').hide();
		      $('#title_nama_lokasi').html('Nama Lokasi');
		      $('#nama_lokasi').attr('placeholder','Nama Lokasi');
		  }
		      
		  $('.input-group.date').datetimepicker({
		      language: "id",
		      showToday: true,
		      useCurrent: true,               //when true, picker will set the value to the current date/time
		      pick12HourFormat: false,
		      format: 'DD/MM/YYYY HH:mm',
		      sideBySide: true
		  });
		  
					      
		    function showhide(){
			if ($('#kategori').val()=='4') {//penyimpangan dana
			  $('#rp_dana').show();
			}else{
			  $('#rp_dana').hide();
			}
			
			if ($('#sumber_info').val()=='9999') {//sumber lain
			  $('#sumber_lain_form').show();
			}else{
			  $('#sumber_lain_form').hide();
			}
			
			var pelaku_selected = new Array();
			var n = $(".pelaku:checked").length;
			if (n > 0){
			  $(".pelaku:checked").each(function(){
			      pelaku_selected.push($(this).val());
			  });
			}
			//console.log(pelaku_selected);
			if(jQuery.inArray( '7', pelaku_selected) >= 0){
				  $('#pelaku_lain').show();
			}else{
				  $('#pelaku_lain').hide();
			}
			
			
			
			if ($('.kategori_penyelesaian:checked').val()) {//non hukum
			  $('#penyelesaian').show();
			}else{
			  $('#penyelesaian').hide();
			}
		    }
		    showhide();
		    $('#kategori').change(function(){
			showhide();
		    });
		    $('#sumber_info').change(function(){
			showhide();
		    });
		    $('.pelaku').change(function(){
			showhide();
		    });
		    $('.kategori_penyelesaian').change(function(){
			showhide(); 
		    });		      
	      }
	      //end script modal pengaduan
	      $(document).ready(function() {
		  //$('#modal_pengaduan form').bootstrapValidator();
		 
		  
		  
		  $("#provinsi").change(function() {
		      $("#kabkota").empty();
		      $("#kabkota").append('<option value="all">-Semua-</option>');
		      $("#kecamatan").empty();
		      $("#kecamatan").append('<option value="">-Pilih-</option>');
		      $("#desa").empty();
		      $("#desa").append('<option value="">-Pilih-</option>');
		      $.post('<?=site_url('services/get_kabkota')?>',
		      { KdProv : $("#provinsi option:selected").val() },
		      function(data) {
			  $.each(data, function(i, item){
			      $("#kabkota").append(
				  '<option value="' + item.KdKab + '">'+item.NmKab + '</option>'
			      );
			  })
		      },
		      'json'
		      );
		  });
	      
	      });
  
	  //script table handling
	  var asInitVals = new Array();
	  var i=0;
	  var update=true;
	  $(document).ready(function() {
	      $.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
	      {
		return {
		      "iStart":         oSettings._iDisplayStart,
		      "iEnd":           oSettings.fnDisplayEnd(),
		      "iLength":        oSettings._iDisplayLength,
		      "iTotal":         oSettings.fnRecordsTotal(),
		      "iFilteredTotal": oSettings.fnRecordsDisplay(),
		      "iPage":          Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
		      "iTotalPages":    Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
		};
	      }
		  
		  var oTable = $('#pengaduan').dataTable( {
			  "bStateSave": false,
			  "bProcessing": true,
			  "bServerSide": true,
			  "iDisplayLength": 25,
			  "sPaginationType": "bs_full",
			  "sAjaxSource": "<?=$link_ajax_pengaduan?>",
			  "sServerMethod": "POST",
			  "aoColumnDefs": [ 
			    { "bSortable": false, "aTargets": [ 0 ] }, //disable soft kolom 2/no
			    { "bSortable": false, "aTargets": [ 9 ] }, //disable soft kolom 10/action
			    {
				  "fnRender": function ( oObj ) {
					  if(update){
						  i=oTable.fnPagingInfo().iStart;
						  update=false;
					  }
					  if(i+1 == oTable.fnPagingInfo().iEnd){
						  update=true;
					  }
					  //alert(i+' '+oTable.fnPagingInfo().iEnd);
					  return ++i;
					  
				  },
				  "bUseRendered": false,
				  "aTargets": [ 0 ] //no
			    }
			  ],
			  "fnRowCallback": function( nRow, aData, iDisplayIndex) {
			      //console.log(aData[10]);
			      if(parseInt(aData[10]) == parseInt('<?=$this->role_level->level_id?>')){
				  $(nRow).addClass('danger');
			      }
			      
			      //return nRow;
			  },
			  "fnDrawCallback": function () {
				  run_expander();
				  
				  $('.respon-edit').click(function(){
						alert('uy');
				  });
				  
				   $(".respon-delete").click(function(){
							      //alert();
				      $('#modal_delete .modal-title').html($(this).data('title'));
				      $('#modal_delete .modal-body').html($(this).data('body'));
				      $('#modal_delete_href').attr('href', $(this).data('href'));
				  }); 
				  
				  $('.btn_pengaduan').click(function(){
					    //alert('uy');
					    $('#modal_pengaduan_label').html($(this).data('label'));
					    $('#modal_pengaduan form').attr('action',$(this).data('action'));
					    $('#modal_pengaduan .modal-body').html('');
					    $('#modal_pengaduan .modal-footer').html('');
					    
					    $.get( $(this).data('href'), function( data ) {
							  $('#modal_pengaduan .modal-body').html(data);
							  $('#modal_pengaduan .modal-footer').html('<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button><button type="submit" class="btn btn-primary">Simpan</button>');
							  script_modal_pengaduan();
							  //alert($('#modal_pengaduan form').html());
							  
					    });
				  });
				  
				  $('.jawab').click(function(){
					    $('#modal_jawab form').attr('action',$(this).data('action'));
					    $('#modal_jawab .modal-body').empty();
					    $.get( $(this).data('href'), function( data ) {
							  $('#modal_jawab .modal-body').html(data);
							  script_modal_pengaduan();
							  run_expander();
							  run_respon();
							  $(".respon-delete").click(function(){
							      //alert();
							      $('#modal_delete .modal-title').html($(this).data('title'));
							      $('#modal_delete .modal-body').html($(this).data('body'));
							      $('#modal_delete_href').attr('href', $(this).data('href'));
							  }); 
					    });
				  });
			  }
		  } );
		  
		  oTable.fnSort( [ [1,'desc'] ] ); //order by tanggal desc
		  oTable.fnSort( [ [8,'asc'] ] ); //order by status asc
		  
		  $("#pengaduan thead input").change( function () {
			  /* Filter on the column (the index) of this element */
			  var id = $(this).attr('id').split("-")[1];
			  oTable.fnFilter( this.value, id );
		  } );
		  $("#pengaduan thead input").keyup( function () {
			  /* Filter on the column (the index) of this element */
			  var id = $(this).attr('id').split("-")[1];
			  oTable.fnFilter( this.value, id );
		  } );
		    
		  
		$('#pengaduan').each(function(){
		    var datatable = $(this);
		    // SEARCH - Add the placeholder for Search and Turn this into in-line form control
		    var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
		    search_input.attr('placeholder', 'Search');
		    search_input.addClass('form-control input-sm');
		    // LENGTH - Inline-Form control
		    var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
		    length_sel.addClass('form-control input-sm');
		});
	  });
  </script>
  <!--script end table handling-->
  <?php endif;?>
  <!--end page pengaduan-->
  
  <!--page laporan-->
  <?php if($this->uri->segment(2)=='laporan'):?>
	      <script>
	      $(document).ready(function() {
		  $("#provinsi").change(function() {
		      $("#kabkota").empty();
		      $("#kabkota").append('<option value="all">-semua-</option>');
		      $.post('<?=site_url('services/get_kabkota')?>',
		      { provinsi_id : $("#provinsi option:selected").val() },
		      function(data) {
			  $.each(data, function(i, item){
			      $("#kabkota").append(
				  '<option value="' + item.id + '">'+item.name + '</option>'
			      );
			  })
		      },
		      'json'
		      );
		  });

	      });
	      </script>
	      <!--report kategori-->
	      <?php if($this->uri->segment(4)=='report_kategori'):
	      
		  $data = $kat_pengaduan = array();
		  foreach($report as $row){
		      $data[]="['".$row->kategori."'".','.$row->total.']';
		      $kat_pengaduan[]="'".$row->kategori."'";
		  }
	      ?>
	      <script>
	      $(document).ready(function(){
		  var data = [<?=implode(",",$data)?>];
		    
		  
		  // Build the chart
		  $('#show_chart').highcharts({
		      chart: {
			  plotBackgroundColor: null,
			  plotBorderWidth: null,
			  plotShadow: false
		      },
		      credits: {
			  enabled: false
		      },
		      title: {
			  text: 'JUMLAH PENGADUAN BERDASARKAN KATEGORI<br>TAHUN <?=($this->uri->segment(5)=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $this->uri->segment(5))?>'
		      },
		      tooltip: {
			      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		      },
		      plotOptions: {
			  pie: {
			      allowPointSelect: true,
			      cursor: 'pointer',
			      dataLabels: {
				  enabled: false
			      },
			      showInLegend: true
			  }
		      },
		      series: [{
			  type: 'pie',
			  name: 'Kategori Pengaduan',
			  data: data
		      }]
		  });
	      });
	      </script>
	      <?php endif;?>
	      <!--./report kategori-->
	      
	      <!--report sumber informasi-->
	      <?php if($this->uri->segment(4)=='report_sumber_info'):
	      
		  $data = $kat_pengaduan = array();
		  foreach($report as $row){
		      $data[]="['".$row->sumber."'".','.$row->total.']';
		      $kat_pengaduan[]="'".$row->sumber."'";
		  }
	      ?>
	      <script>
	      $(document).ready(function(){
		  var data = [<?=implode(",",$data)?>];
		    
		  // Build the chart
		  $('#show_chart').highcharts({
		      chart: {
			  plotBackgroundColor: null,
			  plotBorderWidth: null,
			  plotShadow: false
		      },
		      credits: {
			  enabled: false
		      },
		      title: {
			  text: 'JUMLAH PENGADUAN BERDASARKAN SUMBER INFORMASI<br>TAHUN <?=($this->uri->segment(5)=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $this->uri->segment(5))?>'
		      },
		      tooltip: {
			      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		      },
		      plotOptions: {
			  pie: {
			      allowPointSelect: true,
			      cursor: 'pointer',
			      dataLabels: {
				  enabled: false
			      },
			      showInLegend: true
			  }
		      },
		      series: [{
			  type: 'pie',
			  name: 'Sumber Informasi',
			  data: data
		      }]
		  });
	      });
	      </script>
	      <?php endif;?>
	      <!--./report sumber informasi-->
	      
	      <!--report status-->
	      <?php if($this->uri->segment(4)=='report_status'):
	      
		  $data = $kat_pengaduan = array();
		  foreach($report as $row){
		      $data[]="['".$row->status."'".','.$row->total.']';
		      $kat_pengaduan[]="'".$row->status."'";
		  }
	      ?>
	      <script>
	      $(document).ready(function(){
		  var data = [<?=implode(",",$data)?>];
		    
		  // Build the chart
		  $('#show_chart').highcharts({
		      chart: {
			  plotBackgroundColor: null,
			  plotBorderWidth: null,
			  plotShadow: false
		      },
		      credits: {
			  enabled: false
		      },
		      title: {
			  text: 'JUMLAH PENGADUAN BERDASARKAN STATUS PENGADUAN<br>TAHUN <?=($this->uri->segment(5)=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $this->uri->segment(5))?>'
		      },
		      tooltip: {
			      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		      },
		      plotOptions: {
			  pie: {
			      allowPointSelect: true,
			      cursor: 'pointer',
			      dataLabels: {
				  enabled: false
			      },
			      showInLegend: true
			  }
		      },
		      series: [{
			  type: 'pie',
			  name: 'Status Pengaduan',
			  data: data
		      }]
		  });
	      });
	      </script>
	      <?php endif;?>
	      <!--./report status-->
	      
	      <!--report program-->
	      <?php if($this->uri->segment(4)=='report_program'):
	      
		  $data = $kat_pengaduan = array();
		  foreach($report as $row){
		      $data[]="['".$row->program."'".','.$row->total.']';
		      $kat_pengaduan[]="'".$row->program."'";
		  }
	      ?>
	      <script>
	      $(document).ready(function(){
		  var data = [<?=implode(",",$data)?>];
		    
		  // Build the chart
		  $('#show_chart').highcharts({
		      chart: {
			  plotBackgroundColor: null,
			  plotBorderWidth: null,
			  plotShadow: false
		      },
		      credits: {
			  enabled: false
		      },
		      title: {
			  text: 'JUMLAH PENGADUAN BERDASARKAN PROGRAM PENGADUAN<br>TAHUN <?=($this->uri->segment(5)=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $this->uri->segment(5))?>'
		      },
		      tooltip: {
			      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		      },
		      plotOptions: {
			  pie: {
			      allowPointSelect: true,
			      cursor: 'pointer',
			      dataLabels: {
				  enabled: false
			      },
			      showInLegend: true
			  }
		      },
		      series: [{
			  type: 'pie',
			  name: 'Program Pengaduan',
			  data: data
		      }]
		  });
	      });
	      </script>
	      <?php endif;?>
	      <!--./report program-->
	      
	      <!--report kategori status-->
	      <?php if($this->uri->segment(4)=='report_kategori_status'):
	      
		  $selesai = $proses = $pending = $kat_pengaduan = array();
		  foreach($report as $row){
		      $pending[]=$row->pending;
		      $proses[]=$row->proses;
		      $selesai[]=$row->selesai;
		      
		      $kat_pengaduan[]="'".$row->kategori."'";
		  }
	      ?>
	      <script>
	      $(document).ready(function(){
		  var pending = [<?=implode(",",$pending)?>];
		  var proses = [<?=implode(",",$proses)?>];
		  var selesai = [<?=implode(",",$selesai)?>];
		  var ticks = [<?=implode(",",$kat_pengaduan)?>];
		  
		  $('#show_chart').highcharts({
		      chart: {
			  type: 'column'
		      },
		      credits: {
			  enabled: false
		      },
		      title: {
			  text: 'JUMLAH PENGADUAN BERDASARKAN KATEGORI DAN STATUS PENGADUAN<br>TAHUN <?=($this->uri->segment(5)=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $this->uri->segment(5))?>'
		      },
		      subtitle: {
			  text: ''
		      },
		      xAxis: {
			  categories: ticks
		      },
		      yAxis: {
			  min: 0,
			  title: {
			      text: 'Kategori dan Status'
			  }
		      },
		      tooltip: {
			  headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			  pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
			      '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
			  footerFormat: '</table>',
			  shared: true,
			  useHTML: true
		      },
		      plotOptions: {
			  column: {
			      pointPadding: 0.2,
			      borderWidth: 0
			  }
		      },
		      series: [{
			  name: 'Pending',
			  data: pending
	      
		      }, {
			  name: 'Proses',
			  data: proses
	      
		      }, {
			  name: 'selesai',
			  data: selesai
	      
		      }]
		  });
	      });
	      </script>
	      <?php endif;?>
	      <!--./report kategori status-->
	      
	      <!--report media-->
	      <?php if($this->uri->segment(4)=='report_media'):
	      
		  $data = $kat_pengaduan = array();
		  foreach($report as $row){
		      $data[]="['".$row->media."'".','.$row->total.']';
		      $kat_pengaduan[]="'".$row->media."'";
		  }
	      ?>
	      <script>
	      $(document).ready(function(){
		  var data = [<?=implode(",",$data)?>];
		    
		  // Build the chart
		  $('#show_chart').highcharts({
		      chart: {
			  plotBackgroundColor: null,
			  plotBorderWidth: null,
			  plotShadow: false
		      },
		      credits: {
			  enabled: false
		      },
		      title: {
			  text: 'JUMLAH PENGADUAN BERDASARKAN MEDIA PENGADUAN<br>TAHUN <?=($this->uri->segment(5)=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $this->uri->segment(5))?>'
		      },
		      tooltip: {
			      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		      },
		      plotOptions: {
			  pie: {
			      allowPointSelect: true,
			      cursor: 'pointer',
			      dataLabels: {
				  enabled: false
			      },
			      showInLegend: true
			  }
		      },
		      series: [{
			  type: 'pie',
			  name: 'Media Pengaduan',
			  data: data
		      }]
		  });
	      });
	      </script>
	      <?php endif;?>
	      <!--./report media-->
	      
	      <!--report program status-->
	      <?php if($this->uri->segment(4)=='report_program_status'):
	      
		  $selesai = $proses = $pending = $kat_pengaduan = array();
		  foreach($report as $row){
		      $pending[]=$row->pending;
		      $proses[]=$row->proses;
		      $selesai[]=$row->selesai;
		      
		      $kat_pengaduan[]="'".$row->program."'";
		  }
	      ?>
	      <script>
	      $(document).ready(function(){
		  var pending = [<?=implode(",",$pending)?>];
		  var proses = [<?=implode(",",$proses)?>];
		  var selesai = [<?=implode(",",$selesai)?>];
		  var ticks = [<?=implode(",",$kat_pengaduan)?>];
		  
		  $('#show_chart').highcharts({
		      chart: {
			  type: 'column'
		      },
		      credits: {
			  enabled: false
		      },
		      title: {
			  text: 'JUMLAH PENGADUAN BERDASARKAN PROGRAM DAN STATUS PENGADUAN<br>TAHUN <?=($this->uri->segment(5)=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $this->uri->segment(5))?>'
		      },
		      subtitle: {
			  text: ''
		      },
		      xAxis: {
			  categories: ticks
		      },
		      yAxis: {
			  min: 0,
			  title: {
			      text: 'Program dan Status'
			  }
		      },
		      tooltip: {
			  headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			  pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
			      '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
			  footerFormat: '</table>',
			  shared: true,
			  useHTML: true
		      },
		      plotOptions: {
			  column: {
			      pointPadding: 0.2,
			      borderWidth: 0
			  }
		      },
		      series: [{
			  name: 'Pending',
			  data: pending
	      
		      }, {
			  name: 'Proses',
			  data: proses
	      
		      }, {
			  name: 'selesai',
			  data: selesai
	      
		      }]
		  });
	      });
	      </script>
	      <?php endif;?>
	      <!--./report program status-->
	      
	      <!--report pelaku status-->
	      <?php if($this->uri->segment(4)=='report_pelaku_status'):
	      
		  $selesai = $proses = $pending = $kat_pengaduan = array();
		  foreach($report as $row){
		      $pending[]=$row->pending;
		      $proses[]=$row->proses;
		      $selesai[]=$row->selesai;
		      
		      $kat_pengaduan[]="'".$row->pelaku."'";
		  }
	      ?>
	      <script>
	      $(document).ready(function(){
		  var pending = [<?=implode(",",$pending)?>];
		  var proses = [<?=implode(",",$proses)?>];
		  var selesai = [<?=implode(",",$selesai)?>];
		  var ticks = [<?=implode(",",$kat_pengaduan)?>];
		  
		  
		  $('#show_chart').highcharts({
		      chart: {
			  type: 'column'
		      },
		      credits: {
			  enabled: false
		      },
		      title: {
			  text: 'JUMLAH PENGADUAN BERDASARKAN PELAKU DAN STATUS PENGADUAN<br>TAHUN <?=($this->uri->segment(5)=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $this->uri->segment(5))?>'
		      },
		      subtitle: {
			  text: ''
		      },
		      xAxis: {
			  categories: ticks
		      },
		      yAxis: {
			  min: 0,
			  title: {
			      text: 'Pelaku dan Status'
			  }
		      },
		      tooltip: {
			  headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			  pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
			      '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
			  footerFormat: '</table>',
			  shared: true,
			  useHTML: true
		      },
		      plotOptions: {
			  column: {
			      pointPadding: 0.2,
			      borderWidth: 0
			  }
		      },
		      series: [{
			  name: 'Pending',
			  data: pending
	      
		      }, {
			  name: 'Proses',
			  data: proses
	      
		      }, {
			  name: 'selesai',
			  data: selesai
	      
		      }]
		  });
	      });
	      </script>
	      <?php endif;?>
	      <!--./report pelaku status-->
	      
	      <!--report wilayah-->
	      <?php if($this->uri->segment(4)=='report_wilayah_status'):
	      
		  $selesai = $proses = $pending = $kat_pengaduan = array();
		  foreach($report as $row){
		      $pending[]=$row->pending;
		      $proses[]=$row->proses;
		      $selesai[]=$row->selesai;
		      
		      $kat_pengaduan[]="'".$row->wilayah."'";
		  }
	      ?>
	      <script>
	      $(document).ready(function(){
		  var pending = [<?=implode(",",$pending)?>];
		  var proses = [<?=implode(",",$proses)?>];
		  var selesai = [<?=implode(",",$selesai)?>];
		  var ticks = [<?=implode(",",$kat_pengaduan)?>];
		  
		  <?php /*
		  plot2 = $.jqplot('show_chart', [pending, proses, selesai], {
		      animate: !$.jqplot.use_excanvas,
		      seriesDefaults: {
			      renderer:$.jqplot.BarRenderer,
		      pointLabels: { show: true, location: 'e', edgeTolerance: -15 },
		      shadowAngle: 135,
			      rendererOptions: {
			  barDirection: 'horizontal'
		      }
		      },
		      axes: {
			      
			      yaxis: {
				      renderer: $.jqplot.CategoryAxisRenderer,
				      ticks: ticks,
				      
			      },
			      xaxis: {
				      min:0,
				      tickInterval: 10,
				      tickOptions: { formatString:'%d' }
			      },
		      },
		      legend: {
			      show: true,
			      location: 'ne',
			      placement: 'inside'
		      },
		      series:[
			  {label:'Pending'},
			  {label:'Proses'},
			  {label:'Selesai'}
		      ],
		      title:{
			  text: 'JUMLAH PENGADUAN BERDASARKAN PELAKU DAN STATUS WILAYAH<br>TAHUN <?=($this->uri->segment(5)=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $this->uri->segment(5))?>'
		    }
		  });
		  */
		  ?>
		  $('#show_chart').highcharts({
		      chart: {
			  type: 'bar'
		      },
		      title: {
			  text: 'JUMLAH PENGADUAN BERDASARKAN PELAKU DAN STATUS WILAYAH<br>TAHUN <?=($this->uri->segment(5)=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $this->uri->segment(5))?>'
		      },
		      subtitle: {
			  text: ''
		      },
		      xAxis: {
			  categories: ticks,
			  title: {
			      text: null
			  }
		      },
		      yAxis: {
			  min: 0,
			  title: {
			      text: 'Pengaduan per Wilayah',
			      align: 'high'
			  },
			  labels: {
			      overflow: 'justify'
			  }
		      },
		      tooltip: {
			  headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			  pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
			      '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
			  footerFormat: '</table>',
			  shared: true,
			  useHTML: true
		      },
		      plotOptions: {
			  bar: {
			      dataLabels: {
				  enabled: true
			      }
			  }
		      },
		      
		      credits: {
			  enabled: false
		      },
		      series: [{
			  name: 'Pending',
			  data: pending
		      }, {
			  name: 'Proses',
			  data: proses
		      }, {
			  name: 'Selesai',
			  data: selesai
		      }]
		  });
	      });
	      </script>
	      <?php endif;?>
	      <!--./report wilayah-->
	      <!--end report-->
  <?php endif;?>
  <!--end page laporan-->
  
  
  <!--page akses_log-->
  <?php if($this->uri->segment(2)=='akses_log'):?>
  <script>
  $(document).ready(function() {
      $("#provinsi").change(function() {
	  $("#kabkota").empty();
	  $("#kabkota").append('<option value="">-Semua-</option>');
	  $("#kecamatan").empty();
	  $("#kecamatan").append('<option value="">-Pilih-</option>');
	  $("#desa").empty();
	  $("#desa").append('<option value="">-Pilih-</option>');
	  $.post('<?=site_url('services/get_kabkota')?>',
	  { provinsi_id : $("#provinsi option:selected").val() },
	  function(data) {
	      $.each(data, function(i, item){
		  $("#kabkota").append(
		      '<option value="' + item.id + '">'+item.name + '</option>'
		  );
	      })
	  },
	  'json'
	  );
      });

  });
  </script>
  <?php endif;?>
  <!--end page akses log-->
  <script>
  $(document).ready(function(){
      $('#export #print').click(function(){
	  var canvas = document.getElementById('print_page');
	  //console.log(canvas);
	  var img    = canvas.toDataURL("image/png");
	  document.write('<img src="'+img+'"/>');
      });
      
  });
  </script>
	      
	      