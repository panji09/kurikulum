<?php $filter=$this->session->flashdata('filter_akses_log');?>

<div id="wrap">
<div class="container">
    <div class="row">
	<div>&nbsp</div>
	<?php if($this->role_level->level_id == 1):?>
        <div class="col-md-3">
	    <!--panel1-->
	    <div class="panel panel-default">
		<div class="panel-heading">
		  <h3 class="panel-title">Akses Log</h3>
		</div>
		<div class="panel-body">
		    <!--form-->
		    <form role="form" action='<?=site_url('handling/akses_log/post_akses_log')?>' method='post'>
		    
		    <div class="form-group">
		      <label for="kategori">Provinsi</label>
		      <select class='form-control' name='provinsi' id='provinsi'>
                        <option value=''>-Semua-</option>
			<?php
                            $provinsi=$this->region_db->provinsi()->result();
                            foreach($provinsi as $row):
                        ?>
                        <option value='<?=$row->id?>' <?=(isset($filter['provinsi']) && $filter['provinsi'] == $row->id ? 'selected' : '')?>><?=$row->name?></option>
                        <?php endforeach;?>
		      </select>
		    </div>
		    
		    
		    
		    <button type="submit" class="btn btn-default">Submit</button>
		</form>
		<!-- end form-->
		</div>
	    </div>
            <!-- end panel1-->
	    
	    
        </div>
        <?php endif;?>
        <div class="col-md-9">
            <!--content-->
            <?php
		/*
		    user yg bisa akses pusat sm provinsi
		*/
		$filter_akses_log = array();
		$level = '';
		$filter['provinsi'] = ($this->role_level->level_id == 2 ? $this->role_level->provinsi_id : $filter['provinsi']);
		if($filter['provinsi']==''){
		    $level = 'Provinsi';
		    //tampilkan smua user provinsi
		    $filter_akses_log['level_id'] = 2;
		}else{
		    $level = 'Kab / Kota';
		    //tampilkan smua user kabkota
		    $filter_akses_log['provinsi_id'] = ($this->role_level->level_id == 2 ? $this->role_level->provinsi_id : $filter['provinsi']);
		    $filter_akses_log['level_id'] = 3;
		}
		
		$akses_log = $this->user_handling_db->get_all($filter_akses_log);
            ?>
            <table class='table' id='akses_log'>
                <thead>
                    <tr>
                        <th>#</th>
                        <th><?=$level?></th>
                        <th>Username / Nama / Level</th>
                        <th>Terakhir Login</th>
                        <th>Terakhir Logout</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
			
			foreach($akses_log->result() as $row):
			    $role_level = $this->user_handling_db->get_role_level($row->id)->row();
			    //echo $this->db->last_query();
			    //print_r($role_level);
			    $level = $this->select_db->level(array('id' => $role_level->level_id))->row();
			    $provinsi = '';
			    $kabkota = '';
			    $wilayah = '';
			    if($role_level->level_id == 2){ //level prov
				$provinsi = $this->region_db->provinsi(array('id' => $role_level->provinsi_id))->row();
				$wilayah = '('.$provinsi->name.')';
			    }elseif($role_level->level_id == 3){//level kabkota
				$provinsi = $this->region_db->provinsi(array('id' => $role_level->provinsi_id))->row();
				
				$kabkota = $this->region_db->kabkota(array('id' => $role_level->kabkota_id))->row();
				
				$wilayah = '('.$provinsi->name.', '.$kabkota->name.')';
			    }
			?>
                    <tr>
                        <td></td>
                        <td><?=($filter['provinsi'] == '' ? $this->region_db->provinsi(array('id' => $row->provinsi_id))->row()->name : $this->region_db->kabkota(array('id' => $row->kabkota_id))->row()->name)?></td>
                        <td><?=$row->username?> / <?=$row->nama?> / <?=$level->name.' '.($wilayah != '' ? $wilayah : '')?></td>
                        <td><?=date('d/m/Y, H:i:s',$row->last_login)?></td>
                        <td><?=date('d/m/Y, H:i:s',$row->last_logout)?></td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
            
	    <!--end content-->
        </div>
    </div>
</div>
</div>