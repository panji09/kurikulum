<?php 
    $filter_laporan = $this->session->userdata('filter_laporan');
    if($this->role_level->level_id == 2){
	$filter_laporan['provinsi'] = $this->role_level->provinsi_id;
	$filter_laporan['kabkota'] = (isset($filter_laporan['kabkota']) ? $filter_laporan['kabkota'] : 'all');
    }elseif($this->role_level->level_id == 3){
	$filter_laporan['provinsi'] = $this->role_level->provinsi_id;
	$filter_laporan['kabkota'] = $this->role_level->kabkota_id;
    }
?>
<div id="wrap">
<div class="container">
    <div class="row">
	<div>&nbsp</div>
        <div class="col-md-3">
	    <!--panel1-->
	    <div class="panel panel-default">
		<div class="panel-heading">
		  <h3 class="panel-title">Laporan Berdasarkan Kategori</h3>
		</div>
		<div class="panel-body">
		    <!--form-->
		    <form role="form" action='<?=site_url('handling/laporan/post_laporan')?>' method='post'>
		    <div class="form-group">
		      <label for="kategori">Kategori</label>
		      <select class='form-control' name='kategori'>
			<?php
			    $kategori = $this->select_db->report_kategori()->result();
			?>
			<?php foreach($kategori as $row):?>
			    <option value='<?=$row->id?>' <?=($this->uri->segment(4)==$row->id ? 'selected' : '')?>><?=$row->name?></option>
			<?php endforeach;?>
		      </select>
		    </div>
		    
		    <?php if($this->role_level->level_id == 1):?>
		    <div class="form-group">
		      <label for="kategori">Provinsi</label>
		      <select class='form-control' name='provinsi' id='provinsi'>
			<option value='all'>-semua-</option>
			<?php
			    $provinsi = $this->region_db->provinsi()->result();
			?>
			<?php foreach($provinsi as $row):?>
			    <option value='<?=$row->id?>' <?=($filter_laporan['provinsi']==$row->id ? 'selected' : '')?>><?=$row->name?></option>
			<?php endforeach;?>
		      </select>
		    </div>
		    <?php endif;?>
		    
		    <?php if($this->role_level->level_id == 1 || $this->role_level->level_id == 2):?>
		    <div class="form-group">
		      <label for="kategori">Kab / Kota</label>
		      <select class='form-control' name='kabkota' id='kabkota'>
			<option value='all'>-semua-</option>
			<?php
			    
			    $kabkota = $this->region_db->kabkota(array('provinsi_id' => $filter_laporan['provinsi']))->result();
			?>
			<?php foreach($kabkota as $row):?>
			    <option value='<?=$row->id?>' <?=($filter_laporan['kabkota']==$row->id ? 'selected' : '')?>><?=$row->name?></option>
			<?php endforeach;?>
		      </select>
		    </div>
		    <?php endif;?>
		    
		    <div class="form-group">
		      <label for="tahun">Tahun</label>
		      <select class='form-control' name='tahun'>
			<option value="all">All</option>
			<?php for($i=2014;$i<=date('Y');$i++):?>
			    <option value="<?=$i?>" <?=($this->uri->segment(5)==$i ? 'selected' : '')?>><?=$i?></option>
			<?php endfor;?>
		      </select>
		    </div>
		    <div class="form-group">
		      <label for="triwulan">Triwulan</label>
		      <select class='form-control' name='triwulan'>
			<option value="all">All</option>
			<?php
			$triwulan=array(
			    1 => 'I',
			    2 => 'II',
			    3 => 'III',
			    4 => 'IV'
			);
			foreach($triwulan as $row=>$value):
			?>
			    <option value="<?=$row?>" <?=($this->uri->segment(6)==$row ? 'selected' : '')?>><?=$value?></option>
			<?php endforeach;?>
		      </select>
		    </div>
		    <button type="submit" class="btn btn-default">Submit</button>
		</form>
		<!-- end form-->
		</div>
	    </div>
            <!-- end panel1-->
	    
	    <!--panel2-->
	    <div class="panel panel-default">
	    <div class="panel-heading">
	      <h3 class="panel-title">Unduh Data</h3>
	    </div>
	    <div class="panel-body">
		
		<ul class="list-group">
		    <li class="list-group-item">
			<!-- rekap pengaduan button -->
			<div class="btn-group">
			  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
			    Rekap Pengaduan <span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu" role="menu">
			    <li><a href="<?=site_url('handling/laporan/rekap/pdf/'.$this->uri->segment(5).'/'.$this->uri->segment(6))?>">PDF</a></li>
			    <?php if($this->role_level->level_id == 1):?>
			    <li><a href="<?=site_url('handling/laporan/rekap/excel/'.$this->uri->segment(5).'/'.$this->uri->segment(6))?>">Excel</a></li>
			    <?php endif;?>
			  </ul>
			</div>
			<!-- end rekap dana button -->
		    </li>
		</ul>
		
		
		
	    </div>
	  </div>
	    <!--end panel2-->
        </div>
        <div class="col-md-9" id="print_page">
            <!--content-->
	    <?php
		$filter = array(
		    'tahun' => $this->uri->segment(5),
		    'triwulan' => $this->uri->segment(6),
		    'provinsi' => $filter_laporan['provinsi'],
		    'kabkota' => $filter_laporan['kabkota']
		);
		$role_level = $this->session->userdata('role_level');
		
		if($role_level->level_id == 2)
		    $filter['provinsi'] = $role_level->provinsi_id;
		elseif($role_level->level_id == 3)
		    $filter['kabkota'] = $role_level->kabkota_id;
		
		if($this->uri->segment(4) == 'report_kategori')
		    $report = $this->report_db->kategori($filter)->result();
		elseif($this->uri->segment(4) == 'report_sumber_info')
		    $report = $this->report_db->sumber_info($filter)->result();
		elseif($this->uri->segment(4) == 'report_status')
		    $report = $this->report_db->status($filter)->result();
		elseif($this->uri->segment(4) == 'report_program')
		    $report = $this->report_db->program($filter)->result();
		elseif($this->uri->segment(4) == 'report_kategori_status')
		    $report = $this->report_db->kategori_status($filter)->result();
		elseif($this->uri->segment(4) == 'report_media')
		    $report = $this->report_db->media($filter)->result();
		elseif($this->uri->segment(4) == 'report_program_status')
		    $report = $this->report_db->program_status($filter)->result();
		elseif($this->uri->segment(4) == 'report_pelaku_status')
		    $report = $this->report_db->pelaku_status($filter)->result();
		elseif($this->uri->segment(4) == 'report_wilayah_status'){
		    
		    if($filter_laporan['provinsi'] == 'all'){
			$level = 'nasional';
			$height = $this->region_db->provinsi()->num_rows() * 60;
		    }
		    
		    if($filter_laporan['provinsi'] != 'all'){
			$level = 'provinsi';
			if($role_level->level_id == 2)
			    $filter_laporan['provinsi'] = $role_level->provinsi_id;
			    
			$height = $this->region_db->kabkota(array('provinsi_id' => $filter_laporan['provinsi']))->num_rows() * 60;
			//echo $height;
		    }
		    
		    if($filter_laporan['kabkota'] != 'all'){
			$level = 'kabkota';
			
			if($role_level->level_id == 3)
			    $filter_laporan['kabkota'] = $role_level->kabkota_id;
			    
			$height = $this->region_db->kecamatan(array('kabkota_id' => $filter_laporan['kabkota']))->num_rows() * 60;
			
			//echo $height;
		    }
		    
		    $report = $this->report_db->wilayah_status($filter, $level)->result();
		    //echo $this->db->last_query();
		}
		
		$chart = ($this->uri->segment(3) ? $this->uri->segment(4) : 'report_kategori');
		
	    ?>
		<?php if($this->uri->segment(4) == 'report_wilayah_status'):?>
		    <div id="show_chart" align="center" style="height:<?=$height?>px; max-width:1000px"><!--Chart Di Load disini--></div>
		<?php else:?>
		    <div id="show_chart" align="center" style="height:450px; max-width:1000px"><!--Chart Di Load disini--></div>
		<?php endif;?>
		<?=$this->load->view('handling/modules/chart/'.$chart,array('report' =>$report));?>
	    <!--end content-->
        </div>
    </div>
</div>
</div>