<?php $filter=$this->session->userdata('filter');?>

<div id="wrap">
<div class="container">
  <?php
  $notif=$this->session->flashdata('success');
  if($notif):
  ?>
    <div class="alert <?=($notif['status'] ? 'alert-success' : 'alert-danger')?>"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?=$notif['msg']?></div>
  <?php endif;?>
  <div>&nbsp</div>
  <form class="form-inline" role="form" action='<?=site_url('handling/pengaduan/post_filter_pengaduan')?>' method='post'>
    <div class="form-group">
      <button type="button" class="btn_pengaduan btn btn-default" data-label='Input Pengaduan' data-toggle="modal" data-href='<?=site_url('handling/pengaduan/modal_pengaduan/add')?>'  data-action='<?=site_url('handling/pengaduan/post_pengaduan')?>' data-target="#modal_pengaduan">Input Pengaduan</button>
    </div>
    <div class="form-group">
      <label class="sr-only" for="exampleInputPassword2">Tahun</label>
      <select class="form-control" name='tahun'>
          <option value='all'>-Seluruh Tahun-</option>
          <?php for($tahun=2014; $tahun<=date('Y'); $tahun++): ?>
            <?php if($filter && $filter['tahun']==$tahun):?>
              <option value="<?=$tahun?>" selected><?=$tahun?></option>
            <?php else:?>
              <option value="<?=$tahun?>"><?=$tahun?></option>
            <?php endif;?>
            
          <?php endfor;?>
      </select>
    </div>
    <div class="form-group">
      <label class="sr-only" for="exampleInputPassword2">Triwulan</label>
      <select class="form-control" name='triwulan'>
          <option value='all'>-Seluruh Triwulan-</option>
          <?php
          $triwulan=array(
              1 => 'I',
              2 => 'II',
              3 => 'III',
              4 => 'IV'
          );
          foreach($triwulan as $row=>$value):
          ?>
          <?php if($filter && $filter['triwulan']==$row):?>
            <option value="<?=$row?>" selected><?=$value?></option>
          <?php else:?>
            <option value="<?=$row?>"><?=$value?></option>
          <?php endif;?>
          <?php
          endforeach;
          ?>
      </select>
    </div>
    
    <?php if($this->role_level->level_id == 1):?>
    <div class="form-group">
        <select class="form-control" id="provinsi" name='provinsi'>
          <option value='all'>-Seluruh provinsi-</option>
          
            <?php
            $provinsi=$this->region_db->provinsi()->result();
            foreach($provinsi as $row):
            ?>
              <option value='<?=$row->id?>' <?=($row->id == $filter['provinsi'] ? 'selected' : '')?>><?=$row->name?></option>
            <?php endforeach;?>
          
      </select>
    </div>
    <?php endif;?>
    
    <?php if($this->role_level->level_id == 1 || $this->role_level->level_id == 2):?>
    <div class="form-group">
        <select class="form-control" id="kabkota" name='kabkota'>
          <option value='all'>-Seluruh kabupaten/Kota-</option>
          
            <?php
            if($this->role_level->level_id == 2){
		$filter['provinsi'] = $this->role_level->provinsi_id;
            }
            
            $kabkota=$this->region_db->kabkota(array('provinsi_id' => $filter['provinsi']))->result();
            foreach($kabkota as $row):
            ?>
            
            <?php
              if($row->id == $filter['kabkota']):
            ?>
              <option value='<?=$row->id?>' selected><?=$row->name?></option>
              <?php else:?>
              <option value='<?=$row->id?>'><?=$row->name?></option>
              <?php endif;?>
            <?php endforeach;?>
          
      </select>
    </div>
    <?php endif;?>
    <button type="submit" class="btn btn-default">Tampilkan</button>
  </form>
  <div>&nbsp</div>
  <div class="table-responsive">
  <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="pengaduan">
      <thead>
	  <tr>              
	      <th>&nbsp;</th>
	      <th><input id="i-1" type="text" name="search_1" value="" class="form-control input-sm search_init" /></th>
	      <th><input id="i-2" type="text" name="search_2" value="" class="form-control input-sm search_init" /></th>
	      <th><input id="i-3" type="text" name="search_3" value="" class="form-control input-sm search_init" /></th>
	      <th><input id="i-4" type="text" name="search_4" value="" class="form-control input-sm search_init" /></th>
	      <th><input id="i-5" type="text" name="search_5" value="" class="form-control input-sm search_init" /></th>
	      <th><input id="i-6" type="text" name="search_6" value="" class="form-control input-sm search_init" /></th>
	      <th><input id="i-7" type="text" name="search_7" value="" class="form-control input-sm search_init" /></th>
	      <th><input id="i-8" type="text" name="search_8" value="" class="form-control input-sm search_init" /></th>
	      <th>&nbsp;</th>
	  </tr>
          <tr>
              <th>#</th>
              <th>Tanggal</th>
              <th>Kode Pengaduan</th>
              <th>Ditujukan Kepada</th>
              <th>kategori</th>
              <th>Deskripsi</th>
              <th>Lokasi</th>
              <th>Daerah</th>
              <th>Status</th>
              <th>Action</th>
          </tr>
      </thead>
      <tbody>					
        <tr>                    
          <td colspan="9" class="dataTables_empty">Loading data from server</td>
        </tr>
      </tbody>
      <tfoot>
        
      </tfoot>
  </table>
  </div>
</div>
</div>