<?php
    if(isset($pengaduan_id))
	$pengaduan = $this->pengaduan_db->get($pengaduan_id)->row();
?>
<div class="row">
		    <div class="col-md-12">
			
				
				    <h4>Ditujukan Kepada</h4>
				    <div class='row'>
					
					<div class="col-md-10 col-md-offset-2">
					    <div class='form-group'>
						<select name='level' id='level' class="form-control" required data-bv-notempty-message="Harus diisi">
						  <option value=''>-- pilih --</option>
						  <?php 
						      $level = $this->select_db->level()->result();
						      foreach($level as $row):?>
							<option value="<?=$row->id?>" <?=(isset($pengaduan) && $pengaduan->level_id == $row->id ? 'selected' : '')?>>Tim  <?=ucfirst($row->name)?></option>
						  <?php endforeach;?>
						</select>
					    </div>
					</div>
				    </div>
				    <h4>Lokasi Kejadian</h4>
				    <div class='row'>
					<div class="col-md-2">Daerah</div>
					<div class="col-md-10">
					    <div class="row">
						<div class="col-md-4">
						    <div class='form-group'>
							<label for="">Provinsi</label>
							<select name='provinsi' id='provinsi1' class="form-control"  required data-bv-notempty-message="Harus diisi">
							  <option value=''>-- pilih provinsi --</option>
							  <?php foreach($this->region_db->provinsi()->result() as $row):?>
							      <option value='<?=$row->id?>' <?=(isset($pengaduan) && $pengaduan->provinsi_id == $row->id ? 'selected' : '')?>><?=$row->name?></option>
							  <?php endforeach;?>
							  
							</select>
						    </div>
						</div>
						<div class="col-md-4">
						    <div class='form-group'>
							<label for="">Kabupaten / Kota</label>
							<select name='kabkota' id='kabkota1' class="form-control"  required data-bv-notempty-message="Harus diisi">
							  <option value=''>-- pilih kab/kota --</option>
							  <?php if(isset($pengaduan)):?>
							      <?php foreach($this->region_db->kabkota(array('provinsi_id' => $pengaduan->provinsi_id))->result() as $row):?>
								  <option value='<?=$row->id?>' <?=(isset($pengaduan) && $pengaduan->kabkota_id == $row->id ? 'selected' : '')?>><?=$row->name?></option>
							      <?php endforeach;?>
							  <?php endif;?>
							</select>
						    </div>
						</div>
						<div class="col-md-4">
						    <div class='form-group'>
							<label for="">Kecamatan</label>
							<select class="form-control" id='kecamatan1' name='kecamatan'  required data-bv-notempty-message="Harus diisi">
							  <option value=''>-- pilih kecamatan --</option>
							  <?php if(isset($pengaduan)):?>
							      <?php foreach($this->region_db->kecamatan(array('kabkota_id' => $pengaduan->kabkota_id))->result() as $row):?>
								  <option value='<?=$row->id?>' <?=(isset($pengaduan) && $pengaduan->kecamatan_id == $row->id ? 'selected' : '')?>><?=$row->name?></option>
							      <?php endforeach;?>
							  <?php endif;?>
							</select>
						    </div>
						</div>
					    </div>
					</div>
				    </div>
				    <div class='row'>
					<div class="col-md-2">Lokasi</div>
					<div class="col-md-10">
					    <div class='form-group'>
						<select name='lokasi' id='lokasi' class="form-control"  required data-bv-notempty-message="Harus diisi">
						    <option value=''>-- pilih --</option>
						    <?php foreach($this->select_db->lokasi()->result() as $row):?>
							<option value='<?=$row->id?>' <?=(isset($pengaduan) && $pengaduan->lokasi_id == $row->id ? 'selected' : '')?>><?=$row->name?></option>
						    <?php endforeach;?>	
						</select>
					    </div>
					</div>
				    </div>
				    
				    <div class='row' id='jenjang_sekolah_form' style='display:none'>
					<div class="col-md-2">Jenjang</div>
					<div class="col-md-10">
					    <div class="row">
						<div class="col-md-6">
						    <div class='form-group'>
							<label for="">Status Madrasah</label>
							<select name='status_sekolah' id='status_sekolah' class="form-control" data-bv-notempty-message="Harus diisi">
							  <option value=''>-- pilih --</option>
							  <?php 
							      $status_sekolah = $this->select_db->status_sekolah()->result();
							      foreach($status_sekolah as $row):?>
								<option value="<?=$row->id?>" <?=(isset($pengaduan) && $pengaduan->status_sekolah_id == $row->id ? 'selected' : '')?>><?=$row->name?></option>
							  <?php endforeach;?>
							</select>
						    </div>
						</div>
						<div class="col-md-6">
						    <div class='form-group'>
							<label for="">Jenjang Pendidikan</label>
							<select name='jenjang' id='jenjang' class="form-control" data-bv-notempty-message="Harus diisi">
							  <option value=''>-- pilih --</option>
							  <?php 
							      $jenjang = $this->select_db->jenjang()->result();
							      foreach($jenjang as $row):?>
								<option value="<?=$row->id?>" <?=(isset($pengaduan) && $pengaduan->jenjang_id == $row->id ? 'selected' : '')?>><?=$row->name?></option>
							  <?php endforeach;?>
							</select>
						    </div>
						</div>
					    </div>
					    
					</div>
				    </div>
				    <div class='row'>
					<div class="col-md-2" id='title_nama_lokasi'>Nama Lokasi</div>
					<div class="col-md-10">
					    <div class="form-group">
						<input  required name='nama_lokasi' id='nama_lokasi' type="text" class="form-control" placeholder="Nama Lokasi" data-bv-notempty-message="Harus diisi" value="<?=(isset($pengaduan) ? $pengaduan->nama_lokasi : '')?>">
					    </div>
					</div>
				    </div>
				    
				    <h4>Kategori Pengaduan</h4>
				    <div class='row'>
					<div class="col-md-2">Kategori</div>
					<div class="col-md-10">
					    <div class='form-group'>
						<select name='kategori' id='kategori' class="form-control" required data-bv-notempty-message="Harus diisi">
						  <option value=''>-- pilih --</option>
						  <?php 
						      $kategori = $this->select_db->kategori()->result();
						      foreach($kategori as $row):?>
						      <option value="<?=$row->id?>" <?=(isset($pengaduan) && $pengaduan->kategori_id == $row->id ? 'selected' : '')?>><?=$row->name?></option>
						  <?php endforeach;?>
						</select>
					    </div>
					</div>
				    </div>
				    
				    <?php if(isset($pengaduan)){
					$person = $this->person_db->get_info($pengaduan->person_id);
				    }?>
				    <h4>Identitas Pelapor</h4>
				    <div class='row'>
					<div class="col-md-2">Profesi</div>
					<div class="col-md-10">
					    <div class='form-group'>
						<select name='sumber_info' id='sumber_info' class="form-control" required data-bv-notempty-message="Harus diisi">
						  <option value=''>-- pilih --</option>
						  <?php 
						      $sumber = $this->select_db->sumber_info($role='home')->result();
						      foreach($sumber as $row):?>
						      <option value="<?=$row->id?>" <?=(isset($pengaduan) && $pengaduan->sumber_id == $row->id ? 'selected' : '')?>><?=$row->name?></option>
						  <?php endforeach;?>
						</select>
					    </div>
					    <div class="form-group" id='sumber_lain_form' style='display:none'>
						<input name='sumber_lain' id='sumber_lain' type="text" class="form-control" value="<?=(isset($pengaduan)  ? $pengaduan->sumber_lain : '')?>" placeholder="Sumber lain">
					    </div>
					</div>
				    </div>
				    <div class='row'>
					<div class="col-md-2">Nama</div>
					<div class="col-md-10">
					    <div class="form-group">
						<input name='nama' id='nama' type="text" class="form-control" placeholder="Nama" value="<?=(isset($person) ? $person->nama : '')?>">
					    </div>
					    
					</div>
				    </div>
				    <div class='row'>
					<div class="col-md-2">Email</div>
					<div class="col-md-10">
					    <div class="form-group">
						<input name='email' type="email" class="form-control" placeholder="Email" value="<?=(isset($person) ? $person->email : '')?>">
					    </div>
					</div>
				    </div>
				    <div class='row'>
					<div class="col-md-2">Telp</div>
					<div class="col-md-10">
					    <div class="form-group">
						<input id='telp' name='telp' type="text" class="form-control" placeholder="Telp" value="<?=(isset($person) ? $person->handphone : '')?>" >
					    </div>
					    
					</div>
				    </div>
				    <div class='row'>
					<div class="col-md-2">Alamat</div>
					<div class="col-md-10">
					    <div class="form-group">
						<textarea name='alamat' id='alamat' class="form-control" rows="3" ><?=(isset($person) ? $person->alamat_rumah : '')?></textarea>
					    </div>
					    
					</div>
				    </div>
				    
				    <h4>Deskripsi</h4>
				    <div class='row'>
					<div class="col-md-2" id='title_deskripsi'></div>
					<div class="col-md-10">
					    <div class="form-group">
						<textarea name='deskripsi' id='deskripsi' class="form-control" rows="3" required data-bv-notempty-message="Harus diisi"><?=(isset($pengaduan) ? $pengaduan->deskripsi : '')?></textarea>
					    </div>
					</div>
				    </div>
				    
				    
				
		    </div>
		</div>