<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> 
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en"> 
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <!--<title><?=$title?></title>-->
    <title><?=(isset($title) ? $title : 'Pusat Informasi Kurikulum Nasional')?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Pengaduan Masyarakat">
    <meta name="author" content="Esmet">
    <meta charset="UTF-8">

    <?=$this->load->view('home/inc/script_header');?>
</head>
<body>

    <!-- This one in here is responsive menu for tablet and mobiles -->
    

    <?=$this->load->view('home/inc/header_menu')?>
    
    
    <?=$this->load->view('home/inc/content/'.$content)?>

    <!-- begin The Footer -->
    <?=$this->load->view('home/inc/footer')?>


    <?=$this->load->view('home/inc/script_footer')?>

</body>
</html> 
