		<?php if(isset($halaman) && in_array($halaman, array('usulan_buku','usulan_standar_pendidikan','usulan_kerangka_dasar_dan_struktur'))): $category = $this->session->userdata($halaman.'_category'); ?>
		<div class="widget-main">
                    <div class="widget-main-title">
                        <h4 class="widget-title">Kategori</h4>
                    </div>
                    <div class="widget-inner">
                        <div class="blog-categories">
			    <?php //print_r($tag)?>
                            <div class="row">
                                <?php if($halaman == 'usulan_buku'): // if referensi buku 6?>
                                <form method='post' action='<?=site_url('home/halaman/post_filter/'.$halaman)?>'>
                                <div class="form-group">
				    <div class="row">
					<div class="col-lg-12">
					    <div class="form-group">
						<label for="">Buku</label>
						<select class="form-control" name='category[]'>
						    <option value="null">-pilih-</option>
						    <?php
							$kategori_referensi_buku = $this->select_db->kategori_referensi_buku();
							foreach($kategori_referensi_buku->result() as $row):
						    ?>
						    <option value='<?=$row->id?>' <?=(isset($category[0]) && $category[0] == $row->id ? 'selected' : '')?>><?=$row->name?></option>
						    
						    <?php endforeach;?>
						</select>
					    </div>
					</div>
				    </div>
				    
				    <div class="row">
					<div class="col-lg-12">
					    <div class="form-group">
						<label for="">Jenjang Pendidikan</label>
						<select class="form-control" id="jenjang" name='category[]'>
						    <option value="null">-pilih-</option>
						<?php
						    $jenjang = $this->select_db->jenjang(array('in_id'=> array(1,2,3)));
						    foreach($jenjang->result() as $row):
						?>
						    <option value="<?=$row->id?>" <?=(isset($category[1]) && $category[1] == $row->id ? 'selected' : '')?>><?=$row->desc_name?></option>
						    
						<?php endforeach;?>
						</select>
					    </div>
					</div>
				    </div>
				    
				    <div class="row">
					<div class="col-lg-12">
					    <div class="form-group">
						<label for="">Mata Pelajaran</label>
						<select class="form-control" id='mata_pelajaran' name='category[]'>
						    <option value="null">-pilih-</option>
						    <?php if(isset($category[1]) && $category[1]!='null'):?>
							<?php foreach($this->select_db->kategori_mata_pelajaran(array('jenjang_id' => $category[1]))->result() as $row):?>
							    <option value="<?=$row->id?>" <?=(isset($category[2]) && $category[2] == $row->id ? 'selected' : '')?>><?=$row->name?></option>
							<?php endforeach;?>
						    <?php endif;?>
						</select>
					    </div>
					</div>
				    
				    </div>
				    
				    <div class="row">
					<div class="col-lg-12">
					    <div class="form-group">
						<input type="submit" value='Proses' class="btn btn-primary btn-lg btn-block" >
						  

					    </div>
					</div>
				    
				    </div>
				</div>
				</form>
                                <?php endif; // end if kategori referensi buku 6?>
                                
                                <?php if($halaman == 'usulan_standar_pendidikan'): // if referensi standar pendidikan 7?>
                                <form method='post' action='<?=site_url('home/halaman/post_filter/'.$halaman)?>'>
                                <div class="form-group">
				    <div class="row">
					<div class="col-lg-12">
					    <div class="form-group">
						<label for="">Standar Pendidikan</label>
						<select class="form-control" name='category[]'>
						    <option value="null">-pilih-</option>
						    <?php
							$kategori_standar_pendidikan = $this->select_db->kategori_standar_pendidikan();
							foreach($kategori_standar_pendidikan->result() as $row):
						    ?>
							<option value="<?=$row->id?>" <?=(isset($category[0]) && $category[0] == $row->id ? 'selected' : '')?>><?=$row->name?></option>
							
						    <?php endforeach;?>
						</select>
					    </div>
					</div>
					
				    </div>
				    
				    <div class="row">
					<div class="col-lg-12">
					    <div class="form-group">
						<input type="submit" value='Proses' class="btn btn-primary btn-lg btn-block" >
						  

					    </div>
					</div>
				    
				    </div>
				</div>
				</form>
                                <?php endif; // end if kategori referensi buku 7?>
                                
                                <?php if($halaman == 'usulan_kerangka_dasar_dan_struktur'): // if referensi kerangka dasar dan struktur 8?>
                                <form method='post' action='<?=site_url('home/halaman/post_filter/'.$halaman)?>'>
                                <div class="form-group">
				    <div class="row">
					<div class="col-lg-12">
					    <div class="form-group">
						<label for="">Jenjang Pendidikan</label>
						<select class="form-control" id="jenjang" name='category[]'>
						    <option value="null">-pilih-</option>
						    <?php
							$jenjang = $this->select_db->jenjang(array('in_id'=> array(1,2,3)));
							foreach($jenjang->result() as $row):
						    ?>
							<option value="<?=$row->id?>" <?=(isset($category[0]) && $category[0] == $row->id ? 'selected' : '')?>><?=$row->desc_name?></option>
							
						    <?php endforeach;?>
						</select>
					    </div>
					</div>
				    </div>
				    
				    <div class="row">
					<div class="col-lg-12">
					    <div class="form-group">
						<label for="">Mata Pelajaran</label>
						<select class="form-control" id='mata_pelajaran' name='category[]'>
						    <option value="null">-pilih-</option>
						    <?php if(isset($category[0]) && $category[0]!='null'):?>
							<?php foreach($this->select_db->kategori_mata_pelajaran(array('jenjang_id' => $category[0]))->result() as $row):?>
							    <option value="<?=$row->id?>" <?=(isset($category[1]) && $category[1] == $row->id ? 'selected' : '')?>><?=$row->name?></option>
							<?php endforeach;?>
						    <?php endif;?>
						</select>
					    </div>
					</div>
				    </div>
				    
				    <div class="row">
					<div class="col-lg-12">
					    <div class="form-group">
						<input type="submit" value='Proses' class="btn btn-primary btn-lg btn-block" >
						  

					    </div>
					</div>
				    
				    </div>
				</div>
				</form>
                                <?php endif; // end if kategori referensi kerangka dasar dan struktur 8?>
                                
                            </div>
                            
                        </div> <!-- /.blog-categories -->
                    </div> <!-- /.widget-inner -->
                </div> <!-- /.widget-main -->
                <?php endif;?>
		
		<?php if(isset($tag) && $tag):?>
		<div class="widget-main">
                    <div class="widget-main-title">
                        <h4 class="widget-title">Tag</h4>
                    </div>
                    <div class="widget-inner">
                        <div class="blog-categories">
			    <?php //print_r($tag)?>
                            <div class="row">
                                <ul class="col-md-6">
				    <?php foreach($tag as $tag_name => $tag_value):?>
					<li><a href="<?=site_url($halaman.'/tag/'.$tag_name)?>"> <?=$tag_name?></a></li>
				    <?php endforeach;?>
                                    
                                </ul>
                                
                            </div>
                            
                        </div> <!-- /.blog-categories -->
                    </div> <!-- /.widget-inner -->
                </div> <!-- /.widget-main -->
                <?php endif;?>
		<?php if($halaman != 'berita'):?>
		<!-- show berita -->
		<div class="widget-main">
		    <div class="widget-main-title">
			<h4 class="widget-title">Berita</h4>
		    </div> <!-- /.widget-main-title -->
		    <div class="widget-inner">
			<?php if($berita->num_rows()) foreach($berita->result() as $row):?>
			<?php  if($row->published):?>
			    <div class="blog-list-post clearfix">
				<?php 
				/*
				<div class="blog-list-thumb">
				    <a href="blog-single.html"><img src="http://placehold.it/65x65" alt=""></a>
				</div>
				*/
				?>
				
				<div class="blog-list-details">
				    <h5 class="blog-list-title"><a href="<?=site_url('berita/'.$row->id)?>"><?=$row->title?></a></h5>
				    <p class="blog-list-meta small-text"><span><a href="#"><?=$row->last_update?></a></span> <!--with <span><a href="#">3 comments</a></span>--></p>
				</div>
			    </div> <!-- /.blog-list-post -->
			<?php endif;?>
			<?php endforeach;?>
			
			<div class="blog-list-post clearfix">
			    <p class="text-right"><a href='<?=site_url('halaman/berita')?>'>Index</a></p>
			</div>
		    </div> <!-- /.widget-inner -->
		</div> <!-- /.widget-main -->
		<!-- ./show berita -->
		<?php endif;?>
		
		<?php if($halaman != 'agenda'):?>
		<!-- show berita -->
		<div class="widget-main">
		    <div class="widget-main-title">
			<h4 class="widget-title">Agenda</h4>
		    </div> <!-- /.widget-main-title -->
		    <div class="widget-inner">
			<?php if($agenda->num_rows()) foreach($agenda->result() as $row):?>
			<?php  if($row->published):?>
			    <div class="blog-list-post clearfix">
				<?php 
				/*
				<div class="blog-list-thumb">
				    <a href="blog-single.html"><img src="http://placehold.it/65x65" alt=""></a>
				</div>
				*/
				?>
				
				<div class="blog-list-details">
				    <h5 class="blog-list-title"><a href="<?=site_url('agenda/'.$row->id)?>"><?=$row->title?></a></h5>
				    <p class="blog-list-meta small-text"><span><a href="#"><?=$row->last_update?></a></span> <!--with <span><a href="#">3 comments</a></span>--></p>
				</div>
			    </div> <!-- /.blog-list-post -->
			<?php endif;?>
			<?php endforeach;?>
			
			<div class="blog-list-post clearfix">
			    <p class="text-right"><a href='<?=site_url('halaman/berita')?>'>Index</a></p>
			</div>
		    </div> <!-- /.widget-inner -->
		</div> <!-- /.widget-main -->
		<!-- ./show berita -->
		<?php endif;?>