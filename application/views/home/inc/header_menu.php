    <header class="site-header">
        <table align='center' width='100%'>
	    <tr style='background-color:#fec841;'>
		<td align='center'><h2 style='color:white;'><img src="<?=$this->config->item('home_img')?>/logo_kurikulum_2013.png" alt="kemdikbud" width="50" > <strong>Laman Informasi Kurikulum Nasional</strong></h2></td>
		
	    </tr>
	    <tr style='background-color:white;'>
		<td>&nbsp;</td>
	    </tr>
        </table>

        <div class="nav-bar-main" role="navigation">
            <div class="container">
                <div class="masthead">
		  <nav>
		    <ul class="nav nav-justified" style="font-size:20px; font-weight: bold; color:white; margin:10px">
		      <li class="active"><a href="<?=site_url('')?>">Home</a></li>
		      <li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Kurikulum <span class="caret"></span></a>
			<ul class="dropdown-menu" role="menu">
			  <li><a href="<?=site_url('tentang_kurikulum')?>">Tentang Kurikulum</a></li>
			  <li class="divider"></li>
			  <li><a href="<?=site_url('perkembangan_kurikulum')?>">Perkembangan Kurikulum</a></li>
			</ul>
		      </li>
		      <li><a href="<?=site_url('usulan')?>">Masukan</a></li>
		      <li><a href="<?=site_url('pengaduan')?>">Pengaduan</a></li>
		      
		    </ul>
		  </nav>
		</div>
            </div> <!-- /.container -->
        </div> <!-- /.nav-bar-main -->

    </header> <!-- /.site-header --> 
