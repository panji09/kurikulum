			  <div class="modal fade" id="myModal_kehadiran" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
				    <form method='post' action='<?=site_url('home/home/post_kehadiran')?>' enctype="multipart/form-data">
				    <div class="modal-content">
					<div class="modal-header">
					    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					    <h4 class="modal-title" id="myModalLabel">Kehadiran Diskusi</h4>
					</div> <!--./modal-header-->
				      <div class="modal-body">
					      
					      <div class="form-group">
						  <label for="nama">Nama</label>
						  <input type="text" class="form-control" id="nama" name='hadir[nama]' placeholder="Nama" required>
					      </div>
					      <div class="form-group">
						  <label for="nama">No HP</label>
						  <input type="text" class="form-control" id="no_hp" name='hadir[no_hp]' placeholder="No HP" required>
					      </div>
					      <div class="form-group">
						  <label for="nama">Email</label>
						  <input type="email" class="form-control" id="email" name='hadir[email]' placeholder="Email" required>
					      </div>
					      <div class="form-group">
						  <label for="nama">Peran / Instansi</label>
						  <input type="text" class="form-control" id="tema" name='hadir[instansi]' placeholder="Peran / Instansi" required>
					      </div>
					      <div class="form-group">
						  <label>Verifikasi</label>
						  
						      <div id="RecaptchaField2"></div>
						  
					      </div>
					  
				      </div>
				      <div class="modal-footer">
					  <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
					  <input type="submit" value="Kirim" class="btn btn-primary">
				      </div> <!--./modal-footer-->
				      </form>
				    </div> <!--./modal-content-->
				</div><!--./modal-dialog-->
			  </div>