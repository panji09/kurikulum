				    <h4>Data Diri Anda</h4>
				    <div class="form-group">
				      <label>Nama</label>
				      <input type="text" class="form-control" name='usulan[nama]' id="nama" placeholder="Nama">
				    </div>
				    <div class="form-group">
				      <label>Email</label>
				      <input type="text" class="form-control" name='usulan[email]' id="email" placeholder="Email">
				    </div>
				    
				    <div class="form-group">
				      <label>Provinsi</label>
				      <select id='provinsi' placeholder='Provinsi' class="form-control" name='usulan[provinsi]'>
					  <option value=''>-pilih-</option>
					  <?php foreach($this->location_db->get_province() as $row):?>
					  <option value='<?=$row['id']?>'><?=$row['name']?></option>
					  <?php endforeach;?>
				      </select>
				    </div>
				    
				    <div class="form-group">
				      <label>Kab / Kota</label>
				      <select id='kabkota' placeholder='Kab / Kota' class="form-control" name='usulan[kota]'>
					  <option value=''>-pilih-</option>
					  
				      </select>
				    </div>
				    
				    <div class="form-group">
				      <label>Pekerjaan</label>
				      <input type="text" class="form-control" name='usulan[pekerjaan]' id="pekerjaan" placeholder="Pekerjaan">
				    </div>
				    <div class="form-group">
				      <label>Telp</label>
				      <input type="text" class="form-control" name='usulan[telp]' id="telp" placeholder="Telp">
				    </div>
				    <div class="form-group">
				      <label>Kategori</label>
				      <select name='usulan[kategori]' id="kategori" class="form-control">
					<?php foreach($this->init_config_db->get_kategori() as $row):?>
					    <option><?=$row?></option>
					<?php endforeach;?>
				      </select>
				    </div>
				   
				    <div class="form-group">
				      <label for="">Silahkan unggah dokumen masukan anda dalam format (doc,docx,xls,xlsx,ppt,pptx,pdf,zip,rar,jpg,png)</label>
				      <input type="file" id="file" name='file'>
				    </div>
				    <div class="form-group">
					  <label>Verifikasi</label>
					  
					      <div id="RecaptchaField1"></div>
					  
				      </div>
				    
				    