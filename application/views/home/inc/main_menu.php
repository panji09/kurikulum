		<?php $page = $this->uri->segment(1);?>
		<li <?=($page == '' ? 'class="active"' : '')?>><a href="<?=site_url('')?>">Home</a></li>
		<li><a href="#">Usulan</a>
		    <ul>
			<li <?=($page == 'usulan_buku' ? 'class="active"' : '')?>><a href="<?=site_url('usulan_buku')?>">Buku</a></li>
			<li <?=($page == 'usulan_standar_pendidikan' ? 'class="active"' : '')?>><a href="<?=site_url('usulan_standar_pendidikan')?>">Standar Pendidikan</a></li>
			<li <?=($page == 'usulan_kerangka_dasar_dan_struktur' ? 'class="active"' : '')?>><a href="<?=site_url('usulan_kerangka_dasar_dan_struktur')?>">Kerangka Dasar dan Struktur</a></li>
		    </ul>
		</li>
		<li><a href="#">Pengaduan</a>
		    <ul>
			<li <?=($page == 'statistik_pengaduan' ? 'class="active"' : '')?>>
			    <a href="<?=site_url('statistik_pengaduan')?>">Statistik Pengaduan</a>
			</li>
			<li <?=($page == 'lihat_pengaduan' ? 'class="active"' : '')?>>
			    <a href="<?=site_url('lihat_pengaduan')?>">Lihat Pengaduan</a>
			</li>
			<li <?=($page == 'pengaduan' ? 'class="active"' : '')?>>
			    <a href="<?=site_url('pengaduan')?>">Pengaduan Online <span id='unapproved' style='display:none' class="badge">0</span></a>
			</li>
		    </ul>
		</li>
		<li <?=($page == 'berita' ? 'class="active"' : '')?>><a href="<?=site_url("berita")?>">Berita</a>
		</li>
		<li <?=($page == 'agenda' ? 'class="active"' : '')?>><a href="<?=site_url('agenda')?>">Agenda</a>
		    
		</li>
		
		
		<!--
		<li <?=($page == 'faq' ? 'class="active"' : '')?>>
		    <a href="<?=site_url('faq')?>">FAQ</a>
		</li>
		-->
		<li <?=($page == 'kontak' ? 'class="active"' : '')?>><a href="<?=site_url('kontak')?>">Kontak</a></li>