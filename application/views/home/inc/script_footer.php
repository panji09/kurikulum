    <script src="<?=$this->config->item('plugin')?>/bootstrap/js/bootstrap.min.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timeago/1.4.1/jquery.timeago.min.js"></script>
    <script src="<?=$this->config->item('handling_plugin')?>/moment/moment.js"></script>
    <script src="<?=$this->config->item('handling_plugin')?>/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="<?=$this->config->item('home_js')?>/plugins.js"></script>
    <script src="<?=$this->config->item('home_js')?>/custom.js"></script>
    
    <?php /*
    <!--highchart-->
    <script src="<?=$this->config->item("plugin")?>/highcharts/js/highcharts.js"></script>
    <script src="<?=$this->config->item("plugin")?>/highcharts/js/modules/exporting.src.js"></script>
    <!--./highchart-->
    */
    ?>
    
    <?php if($this->uri->segment(1)==''):?>
    <!--share this-->
    <script type="text/javascript">var switchTo5x=true;</script>
    <script type="text/javascript">
    var __st_loadLate=true; //if __st_loadLate is defined then the widget will not load on domcontent ready
    </script>
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js">
    <script type="text/javascript">stLight.options({publisher: "7163e598-47a3-41cd-b266-f25720c8a725", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
    <!--./share this-->
    <?php endif;?>
    <!--bootstrap validator-->
    <script type="text/javascript" src="<?=$this->config->item('plugin')?>/bootstrap-validator/dist/js/bootstrapValidator.min.js"></script>
    <!--./bootstrap validator-->
    
    <script>
	var CaptchaCallback = function(){
	    grecaptcha.render('RecaptchaField1', {'sitekey' : '<?=$this->config->item('recaptcha_site_key')?>'});
	    grecaptcha.render('RecaptchaField2', {'sitekey' : '<?=$this->config->item('recaptcha_site_key')?>'});
	    grecaptcha.render('RecaptchaField3', {'sitekey' : '<?=$this->config->item('recaptcha_site_key')?>'});
	    
	};
	$(document).ready(function(){
	
	    $("abbr.timeago").timeago();
	    setInterval(function() {
		  // Do something every 15 seconds
		  $( "#unapproved" ).load( "<?=site_url('services/get_count_unapproved')?>", function(data) {
		    if(parseInt(data)){
			$( "#unapproved" ).show();
		    }else{
			$( "#unapproved" ).hide();
		    }
		  });
	    }, 5000);
	    
	    
		
	    
	    
	    $("a[href$='.pdf']").each(function(){
		$(this).attr('href', '<?=site_url("media/plugin/viewerjs/#")?>' + $(this).attr('href'));
	    });
	    
	    $("a[href$='.odt']").each(function(){
		$(this).attr('href', '<?=site_url("media/plugin/viewerjs/#")?>' + $(this).attr('href'));
	    });
	    
	    $("a[href$='.odp']").each(function(){
		$(this).attr('href', '<?=site_url("media/plugin/viewerjs/#")?>' + $(this).attr('href'));
	    });
	    
	    $("a[href$='.ods']").each(function(){
		$(this).attr('href', '<?=site_url("media/plugin/viewerjs/#")?>' + $(this).attr('href'));
	    });
	    
	    $('.input-group.date').datetimepicker({
		language: "id",
		showToday: true,
		useCurrent: true,               //when true, picker will set the value to the current date/time
		pick12HourFormat: false,
		format: 'DD/MM/YYYY HH:mm',
		sideBySide: true
	    });
	    
	    
	    
	    $("#jenjang").change(function() {
		$("#mata_pelajaran").empty();
		$("#mata_pelajaran").append('<option value="null">-pilih-</option>');
		$.post('<?=site_url('services/get_kategori_mata_pelajaran')?>', { jenjang_id : $("#jenjang option:selected").val() }, function(data) {
			$.each(data, function(i, item){
			    $("#mata_pelajaran").append(
				'<option value="' + item.id + '">'+item.name + '</option>'
			    );
			})
		    },
		    'json'
		);
	    });
	    //console.log(firstDiv);
	});
    </script>
    <?php $page = $this->uri->segment(1);?>
    
    <!--page pengaduan -->
    <?php if(in_array($page, array('pengaduan'))):?>
    <script>
	$(document).ready(function() {
	    $('#kategori').change(function(){
		$('#title_deskripsi').html($('#kategori option:selected').text());
	    });
	    
	    $('#pengaduan_form').bootstrapValidator();
	});
    </script>
    <?php endif;?>
    <!--./page pengaduan -->
    
    <?php if(in_array($page, array('kontak'))):?>
    <script>
        function initialize() {
	  var pos = new google.maps.LatLng(-6.225662, 106.801874);
          var mapOptions = {
            zoom: 15,
            center: pos
            
          };

          var map = new google.maps.Map(document.getElementById('map-canvas'),
              mapOptions);
          
          var infowindow = new google.maps.InfoWindow({
	    map: map,
	    position: pos,
	    content: 'Kementerian Pendidikan dan Kebudayaan',
	    maxWidth: 100
	  });
        }

        function loadScript() {
          var script = document.createElement('script');
          script.type = 'text/javascript';
          script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&' +
              'callback=initialize';
          document.body.appendChild(script);
        }

        window.onload = loadScript;
    </script>
    <?php endif;?>
    <!--./kontak-->
    
    <?php if(in_array($page, array('berita','berita_kurikulum','agenda','usulan_buku','usulan_standar_pendidikan','usulan_kerangka_dasar_dan_struktur'))):?>
    <script>
        $(document).ready(function(){
	    $('#loadmore').click(function(){
		var last_id = $('.content_halaman').last().attr('id');
		$.get( "<?=site_url('home/halaman/ajax_loadmore/'.$halaman)?>/"+last_id, function( data ) {
		    $( "#list_content_halaman" ).append( data);
		
		});
		//alert(last_id);
	    });
	    
	    
        });
    </script>
    <?php endif;?>
    
    <?php if(in_array($page, array('pengaduan'))):?>
    <script>
        $(document).ready(function(){
	    $('#sumber_info').change(function(){
		if($('#sumber_info').val() == 9999){
		    $('#sumber_lain_form').show();
		}else{
		    $('#sumber_lain_form').hide();
		}
	    });
	    
	    $('#lokasi').change(function(){
		if($('#lokasi').val() == 1){
		    $('#jenjang_sekolah_form').show();
		    $('#title_nama_lokasi').html('Nama Sekolah');
		    $('#nama_lokasi').attr('placeholder','Nama Sekolah');
		}else if($('#lokasi').val() == 9999){
		    $('#jenjang_sekolah_form').hide();
		    $('#title_nama_lokasi').html('Nama Lokasi');
		    $('#nama_lokasi').attr('placeholder','Nama Lokasi');
		}
	    });
	    
	    
        });
    </script>
    <?php endif;?>
    <!--./kotak-->
    <?php if(in_array($page, array('usulan'))):?>
	<script>
	$(document).ready(function() {	
	    
	    $("#provinsi").change(function() {
		$("#kabkota").empty();
		$("#kabkota").append('<option value="">-pilih-</option>');
		
		$.post('<?=site_url('services/get_kabkota')?>', { provinsi_id : $("#provinsi option:selected").val() }, function(data) {
			$.each(data, function(i, item){
			    $("#kabkota").append(
				'<option value="' + item.id + '">'+item.name + '</option>'
			    );
			})
		    },
		    'json'
		);
	    });
	    
	    
	    
	    
		    
	});
	</script>
    
    <?php endif;?>
    
   
    
    <!--page statistik pengaduan-->
    <?php 
	$page_report = $this->uri->segment(3);
	$page_tahun = $this->uri->segment(4);
	$page_triwulan = $this->uri->segment(5);
    ?>
    <?php if($page=='statistik_pengaduan'):?>
    
		<!--report kategori-->
		<?php if($page_report=='report_kategori'):
		
		    $data = $kat_pengaduan = array();
		    foreach($report as $row){
			$data[]="['".$row->kategori."'".','.$row->total.']';
			$kat_pengaduan[]="'".$row->kategori."'";
		    }
		?>
		<script>
		$(document).ready(function(){
		    var data = [<?=implode(",",$data)?>];
		      
		    
		    // Build the chart
		    $('#show_chart').highcharts({
			chart: {
			    plotBackgroundColor: null,
			    plotBorderWidth: null,
			    plotShadow: false
			},
			credits: {
			    enabled: false
			},
			title: {
			    text: 'JUMLAH PENGADUAN BERDASARKAN KATEGORI<br>TAHUN <?=($page_tahun=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $page_tahun)?>'
			},
			tooltip: {
				pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions: {
			    pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
				    enabled: false
				},
				showInLegend: true
			    }
			},
			series: [{
			    type: 'pie',
			    name: 'Kategori Pengaduan',
			    data: data
			}]
		    });
		});
		</script>
		<?php endif;?>
		<!--./report kategori-->
		
		<!--report sumber informasi-->
		<?php if($page_report=='report_sumber_info'):
		
		    $data = $kat_pengaduan = array();
		    foreach($report as $row){
			$data[]="['".$row->sumber."'".','.$row->total.']';
			$kat_pengaduan[]="'".$row->sumber."'";
		    }
		?>
		<script>
		$(document).ready(function(){
		    var data = [<?=implode(",",$data)?>];
		      
		    // Build the chart
		    $('#show_chart').highcharts({
			chart: {
			    plotBackgroundColor: null,
			    plotBorderWidth: null,
			    plotShadow: false
			},
			credits: {
			    enabled: false
			},
			title: {
			    text: 'JUMLAH PENGADUAN BERDASARKAN SUMBER INFORMASI<br>TAHUN <?=($page_tahun=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $page_tahun)?>'
			},
			tooltip: {
				pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions: {
			    pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
				    enabled: false
				},
				showInLegend: true
			    }
			},
			series: [{
			    type: 'pie',
			    name: 'Sumber Informasi',
			    data: data
			}]
		    });
		});
		</script>
		<?php endif;?>
		<!--./report sumber informasi-->
		
		<!--report status-->
		<?php if($page_report=='report_status'):
		
		    $data = $kat_pengaduan = array();
		    foreach($report as $row){
			$data[]="['".$row->status."'".','.$row->total.']';
			$kat_pengaduan[]="'".$row->status."'";
		    }
		?>
		<script>
		$(document).ready(function(){
		    var data = [<?=implode(",",$data)?>];
		      
		    // Build the chart
		    $('#show_chart').highcharts({
			chart: {
			    plotBackgroundColor: null,
			    plotBorderWidth: null,
			    plotShadow: false
			},
			credits: {
			    enabled: false
			},
			title: {
			    text: 'JUMLAH PENGADUAN BERDASARKAN STATUS PENGADUAN<br>TAHUN <?=($page_tahun=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $page_tahun)?>'
			},
			tooltip: {
				pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions: {
			    pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
				    enabled: false
				},
				showInLegend: true
			    }
			},
			series: [{
			    type: 'pie',
			    name: 'Status Pengaduan',
			    data: data
			}]
		    });
		});
		</script>
		<?php endif;?>
		<!--./report status-->
		
		<!--report program-->
		<?php if($page_report=='report_program'):
		
		    $data = $kat_pengaduan = array();
		    foreach($report as $row){
			$data[]="['".$row->program."'".','.$row->total.']';
			$kat_pengaduan[]="'".$row->program."'";
		    }
		?>
		<script>
		$(document).ready(function(){
		    var data = [<?=implode(",",$data)?>];
		      
		    // Build the chart
		    $('#show_chart').highcharts({
			chart: {
			    plotBackgroundColor: null,
			    plotBorderWidth: null,
			    plotShadow: false
			},
			credits: {
			    enabled: false
			},
			title: {
			    text: 'JUMLAH PENGADUAN BERDASARKAN PROGRAM PENGADUAN<br>TAHUN <?=($page_tahun=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $page_tahun)?>'
			},
			tooltip: {
				pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions: {
			    pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
				    enabled: false
				},
				showInLegend: true
			    }
			},
			series: [{
			    type: 'pie',
			    name: 'Program Pengaduan',
			    data: data
			}]
		    });
		});
		</script>
		<?php endif;?>
		<!--./report program-->
		
		<!--report kategori status-->
		<?php if($page_report=='report_kategori_status'):
		
		    $selesai = $proses = $pending = $kat_pengaduan = array();
		    foreach($report as $row){
			$pending[]=$row->pending;
			$proses[]=$row->proses;
			$selesai[]=$row->selesai;
			
			$kat_pengaduan[]="'".$row->kategori."'";
		    }
		?>
		<script>
		$(document).ready(function(){
		    var pending = [<?=implode(",",$pending)?>];
		    var proses = [<?=implode(",",$proses)?>];
		    var selesai = [<?=implode(",",$selesai)?>];
		    var ticks = [<?=implode(",",$kat_pengaduan)?>];
		    
		    $('#show_chart').highcharts({
			chart: {
			    type: 'column'
			},
			credits: {
			    enabled: false
			},
			title: {
			    text: 'JUMLAH PENGADUAN BERDASARKAN KATEGORI DAN STATUS PENGADUAN<br>TAHUN <?=($page_tahun=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $page_tahun)?>'
			},
			subtitle: {
			    text: ''
			},
			xAxis: {
			    categories: ticks
			},
			yAxis: {
			    min: 0,
			    title: {
				text: 'Kategori dan Status'
			    }
			},
			tooltip: {
			    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
				'<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
			    footerFormat: '</table>',
			    shared: true,
			    useHTML: true
			},
			plotOptions: {
			    column: {
				pointPadding: 0.2,
				borderWidth: 0
			    }
			},
			series: [{
			    name: 'Pending',
			    data: pending
		
			}, {
			    name: 'Proses',
			    data: proses
		
			}, {
			    name: 'selesai',
			    data: selesai
		
			}]
		    });
		});
		</script>
		<?php endif;?>
		<!--./report kategori status-->
		
		<!--report media-->
		<?php if($page_report=='report_media'):
		
		    $data = $kat_pengaduan = array();
		    foreach($report as $row){
			$data[]="['".$row->media."'".','.$row->total.']';
			$kat_pengaduan[]="'".$row->media."'";
		    }
		?>
		<script>
		$(document).ready(function(){
		    var data = [<?=implode(",",$data)?>];
		      
		    // Build the chart
		    $('#show_chart').highcharts({
			chart: {
			    plotBackgroundColor: null,
			    plotBorderWidth: null,
			    plotShadow: false
			},
			credits: {
			    enabled: false
			},
			title: {
			    text: 'JUMLAH PENGADUAN BERDASARKAN MEDIA PENGADUAN<br>TAHUN <?=($page_tahun=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $page_tahun)?>'
			},
			tooltip: {
				pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions: {
			    pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
				    enabled: false
				},
				showInLegend: true
			    }
			},
			series: [{
			    type: 'pie',
			    name: 'Media Pengaduan',
			    data: data
			}]
		    });
		});
		</script>
		<?php endif;?>
		<!--./report media-->
		
		<!--report program status-->
		<?php if($page_report=='report_program_status'):
		
		    $selesai = $proses = $pending = $kat_pengaduan = array();
		    foreach($report as $row){
			$pending[]=$row->pending;
			$proses[]=$row->proses;
			$selesai[]=$row->selesai;
			
			$kat_pengaduan[]="'".$row->program."'";
		    }
		?>
		<script>
		$(document).ready(function(){
		    var pending = [<?=implode(",",$pending)?>];
		    var proses = [<?=implode(",",$proses)?>];
		    var selesai = [<?=implode(",",$selesai)?>];
		    var ticks = [<?=implode(",",$kat_pengaduan)?>];
		    
		    $('#show_chart').highcharts({
			chart: {
			    type: 'column'
			},
			credits: {
			    enabled: false
			},
			title: {
			    text: 'JUMLAH PENGADUAN BERDASARKAN PROGRAM DAN STATUS PENGADUAN<br>TAHUN <?=($page_tahun=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $page_tahun)?>'
			},
			subtitle: {
			    text: ''
			},
			xAxis: {
			    categories: ticks
			},
			yAxis: {
			    min: 0,
			    title: {
				text: 'Program dan Status'
			    }
			},
			tooltip: {
			    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
				'<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
			    footerFormat: '</table>',
			    shared: true,
			    useHTML: true
			},
			plotOptions: {
			    column: {
				pointPadding: 0.2,
				borderWidth: 0
			    }
			},
			series: [{
			    name: 'Pending',
			    data: pending
		
			}, {
			    name: 'Proses',
			    data: proses
		
			}, {
			    name: 'selesai',
			    data: selesai
		
			}]
		    });
		});
		</script>
		<?php endif;?>
		<!--./report program status-->
		
		<!--report pelaku status-->
		<?php if($page_report=='report_pelaku_status'):
		
		    $selesai = $proses = $pending = $kat_pengaduan = array();
		    foreach($report as $row){
			$pending[]=$row->pending;
			$proses[]=$row->proses;
			$selesai[]=$row->selesai;
			
			$kat_pengaduan[]="'".$row->pelaku."'";
		    }
		?>
		<script>
		$(document).ready(function(){
		    var pending = [<?=implode(",",$pending)?>];
		    var proses = [<?=implode(",",$proses)?>];
		    var selesai = [<?=implode(",",$selesai)?>];
		    var ticks = [<?=implode(",",$kat_pengaduan)?>];
		    
		    
		    $('#show_chart').highcharts({
			chart: {
			    type: 'column'
			},
			credits: {
			    enabled: false
			},
			title: {
			    text: 'JUMLAH PENGADUAN BERDASARKAN PELAKU DAN STATUS PENGADUAN<br>TAHUN <?=($page_tahun=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $page_tahun)?>'
			},
			subtitle: {
			    text: ''
			},
			xAxis: {
			    categories: ticks
			},
			yAxis: {
			    min: 0,
			    title: {
				text: 'Pelaku dan Status'
			    }
			},
			tooltip: {
			    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
				'<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
			    footerFormat: '</table>',
			    shared: true,
			    useHTML: true
			},
			plotOptions: {
			    column: {
				pointPadding: 0.2,
				borderWidth: 0
			    }
			},
			series: [{
			    name: 'Pending',
			    data: pending
		
			}, {
			    name: 'Proses',
			    data: proses
		
			}, {
			    name: 'selesai',
			    data: selesai
		
			}]
		    });
		});
		</script>
		<?php endif;?>
		<!--./report pelaku status-->
		
		<!--report wilayah-->
		<?php if($page_report=='report_wilayah_status'):
		
		    $selesai = $proses = $pending = $kat_pengaduan = array();
		    foreach($report as $row){
			$pending[]=$row->pending;
			$proses[]=$row->proses;
			$selesai[]=$row->selesai;
			
			$kat_pengaduan[]="'".$row->wilayah."'";
		    }
		?>
		<script>
		$(document).ready(function(){
		    var pending = [<?=implode(",",$pending)?>];
		    var proses = [<?=implode(",",$proses)?>];
		    var selesai = [<?=implode(",",$selesai)?>];
		    var ticks = [<?=implode(",",$kat_pengaduan)?>];
		    
		    <?php /*
		    plot2 = $.jqplot('show_chart', [pending, proses, selesai], {
			animate: !$.jqplot.use_excanvas,
			seriesDefaults: {
				renderer:$.jqplot.BarRenderer,
			pointLabels: { show: true, location: 'e', edgeTolerance: -15 },
			shadowAngle: 135,
				rendererOptions: {
			    barDirection: 'horizontal'
			}
			},
			axes: {
				
				yaxis: {
					renderer: $.jqplot.CategoryAxisRenderer,
					ticks: ticks,
					
				},
				xaxis: {
					min:0,
					tickInterval: 10,
					tickOptions: { formatString:'%d' }
				},
			},
			legend: {
				show: true,
				location: 'ne',
				placement: 'inside'
			},
			series:[
			    {label:'Pending'},
			    {label:'Proses'},
			    {label:'Selesai'}
			],
			title:{
			    text: 'JUMLAH PENGADUAN BERDASARKAN PELAKU DAN STATUS WILAYAH<br>TAHUN <?=($page_tahun=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $page_tahun)?>'
		      }
		    });
		    */
		    ?>
		    $('#show_chart').highcharts({
			chart: {
			    type: 'bar'
			},
			title: {
			    text: 'JUMLAH PENGADUAN BERDASARKAN PELAKU DAN STATUS WILAYAH<br>TAHUN <?=($page_tahun=='all' ? (date('Y')==2014 ? '2014' : '2011 s/d '.date('Y')) : $page_tahun)?>'
			},
			subtitle: {
			    text: ''
			},
			xAxis: {
			    categories: ticks,
			    title: {
				text: null
			    }
			},
			yAxis: {
			    min: 0,
			    title: {
				text: 'Pengaduan per Wilayah',
				align: 'high'
			    },
			    labels: {
				overflow: 'justify'
			    }
			},
			tooltip: {
			    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
				'<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
			    footerFormat: '</table>',
			    shared: true,
			    useHTML: true
			},
			plotOptions: {
			    bar: {
				dataLabels: {
				    enabled: true
				}
			    }
			},
			
			credits: {
			    enabled: false
			},
			series: [{
			    name: 'Pending',
			    data: pending
			}, {
			    name: 'Proses',
			    data: proses
			}, {
			    name: 'Selesai',
			    data: selesai
			}]
		    });
		});
		</script>
		<?php endif;?>
		<!--./report wilayah-->
		<!--end report-->
    <?php endif;?>
    <!--./statistik pengaduan-->
	      
    
