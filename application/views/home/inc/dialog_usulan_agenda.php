			  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
				    <form method='post' action='<?=site_url('home/home/post_usulan_agenda')?>' enctype="multipart/form-data">
				    <div class="modal-content">
					<div class="modal-header">
					    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					    <h4 class="modal-title" id="myModalLabel">Pengajuan Agenda</h4>
					</div> <!--./modal-header-->
				      <div class="modal-body">
					      <div class="alert alert-warning alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						  Bila anda ingin mengundang tim kurikulum untuk menghadiri kegiatan anda, silahkan isian dibawah ini.
					      </div>
					      <div class="form-group">
						  <label for="nama">Nama</label>
						  <input type="text" class="form-control" id="nama" name='agenda[nama]' placeholder="Nama" required>
					      </div>
					      <div class="form-group">
						  <label for="nama">Lembaga</label>
						  <input type="text" class="form-control" id="lembaga" name='agenda[lembaga]' placeholder="Lembaga" required>
					      </div>
					      <div class="form-group">
						  <label for="nama">Alamat</label>
						  <textarea class="form-control" rows="3" placeholder="Alamat" name='agenda[alamat]' required></textarea>
					      </div>
					      <div class="form-group">
						  <label for="nama">No HP</label>
						  <input type="text" class="form-control" id="no_hp" name='agenda[no_hp]' placeholder="No HP" required>
					      </div>
					      <div class="form-group">
						  <label for="nama">Email</label>
						  <input type="email" class="form-control" id="email" name='agenda[email]' placeholder="Email" required>
					      </div>
					      <div class="form-group">
						  <label for="nama">Tema Materi</label>
						  <input type="text" class="form-control" id="tema" name='agenda[tema_materi]' placeholder="Tema Materi" required>
					      </div>
					      <div class="form-group">
						  <label for="nama">Deskripsi Kegiatan</label>
						  <textarea class="form-control" rows="3" placeholder='Deskripsi Kegiatan' name='agenda[deskripsi_kegiatan]' required></textarea>
					      </div>
					      
					      <div class="form-group">
						  <label for="">Usulan Tanggal</label>
						  <div class="input-group date">
						      <input type="text" class="form-control" placeholder='Usulan Tanggal' name='agenda[usulan_tanggal]' value='<?=date("d/m/Y H:i",time());?>' required> <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
						  </div>
					      </div>
					      
					      <div class="form-group">
						  <label for="nama">Masukan Proposal Anda Dalam Format (doc,docx,xls,xlsx,ppt,pptx,pdf,zip,rar,jpg,png)</label>
						  <input type="file" id="file" name='file'>
						  
					      </div>
					      
					      <div class="form-group">
						  <label>Verifikasi</label>
						  
						      <div id="RecaptchaField3"></div>
						  
					      </div>
					  
				      </div>
				      <div class="modal-footer">
					  <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
					  <input type="submit" value="Kirim" class="btn btn-primary">
				      </div> <!--./modal-footer-->
				      </form>
				    </div> <!--./modal-content-->
				</div><!--./modal-dialog-->
			  </div>