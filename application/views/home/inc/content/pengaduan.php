        
    <!-- Being Page Title -->
	<style>
.text-center{
	text-align: center;
}

.image-container{
	text-align: center;
}

.background-white{
	background-color:#ffffff;
}

.box-container{
	width:340px;
	overflow:hidden;
}

.text-description-container{
	padding-top:10px;
	padding-bottom:10px;
    padding-right: 17%;
    padding-left: 17%;
}


</style>
<div class="row">
        
	<div class="row">
		<div class="row">
	    <!-- Here begin Main Content -->
	    
	    
	    <div class="col-md-6">
		
		<div class="row">
		    <div class="col-md-12">
			<div style='padding:100px'><h4 style='background:#41a5d6; color:black; padding:5px'>Jika Anda menemukan masalah pada pelaksanaan Kurikulum 2013, jangan ragu untuk menghubungi kami. Laporan Anda, akan kami terima dan tidak lanjuti. Mari bekerja sama dalam mengawasi jalannya Kurikulum 2013.</h4> </div>
		    </div>
		</div>
	    </div>
	    
	    <div class="col-md-6" style='background:white;padding-left: 0px; padding-right: 0px;'>
		
		<div class="row" style='background:white;'>
				<div style='background:white; padding:100px;'>
				<div style='border-style: solid; border-width: 2px;'>
				<p><strong>Biro Komunikasi dan Layanan Masyarakat (BKLM) Kemdikbud Gedung C Kemdikbud Lt 4 Jl Jenderal Sudirman Senayan Jakarta, 10270</strong>
				<ul class="list-links">
				    <li><i class="fa fa-phone"></i> Call center : <a href="tel:0215725980">021 5725980</a></li>
				    <li><i class="fa fa-phone"></i> Call center : <a href="tel:177">177</a></li>
				    <li><i class="fa fa-phone"></i> Telepon alternatif : <a href="tel:0215703303">021 5703303</a> / <a href="tel:5711144">5711144</a> ext. 2115</li>
				    <li><i class="fa fa-envelope"></i> SMS : 0811976929</li>
				    <li><i class="fa fa-envelope"></i> Email : <a href="mailto:pengaduan@kemdikbud.go.id">pengaduan[at]kemdikbud.go.id</a></li>
				</ul>
				</p>
				</div>
				</div>
				
		</div>
	    </div>
	</div>
	</div>
	
	
	
</div>
    
	
	
        
    

    <!-- Being Page Title -->
    
        