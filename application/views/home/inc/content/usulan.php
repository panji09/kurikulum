    
    <!-- Being Page Title -->
	<style>
.text-center{
	text-align: center;
}

.image-container{
	text-align: center;
}

.background-white{
	background-color:#ffffff;
}

.box-container{
	width:340px;
	overflow:hidden;
}

.text-description-container{
	padding-top:10px;
	padding-bottom:10px;
    padding-right: 17%;
    padding-left: 17%;
}


</style>
<div class="row">
        <div class="row">
	    <!-- Here begin Main Content -->
		<div class="col-md-12">
			<div class="text-center">
			
			<?php
			    $query = $this->static_pages_db->get('masukan');
			    $content = '';
			    if($query){
				$content = $query[0]['content'];
			    }
			?>
			<?=($content ? $content : '')?>
		</div>
	</div>
	
	<div class="row background-white">
		<div class="row">
	    <!-- Here begin Main Content -->
	    <?php
		$notif=$this->session->flashdata('success');
		if($notif):
	    ?>
	      <div class="alert <?=($notif['status'] ? 'alert-success' : 'alert-danger')?> alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		  <p class="text-center"><?=$notif['msg']?></p>
	      </div>
	    <?php endif;?>
	    
	    <div class="col-md-1">
		
		<div class="row">
		    <div class="col-md-12">
			
		    </div>
		</div>
	    </div>
	    <div class="col-md-5">
		
		<div class="row">
		    <div class="col-md-12">
			<div id="blog-comments" class="blog-post-container">
			    <div class="blog-post-inner">
				
				<form style='background:#e4e4e4; padding:20px;  border-style: solid; border-width: 2px;' id='pengaduan_form' role='form' class='form-pengaduan' method='post' enctype="multipart/form-data" action='<?=site_url('home/halaman/post_usulan')?>'>
				    <?php $this->load->view('home/inc/form_usulan');?>
				    <div class='row'>
					<div class="col-md-3 col-md-offset-9">
					    <input type='submit' class='btn btn-primary btn-lg btn-block' value='Kirim'>
					</div>
					
				    </div>
				</form>
			    </div>
			</div>
		    </div>
		</div>
	    </div>
	    <div class="col-md-1">
		
		<div class="row">
		    <div class="col-md-12">
			
		    </div>
		</div>
	    </div>
	    
	    <div class="col-md-4" style='background:#41a5d6;padding-left: 0px; padding-right: 0px; margin-top: 50px;'>
		
		<div class="row" style='background:#41a5d6; margin-top:50px; height:800px;'>
				<div style='background:white;'>
				<p><strong><h4>YUK DISKUSI!</h4></strong></p>
				
				</div>
				<div style='padding-left:20px; padding-bottom:20px;'>
				<br><br>
				<img src='<?=$this->config->item('home_img').'/masukan.jpg'?>'>
				<br>
				<p><h4 style='color:white;'>Selain Anda bisa memberikan masukan melalui online, kami mengundang Anda untuk hadir pada diskusi terbuka mengenai kurikulum. Untuk melihat jadwal terdekat, <br><a style='color:black;' href='#'>Klik Disini</a></h4></p><br>
				
				<a href=# class='btn btn-default' data-toggle="modal" data-target="#myModal_kehadiran">YA, SAYA AKAN DATANG</a><br><br>
				<?php $this->load->view('home/inc/dialog_kehadiran')?>
				
				<a href=# class='btn btn-default' data-toggle="modal" data-target="#myModal">SAYA INGIN MENGUNDANG DISKUSI DITEMPAT SAYA</a>
				<?php $this->load->view('home/inc/dialog_usulan_agenda')?>
				</div>
				
		</div>
	    </div>

	</div>
	</div>
	
	
	
</div>
    
	
	
        
    
