		    <?php if($halaman_main->num_rows()) foreach($halaman_main->result() as $row): $content = str_get_html($row->content);?>
		    <?php  if($row->published):?>
                    <div class="col-md-6 col-sm-6 content_halaman" id='<?=$row->id?>'>
                        
                        
			    <div class="list-event-item">
				<div class="box-content-inner clearfix">
				    
				    <?php if($halaman != 'agenda'):?>
					<?php
					    $img = null;
					    foreach($content->find('img') as $element){ 
						$img = $element->src; break;
					    }
					?>
					<?php if($img):?>
					<div class="list-event-thumb">
					    <a href="<?=site_url($halaman.'/'.$row->id)?>">
						<img src="<?=$img?>" alt="">
					    </a>
					</div>
					<?php endif;?>
				    <?php else: //$halaman != 'agenda'?>
					<?php if($row->agenda_from != '0000-00-00 00:00:00'):?>
					  <div class="calendar-small">
					      <?php
						  $date = $row->agenda_from;
						  
						  $month = substr($date,5,2);
						  $day = substr($date,8,2);
					      ?>
					      <span class="s-month"><?=month_indonesia(intval($month))?></span>
					      <span class="s-date"><?=$day?></span>
					  </div>
					  <?php endif;?>
				    <?php endif; //$halaman != 'agenda'?>
				    
				    <h5 class="event-title"><a href="<?=site_url($halaman.'/'.$row->id)?>"><?=$row->title?></a></h5>
				    
				    <?php if($halaman == 'agenda'):?>
					
					<i class="fa fa-calendar-o"></i> Mulai : <?=mysqldatetime_to_date($row->agenda_from, "d/m/Y H:i")?><br>      
					<?php if(isset($row->agenda_to) && $row->agenda_to != '0000-00-00 00:00:00'):?>
					<i class="fa fa-calendar-o"></i> Sampai : <?=mysqldatetime_to_date($row->agenda_to, "d/m/Y H:i")?>
					<?php endif;?>
				      
				    <?php endif;?>
				    <p>
					<?php
					    $content_text = $content->plaintext;
					    
					    $arr1 = str_split($content_text);
					    
					    $pos = null; $found = 0;
					    foreach($arr1 as $index=>$value){
						if($value == '.'){
						    if($found < 2){
							$pos = $index;
							$found++;
						    }
						    
						}
					    }
					    //echo $content_text;
					    $content_first = substr($content_text, 0, $pos + 1);
					    echo ($pos ? $content_first : $content_text);
					    //echo ($content_first ? $content_first : $content_text);
					?>
				    </p>
				    
				    <?php if(in_array($halaman, array('agenda','berita'))):?>
					<p class="event-list-meta small-text"> <span> Tags : <?=$row->tags?></span> - <span><abbr class="timeago" title="<?=$row->last_update?>"><?=$row->last_update?></abbr></span></p>
				    <?php endif;?>
				    
				    <?php if(in_array($halaman, array('usulan_buku','usulan_standar_pendidikan','usulan_kerangka_dasar_dan_struktur'))):?>
					
					<?php 
					    $category = (isset($row->category) && $row->category ? explode(',',$row->category) : array());	
				    
					    if($halaman == 'usulan_buku'): // if referensi buku 6
					    
						$kategori_referensi_buku = $this->select_db->kategori_referensi_buku();
						foreach($kategori_referensi_buku->result() as $row1):
						    if($category[0] == 'null'){
							$cat_name[] = 'null'; break;
						    }elseif($category[0] == $row1->id)
							$cat_name[] = $row1->name;
						endforeach;
						
						$jenjang = $this->select_db->jenjang(array('in_id'=> array(1,2,3)));
						foreach($jenjang->result() as $row1):
						    if($category[1] == 'null'){
							$cat_name[] = 'null'; break;
						    }elseif($category[0] == $row1->id)
							$cat_name[] = $row1->desc_name;
						endforeach;
						
						foreach($this->select_db->kategori_mata_pelajaran(array('jenjang_id' => $category[1]))->result() as $row1):
						    if($category[2] == 'null'){
							$cat_name[] = 'null'; break;
						    }elseif($category[0] == $row1->id)
							$cat_name[] = $row1->name;
						endforeach;
					    endif;
					    
					    if($halaman == 'usulan_standar_pendidikan'): // if referensi buku 6
					    
						$kategori_standar_pendidikan = $this->select_db->kategori_standar_pendidikan();
						foreach($kategori_standar_pendidikan->result() as $row1):
						    if($category[0] == 'null'){
							$cat_name[] = 'null'; break;
						    }elseif($category[0] == $row1->id)
							$cat_name[] = $row1->name;
						endforeach;
						
						
					    endif;
					    
					    if($halaman == 'usulan_kerangka_dasar_dan_struktur'): // if referensi buku 6
					    
						$jenjang = $this->select_db->jenjang(array('in_id'=> array(1,2,3)));
						foreach($jenjang->result() as $row1):
						    if($category[0] == 'null'){
							$cat_name[] = 'null'; break;
						    }elseif($category[0] == $row1->id)
							$cat_name[] = $row1->desc_name;
						endforeach;
						
						foreach($this->select_db->kategori_mata_pelajaran(array('jenjang_id' => $category[0]))->result() as $row1):
						    if($category[1] == 'null'){
							$cat_name[] = 'null'; break;
						    }elseif($category[1] == $row1->id)
							$cat_name[] = $row1->name;
						endforeach;
					    endif;
					    ?>
					    
					    <?php foreach($cat_name as $row1):
						$category_name[] = $row1;
					    endforeach;?>
					<p class="event-list-meta small-text"> <span> Kategori : <?=implode(',',$category_name)?></span> - <span><abbr class="timeago" title="<?=$row->last_update?>"><?=$row->last_update?></abbr></span></p>
				    <?php endif;?>
				    
				</div> <!-- /.box-content-inner -->
			    </div> <!-- /.list-event-item -->
			    
			    
                        
                        
                    </div> <!-- /.col-md-12 -->
		    <?php endif;?>
		    <?php endforeach;?>