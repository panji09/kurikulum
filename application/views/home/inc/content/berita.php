    
        
    <!-- Being Page Title -->
	<style>
.text-center{
	text-align: center;
}

.image-container{
	text-align: center;
}

.background-white{
	background-color:black;
}

.box-container{
	width:340px;
	overflow:hidden;
}

.text-description-container{
	padding-top:10px;
	padding-bottom:10px;
    padding-right: 17%;
    padding-left: 17%;
}


</style>
<div class="row">
        <div class="row">
	    <!-- Here begin Main Content -->
		<div class="col-md-12">
			<div class="text-center">
			<?php
			    $query = $this->static_pages_db->get('perkembangan_kurikulum');
			    $content = '';
			    if($query){
				$content = $query[0]['content'];
			    }
			?>
			<?=($content ? $content : '')?>
		</div>
	</div>
	
	<div class="row background-white">
	    <div class="row">
	    <!-- Here begin Main Content -->
	    
	    
		<div class="col-md-3" style='background:#41a5d6;padding-left: 0px; padding-right: 0px;'>
		    
		    
		</div>
	    
		<div class="col-md-6" style='background:black; padding-left: 0px; padding-right: 0px;'>
		    
		    <div class="row content_halaman" style='background:black; margin-top:50px; margin-left:50px; min-height:300px; color:white;'>
				    <?php if($this->uri->segment(2)):?>
					<?php 
					    $content_id = $this->uri->segment(2);
					    $query = $this->dynamic_pages_db->get($content_id);
					    
					    if($query):
						$row = $query[0];
					?>
					<h4 style='color:white'><u><b><?=$row['title']?></b></u></h4>
					
					<div>
					    <a href='<?=site_url('berita/'.$row['_id']->{'$id'})?>' style='color:white'><?=date('d-m-Y',$row['created'])?></a> <br>
					    <a href='<?=site_url('berita/'.$row['_id']->{'$id'})?>' style='color:white'><?=$row['title']?></a>
					    <p><?=$row['content']?></p>
					</div>
					    <?php endif;?>
				    <?php else:?>
					<h4 style='color:white'><u><b>BERITA LAINNYA</b></u></h4>
					
					<?php foreach($this->dynamic_pages_db->get_all(array('page_id' => 'berita', 'published' => "1"), $limit=4) as $row):?>
					<div>
					    <a href='<?=site_url('berita/'.$row['_id']->{'$id'})?>' style='color:white'><?=date('d-m-Y',$row['created'])?></a> <br>
					    <a href='<?=site_url('berita/'.$row['_id']->{'$id'})?>' style='color:white'><?=$row['title']?></a>
					</div>
				    
				    
				    <?php endforeach;?>
				    <br>
				    <a href='<?=site_url('berita')?>' class='btn btn-default'>load more</a>
				    <?php endif;?>
				    
				    
		    </div>
		</div>
		
		<div class="col-md-3" style='background:#41a5d6;padding-left: 0px; padding-right: 0px;'>
		    
		   
		</div>
	    
		
	    </div>
	</div>
	
	
	
</div>
    
	
	
  