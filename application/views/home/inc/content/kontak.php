 
    <!-- Being Page Title -->
    <div class="container">
        <div class="page-title clearfix">
            <div class="row">
                <div class="col-md-12">
                    <h6><a href="<?=site_url('')?>">Home</a></h6>
                    <h6><span class="page-active">Kontak</span></h6>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">

            <div class="col-md-5">
                <div class="contact-map">
                    <div class="google-map-canvas" id="map-canvas" style="height: 542px;">
                    </div>
                </div>
            </div> <!-- /.col-md-5 -->
            
            <div class="col-md-7">
                <div class="contact-page-content">
		    <?php
			$notif=$this->session->flashdata('success');
			if($notif):
		    ?>
		      <div class="alert <?=($notif['status'] ? 'alert-success' : 'alert-danger')?> alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			  <?=$notif['msg']?>
		      </div>
		    <?php endif;?>
                    <div class="contact-heading">
                        <h3>Kontak</h3>
                        <p>Masukan dan saran tentang sistem kami ini</p>
                    </div>
                    <form class="form-horizontal" role="form" method='post' action='<?=site_url('kontak/post_kontak')?>'>
		    <div class="form-group">
			<label class="col-sm-2 control-label">Nama</label>
			<div class="col-sm-10">
			    <input type="text" class="form-control" id="nama" name='nama' placeholder="Nama">
			</div>
		    </div>
		    <div class="form-group">
			<label class="col-sm-2 control-label">Email</label>
			<div class="col-sm-10">
			    <input type="email" class="form-control" id="email" name='email' placeholder="Email">
			</div>
		    </div>
		    <div class="form-group">
			<label class="col-sm-2 control-label">Pesan</label>
			<div class="col-sm-10">
			    <textarea name='pesan' placeholder='pesan' class="form-control" rows="3"></textarea>
			</div>
		    </div>
		    <div class="form-group">
			<label class="col-sm-2 control-label">Verifikasi</label>
			<div class="col-sm-10">
			    <?php
				$publickey = $this->config->item('public_key');; // you got this from the signup page
				echo recaptcha_get_html($publickey);
			    ?>
			</div>
		    </div>
		    <div class="form-group">
		      <div class="col-sm-offset-2 col-sm-10">
			<input class="mainBtn" type="submit" name="" value="Kirim">
		      </div>
		    </div>
		  </form>
                </div>
            </div> <!-- /.col-md-7 -->

        </div> <!-- /.row -->
    </div> <!-- /.container -->