    <?php
	$halaman = $this->uri->segment(1);
	
    ?>
    <!-- Being Page Title -->
    <div class="container">
        <div class="page-title clearfix">
            <div class="row">
                <div class="col-md-12">
		    <?php
			$list_halaman = array(
			    'berita' => 'Berita',
			    'agenda' => 'Agenda',
			    'usulan_buku' => 'Usulan Buku',
			    'usulan_standar_pendidikan' => 'Usulan Standar Pendidikan',
			    'usulan_kerangka_dasar_dan_struktur' => 'Usulan Kerangka Dasar dan Struktur'
			);
		    ?>
                    <h6><a href="<?=site_url('')?>">Home</a></h6>
                    <?=(in_array($halaman, array('usulan_buku', 'usulan_standar_pendidikan', 'usulan_kerangka_dasar_dan_struktur')) ? '<h6><a href="#">Usulan</a></h6>' : '')?>
                    <h6><a href="<?=site_url($halaman)?>"><?=$list_halaman[$halaman]?></a></h6>
                    <h6><span class="page-active"><?=$halaman_main->title?></span></h6>
                    
                    
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">

            <!-- Here begin Main Content -->
            <div class="col-md-8">

                <div class="row">
                    <div class="col-md-12">
                        <div class="blog-post-container">
                            <?php
				$notif=$this->session->flashdata('success');
				if($notif):
			    ?>
			      <div class="alert <?=($notif['status'] ? 'alert-success' : 'alert-danger')?> alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				  <?=$notif['msg']?>
			      </div>
			    <?php endif;?>
                            <div class="blog-post-inner">
                                <h3 class="blog-post-title"><?=$halaman_main->title?></h3>
                                <?=$halaman_main->content?>
                                
                                <?php if(isset($halaman_main->tags) && $halaman_main->tags):?>
                                <div class="tag-items">
                                    <span class="small-text">Tags:</span>
                                    <?php
					$tags = explode(',', $halaman_main->tags);
					foreach($tags as $tag):
                                    ?>
					<a href="#" rel="tag"><?=$tag?></a>
                                    
                                    <?php endforeach;?>
                                    
                                </div>
                                <?php endif;?>
                                
                                <?php 
				    $category = (isset($halaman_main->category) && $halaman_main->category ? explode(',',$halaman_main->category) : array());	
				    if($category):?>
				    <div class="tag-items">
					<span class="small-text">Kategori:</span>
					    
					    <?php 
					    if($halaman_main->content_kategori_id == 7): // if referensi buku 6
					    
						$kategori_referensi_buku = $this->select_db->kategori_referensi_buku();
						foreach($kategori_referensi_buku->result() as $row):
						    if($category[0] == 'null'){
							$cat_name[] = 'null'; break;
						    }elseif($category[0] == $row->id)
							$cat_name[] = $row->name;
						endforeach;
						
						$jenjang = $this->select_db->jenjang(array('in_id'=> array(1,2,3)));
						foreach($jenjang->result() as $row):
						    if($category[1] == 'null'){
							$cat_name[] = 'null'; break;
						    }elseif($category[0] == $row->id)
							$cat_name[] = $row->desc_name;
						endforeach;
						
						foreach($this->select_db->kategori_mata_pelajaran(array('jenjang_id' => $category[1]))->result() as $row):
						    if($category[2] == 'null'){
							$cat_name[] = 'null'; break;
						    }elseif($category[0] == $row->id)
							$cat_name[] = $row->name;
						endforeach;
					    endif;
					    
					    if($halaman_main->content_kategori_id == 8): // if referensi buku 6
					    
						$kategori_standar_pendidikan = $this->select_db->kategori_standar_pendidikan();
						foreach($kategori_standar_pendidikan->result() as $row):
						    if($category[0] == 'null'){
							$cat_name[] = 'null'; break;
						    }elseif($category[0] == $row->id)
							$cat_name[] = $row->name;
						endforeach;
						
						
					    endif;
					    
					    if($halaman_main->content_kategori_id == 9): // if referensi buku 6
					    
						$jenjang = $this->select_db->jenjang(array('in_id'=> array(1,2,3)));
						foreach($jenjang->result() as $row):
						    if($category[0] == 'null'){
							$cat_name[] = 'null'; break;
						    }elseif($category[0] == $row->id)
							$cat_name[] = $row->desc_name;
						endforeach;
						
						foreach($this->select_db->kategori_mata_pelajaran(array('jenjang_id' => $category[0]))->result() as $row):
						    if($category[1] == 'null'){
							$cat_name[] = 'null'; break;
						    }elseif($category[1] == $row->id)
							$cat_name[] = $row->name;
						endforeach;
					    endif;
					    ?>
					    
					    <?php foreach($cat_name as $row1):?>
						<a href="#" rel="tag"><?=$row1?></a>
					
					    <?php endforeach;?>
					    
					    
					
				    </div>
				    <?php endif;?>
				  
                                <div id="sharethis">
				    <span class='st_facebook_vcount' displayText='Facebook'></span>
				    <span class='st_twitter_vcount' displayText='Tweet'></span>
				    <span class='st_googleplus_vcount' displayText='Google +'></span>
				    <span class='st_email_vcount' displayText='Email'></span>
				</div>
                            </div>
                        </div> <!-- /.blog-post-container -->
                    </div> <!-- /.col-md-12 -->
                    
                    <?php if(in_array($halaman, array('usulan_buku','usulan_standar_pendidikan','usulan_kerangka_dasar_dan_struktur'))):?>
                    <div class="col-md-12">
                        <div class="blog-post-container">
                            <div class="blog-post-inner">
				<button type="button" class="btn btn-primary btn-lg btn-block" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false">
				    Ajukan Usulan / Saran
				</button>
				<div class="collapse" id="collapseExample">
				      <form id='pengaduan_form' role='form' class='form-pengaduan' method='post' enctype="multipart/form-data" action='<?=site_url('home/halaman/post_usulan')?>'>
					  <?php $this->load->view('home/inc/form_usulan');?>
					  <div class='row'>
					      <div class="col-md-3 col-md-offset-9">
						  <input type='submit' class='btn btn-primary btn-lg btn-block' value='Kirim'>
					      </div>
					      
					  </div>
				      </form>
				</div>
				
                            </div>
			</div>
		    </div>
		    <?php endif;?>
                </div> <!-- /.row -->
            </div> <!-- /.col-md-8 -->
            <!--./main content-->
            
           
            <!-- Here begin Sidebar -->
            <div class="col-md-4">
		
		<?php 
		    $param = array(
			'halaman' => $halaman
		    );
		    $this->load->view('home/inc/sidebar_widget',$param);
		?>

            </div> <!-- /.col-md-4 -->
    
        </div> <!-- /.row -->
    </div> <!-- /.container --> 
