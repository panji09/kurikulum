    <!-- Being Page Title -->
    <div class="container">
        <div class="page-title clearfix">
            <div class="row">
                <div class="col-md-12">
                    <h6><a href="<?=site_url('')?>">Home</a></h6>
                    <h6><span class="page-active">Statistik Pengaduan</span></h6>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container">
        <div class="row">

            <!-- Here begin Main Content -->
            <div class="col-md-8">

                <div class="row">
                    <div class="col-md-12">
                        
                        <div class="course-post">
                            <div class="course-details clearfix">
                                <?php
				    $page_tahun = $this->uri->segment(4);
				    $page_provinsi = $this->uri->segment(5);
				    $page_kabkota = $this->uri->segment(6);
				    $filter = array(
					'tahun' => $page_tahun,
					'provinsi' => $page_provinsi,
					'kabkota' => $page_kabkota
				    );
				    /*
				    $role_level = $this->session->userdata('role_level');
				    
				    if($role_level->level_id == 1)
					$filter['provinsi'] = $role_level->provinsi_id;
				    elseif($role_level->level_id == 2)
					$filter['kabkota'] = $role_level->kabkota_id;
				    */
				    $page_report = $this->uri->segment(3);
				    if($page_report == 'report_kategori')
					$report = $this->report_db->kategori($filter)->result();
				    elseif($page_report == 'report_sumber_info')
					$report = $this->report_db->sumber_info($filter)->result();
				    elseif($page_report == 'report_status')
					$report = $this->report_db->status($filter)->result();
				    elseif($page_report == 'report_program')
					$report = $this->report_db->program($filter)->result();
				    elseif($page_report == 'report_kategori_status')
					$report = $this->report_db->kategori_status($filter)->result();
				    elseif($page_report == 'report_media')
					$report = $this->report_db->media($filter)->result();
				    elseif($page_report == 'report_program_status')
					$report = $this->report_db->program_status($filter)->result();
				    elseif($page_report == 'report_pelaku_status')
					$report = $this->report_db->pelaku_status($filter)->result();
				    elseif($page_report == 'report_wilayah_status'){
					
					if($page_provinsi == 'all'){
					    $level = 'nasional';
					    $height = $this->region_db->provinsi()->num_rows() * 60;
					}
					
					if($page_provinsi != 'all'){
					    $level = 'provinsi';
					    $height = $this->region_db->kabkota(array('provinsi_id' => $page_provinsi))->num_rows() * 60;
					}
					
					if($page_kabkota != 'all'){
					    $level = 'kabkota';
					    $height = $this->region_db->kecamatan(array('kabkota_id' => $page_kabkota))->num_rows() * 60;
					}
					$report = $this->report_db->wilayah_status($filter, $level)->result();

				    }
				    
				    $chart = ($this->uri->segment(2) ? $page_report : 'report_kategori');
				?>
				    
				    <?php if($page_report == 'report_wilayah_status'):?>
					<div id="show_chart" align="center" style="height:<?=$height?>px; max-width:1000px"><!--Chart Di Load disini--></div>
				    <?php else:?>
					<div id="show_chart" align="center" style="height:'450px"><!--Chart Di Load disini--></div>
				    <?php endif;?>
				    <?=$this->load->view('handling/modules/chart/'.$chart,array('report' =>$report));?>
				    
				    <div id="sharethis">
					    <span class='st_facebook_vcount' displayText='Facebook'></span>
					    <span class='st_twitter_vcount' displayText='Tweet'></span>
					    <span class='st_googleplus_vcount' displayText='Google +'></span>
					    <span class='st_email_vcount' displayText='Email'></span>
				    </div>
                                
                            </div> <!-- /.course-details -->
                        </div> <!-- /.course-post -->

                    </div> <!-- /.col-md-12 -->
                </div> <!-- /.row -->


                
            </div> <!-- /.col-md-8 -->


            <!-- Here begin Sidebar -->
            <div class="col-md-4">

                <?=$this->load->view('home/inc/widget_form_report')?>

            </div> <!-- /.col-md-4 -->
    
        </div> <!-- /.row -->
    </div> <!-- /.container --> 
